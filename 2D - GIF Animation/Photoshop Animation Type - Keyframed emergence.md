## Keyframed emergence

Say you want visual elements to appear or emergence at some frame:

Say you have designed three layers (actually three text), and each text is only one number. However, they are laid out in such a way that if all layers are visible, you'll see a diagonal 1-2-3:
![](B1tQtSh.png)

By creating the frames using "Make Frames from Layers", each frame will iterate through your layers from bottom to top, toggling only one layer to be visible at all time. Only on the final frame will it have all layers visible:

![](mIKI6rx.png)

When you click Frame 1, then click Frame 2, then click Frame 3, you'll see the Layers panels changed respectively for each Frame clicked:

- Clicking Frame 1 in the timeline shows this at the Layers panel:
  ![](3JOgm6S.png)

- Clicking Frame 2 in the timeline shows this at the Layers panel:
	![](wpRVO56.png)

- Clicking Frame 3 in the timeline shows this at the Layers panel:
	![](EwowOha.png)
