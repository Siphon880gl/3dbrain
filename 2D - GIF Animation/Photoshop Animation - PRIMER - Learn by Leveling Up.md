Learn by leveling up with challenges. Use the [[Photoshop Animation - PRIMER]] for help.

Designed by Weng

Lv 1:
From nothing to faded in
- Hint: [[Photoshop Animation Type - Fade-in (aka Tweened)]]

Lv 2:
From nothing to faded in, but hold the final frame for a while (Applies duplicating frames)
- Hint: Read up on durations at [[Photoshop Animation - PRIMER]], specifically how to lengthen a part of the animation

Lv 2:
Count down from 3 to 1
- Hint: [[Photoshop Animation Type - Keyframed emergence]] but the number can stay in one place

Lv 3:
Image swipes in
- Hint: Is keyframed emergence but first few frames the picture is off the cavas

Lv 4:
Ping pong ball
- Have it bounce back and forth between the left and right edge of the graphic\
- Hint: Duplicate frames and then apply "Reverse Frames" on only the duplicated frames

Lv 5:
Animate drawing an alphabet character. 
- Hint: To write that letter, you can type text, then convert it into a layer. Or you can manually draw the alphabet using the paintbrush tool.
- Hint: The animation type is Accumulating Pixels - For example, on each subsequent frame, it is drawing an alphabet "A" until it finishes drawing the "A". You may want to start from a fully drawn "A" and on each layer you erase a bit of the "A", working backwards from a fully drawn alphabet. 
- Use Grid mode (CMD+H on Mac) so you can decide to erase by one square or two squares per frame, keeping consistency.
- Hint: Then you can make frames out of the layers, and you may have to apply "Reverse Frames".
- Hint: Apply frame by frame which layer has eye icon or visibility. If you follow the above hints as your workflow, usually you need to have only one layer visible per frame.