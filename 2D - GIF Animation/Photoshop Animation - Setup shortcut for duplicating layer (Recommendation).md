Recommend shortcut for duplicating layer

When creating a new layer from a current layer and copying any pixels that belong to that layer, Do not use shortcuts select all on a layer, then copy and paste, because it could be shifted a few pixels off - use duplicate instead. You right click the layer and go to duplicate:

![](z0A3mCg.png)

You may notice the shortcut here SHIFT+CMD+D, but that’s in fact customized. Photoshop doesn't have a default shortcut key for duplicating an image, but you can create your own custom shortcut. Here's how you can assign a shortcut key to duplicate an image. 


**Recommendation: Good one for Duplicate layer is SHIFT+CMD+D because the D can be remembered as duplicate, and when asked to resolve Reselect,  you can just set that to empty (because the Reselect command by default is SHIFT+CMD+D which conflicts with this recommendation, and Reselect is usually not a commonly needed shortcut)**

---

Quick instructions on assigning the shortcut is at [[Photoshop - Assign shortcut keys]]. But for more hand holding, you can follow the instructions at [[Photoshop - Create new layer with same image (Fundamental)]].