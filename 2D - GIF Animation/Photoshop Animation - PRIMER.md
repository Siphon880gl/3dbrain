AKA Get Started

Pathways:
You can follow this video or follow my text tutorial. Although contents differ, the learning objective is the same
https://www.youtube.com/watch?v=omdfcGYEqPY

---

## Setup for animation
1. Make sure animation timeline is on. Either:
	- Window → Workspace → Motion
	- Window → Timeline

2. Convert the timeline into Frame Animation (Not Video Timeline)
![](8DxWLsa.png)

3. Populate the timeline: There are different ways to populate the animation timeline with frames to create the GIF animation. One way is to "Make Frames from Layers". Make sure you have layers that try to match how the animation would look if it were frame by frame.
	![](TnMUK9y.png)

	^ The above steps are required for all your animations

---

## Know the fundamentals

We will create an animation. But before we do that, we need to know some fundamental operations because we may need them while creating the animation:
- TOC
	- Looping Settings
	- Frames from Layers
	- Reverse Frames
	- Keyframe states
	- Setting durations
	- Deleting frames
	- Saving GIF for web

### Looping Settings
- Will it loop? If it loops, how many times or for forever? You can set here:
![](AOS7MYm.png)

### Frames from Layers
- Know the behavior of: "Make Frames from Layers"
	
	- Say you have designed three layers (actually three text), and each text is only one number. However, they are laid out in such a way that if all layers are visible, you'll see a diagonal 1-2-3:
	  ![](B1tQtSh.png)
	
	- By creating the frames using "Make Frames from Layers", each frame will iterate through your layers from bottom to top, toggling only one layer to be visible at all time. Only on the final frame will it have all layers visible:
	
		![](mIKI6rx.png)
	
		- When you click Frame 1, then click Frame 2, then click Frame 3, you'll see the Layers panels changed respectively for each Frame clicked:
			
		- Clicking Frame 1 in the timeline shows this at the Layers panel:
		  ![](3JOgm6S.png)
		
		- Clicking Frame 2 in the timeline shows this at the Layers panel:
			![](wpRVO56.png)
		
		- Clicking Frame 3 in the timeline shows this at the Layers panel:
			![](EwowOha.png)
	
	- Each keyframe saves a state of all your layers' visibility, sizing, and positioning
	  ![](aSEKQd3.png)![](DGGaGKQ.png)![](MAgtDyq.png)
		Explanation: While 1,2,3 are different layers that get visibility toggled for each frame (You dont have to click any save button), the T is the same layer at all frames - it just gets moved for every frame, yet the frame remembers the new position. Likewise, had we resized any layer, it would have remembered for the frame. 

### Reverse Frames
- Reverse Frames
	- When to use:
		- Messed up because the animation is not going in the direction you planned
		- Or you want an animation with alternating directions or loops in the reverse direction.
	- You can select all the frames in the Timeline then go to the timeline menu for “Reverse Frames”:
	  ![](v73BDUR.png)

### Setting durations
- Setting duration of each frame and total duration of the animation
	- It's very straightforward since Photoshop keyframe animation does not use FPS. They use seconds directly:
	- **Example**: This would be a 1 second per frame for a total of 3 seconds animation
	  ![](1dmJiVB.png)
	- **How**: You set the duration by clicking the down caret at the bottom of a frame. This adds to the total duration of the animation
	- **Another scenario**: If you want to make a part of the animation longer, another way is to duplicate frames:
		- Select the frame(s) to duplicate to the right:
		- ![](qgkncgt.png)
		- And then it results:
		  ![](d9uvMkE.png)

### Deleting frames
- Deleting frames:
	- Select the frames to delete (SHIFT+click for multi-select)
	- You can either use the delete icon:
	  ![](olNOcIX.png)

	- Or you can click menu icon at top right of animation timeline and click "Delete Frames":
	  ![](zRYJB23.png)

### Saving GIF for web
- Saving as animated GIF:
	- File → Export → Save for Web (Legacy)


---

## Create the animation

### Decide what type of animation
Decide what type of animation you want to do and follow its tutorial if available:
- Keyframed emergence
	- Follow: [[Photoshop Animation Type - Keyframed emergence]]
- Keyframed motion (one-directional)
	- Eg. Swipe in an image from off screen
	- Hint: Is keyframed emergence but first few frames the picture is off the cavas
- Keyframed motion (alternating directions)
	- Hint: Duplicate frames and then apply "Reverse Frames" on only the duplicated frames
- Accumulating pixels
	- Eg. Each subsequent frame it drawing an alphabet "A" until it finishes drawing the "A"
- Fade in animation (Tweened animation)
	- Follow: [[Photoshop Animation Type - Fade-in (aka Tweened)]]


### Or level up with mastery challenges:
Follow [[Photoshop Animation - PRIMER - Learn by Leveling Up]]