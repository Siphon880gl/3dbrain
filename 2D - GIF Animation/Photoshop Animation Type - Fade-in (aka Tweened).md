
## Tweening

### Simple Fade in

Select the two frames you want to tween. Photoshop will do its best to fade in and morph between the two frames.

If you want a simple fade in, have a transparent frame (by de-selecting all layers when that frame is clicked), followed by the frame showing the final frame you want the animation to fade into.

![](nIC6yXs.png)

Click the tween icon
![](vGv8z34.png)

Enter how many additional frames inbetween the two frames. The more frames, the slower the fade in finishes over time. This is because each frame has a duration:
![](UdAKSYG.png)

### Morphing

If you have two frames with two different visuals
The first frame's visual will fade out while the second frame's visual will fade in, both simultaneously. The more frames you add at the tween settings, the more exaggerated the effect.


### Selecting only one frame

If you select only the very final frame, tweening will automatically select the previous frame and the current frame you selected (which is the final frame).

If you select only the very first frame, tweening will automatically select the current frame (which is the first frame) and the next frame.

If there's only one frame in the timeline, tweening button is disabled gray.
