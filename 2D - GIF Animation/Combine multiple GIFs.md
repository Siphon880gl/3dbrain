To combine multiple GIFs and have them play simultaneously in **Photopea**, follow these steps:

1. If applicable: **Prepare Your Background (Still Image)**. Eg a beach [https://www.photopea.com/tuts/combine-multiple-gifs-together/](https://www.photopea.com/tuts/combine-multiple-gifs-together/)

2. **Open the multiple GIFs**:
   - Import each GIF you want to combine by selecting **File > Open**, and open each one separately.
   - Each GIF will open as a separate project with multiple layers (each representing one frame of the animation).

3. **Copy GIF Frames to the Main Background**:
   - Go to the first GIF's project.
   - Select all its frames (you can hold **Shift** and click on each frame in the layers panel).
   - Right-click on the selected layers and choose **Duplicate Layers...** → You get to select a document, so select the background (or empty image) project.
     ![](zrwBSzq.png)
- You may want to organize each GIF you're combining. Use Groups/folders:
  ![](EZPaaiK.png)
  ^ From [https://www.photopea.com/tuts/combine-multiple-gifs-together/](https://www.photopea.com/tuts/combine-multiple-gifs-together/)

4. **Arrange the GIFs**:
   - Once you've duplicated all GIF layers into the main background project, position, resize, or rotate each GIF's frames by selecting the corresponding layers in the layers panel and using the move or transform tools.


5. **Repeat for All GIFs**:
   - Do the same steps for the other GIFs, duplicating their frames into the main image.

  
Create the timeline in the main Photoshop project: Window → Timeline. Make sure to create a keyframe type timeline

Then click Add new frame icon in the timeline. Create as many frames as needed (based on the layers counted from a GIF)

Select each frame then on the right panel. As you're switching frame, it's changing which layers are visible at the Layer panels. Tip: Holding OPT while clicking eye toggle slot will make it only that eye slot. Second layer you’re adding to the same frame, dont hold OPT