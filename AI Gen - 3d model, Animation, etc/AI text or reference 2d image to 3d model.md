
Some 3d model generators are from text prompt or from 2d image (it makes 2d image into 3d model!)


---

AutoDesk Bernini - New only as of 6/2/24
https://adsknews.autodesk.com/en/news/research-project-bernini/

---


[https://www.tripo3d.ai/](https://www.tripo3d.ai/)  

[https://www.youtube.com/watch?v=JpFRSIPvDSg](https://www.youtube.com/watch?v=JpFRSIPvDSg)  
As of Q1 2024, the best AI 3d model generation when it comes to quality is Tropo3D

---


[https://www.csm.ai/](https://www.csm.ai/)  

[https://www.meshy.ai/](https://www.meshy.ai/)  

[https://www.sudo.ai/](https://www.sudo.ai/)

---



[https://dreamgaussian.github.io/](https://dreamgaussian.github.io/)

Youtuber showcases dreamgaussian on youtube and hugging face:
[https://youtu.be/26MntJCFIzg?si=WsrLNkK2B54hs51q](https://youtu.be/26MntJCFIzg?si=WsrLNkK2B54hs51q)  
[https://huggingface.co/spaces/jiawei011/dreamgaussian](https://huggingface.co/spaces/jiawei011/dreamgaussian)


---



Luma's Genesis
Btw Luma has take picture and convert into 3d model
Weng tested: Easy to use. 3D models not the best quality

Are they the same generator?
[https://lumalabs.ai/genie?view=create](https://lumalabs.ai/genie?view=create)
https://lumalabs.ai/dashboard/imagine

![](uQ2EDlZ.png)

![](f1JN7Dc.png)

I could immediately add to Unreal Engine after downloading:
![](Ka9q32N.png)


![](AxVJiJL.png)

