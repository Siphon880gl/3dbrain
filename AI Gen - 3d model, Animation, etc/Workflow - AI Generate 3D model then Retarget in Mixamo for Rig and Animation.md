Status: Cursorily

You generate a 3d model using AI. Then you upload the 3d model temporarily into Mixamo, which will allow you to retarget from their various animations into your 3d model. Then you redownload your 3d model off Mixamo.

Btw, Mixamo requires Adobe account, but is free as of Q1 2024.

Instructions:
https://youtu.be/iHK_MQlwEis?si=c69oszDt4uR18n6-

Here Meshly AI generates a 3d model from text prompt. You could use the other various available text to 3d model free services out there
![](8ZlQNFa.png)

You map the joints of your imported skeleton character inside Maximo, then it does auto rigging, then any of their animations at Maximo will work! Then I supposed you download the model with the animations

![](doyXSHO.png)


Shows the bear can now dance because you auto rigged a dance animation from mixamo
![](RFLyMkC.png)


![](gC3V9XP.png)
