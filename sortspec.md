---
sorting-spec: |-
  Compositing - Motion Film, Video Editing
  Compositing - Image Editing, Touchup
  2D - GIF Animation
  Unreal Engine
  Blender
  Game Development - Misc
  Game Development - Three Js
  -------------------------------------------
  AI Gen - 3d model, Animation, etc
  Free - 3d Models, Animations, etc
  Prerequisite Knowledge
  Motion Film, Composing, etc
  %
  README
  package.json
  sortspec
  server-update.php
---
