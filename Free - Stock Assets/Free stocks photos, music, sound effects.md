
## Free Photos
- Recommend buying credits at appsumo for depositphotos
  https://depositphotos.com/home.html?gclsrc=aw.ds&&utm_source=google&utm_medium=cpc&utm_campaign=DP_USA_EN_Brand_Search&utm_term=depositphotos&gad_source=1&gclid=CjwKCAjwmaO4BhAhEiwA5p4YL-kYpgGch3Ehtd40b2GwLucjhcwqAoWwskPni7-vOWnRobIwbFKWxxoC6TAQAvD_BwE

## Sound Effects

- Free Sound
  [https://freesound.org/](https://freesound.org/)  

## Music

- Sound Cloud
  [https://soundcloud.com/royaltyfreemusic-nocopyrightmusic](https://soundcloud.com/royaltyfreemusic-nocopyrightmusic)