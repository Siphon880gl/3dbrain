Surfacing is a crucial part of the 3D production pipeline. Surfacing (also called texturing or look development) is the process of defining how a 3D model's surface appears. Here's what it involves:

1. UV Mapping
- Unwrapping the 3D model into a 2D layout
- Creating coordinate systems for applying textures
- Making sure textures align properly without stretching or distortion

2. Texture Maps
- Color/Albedo maps: Define the base color
- Roughness maps: Control how rough or smooth areas appear
- Metalness maps: Define which parts are metallic vs. non-metallic
- Height/Displacement maps: Add surface detail
- Normal maps: Add detail without changing geometry
- Opacity maps: Control transparency

3. Material Creation
- Building shader networks or material definitions
- Combining multiple texture maps
- Setting up procedural textures
- Creating layered materials (like paint over metal, or weathering effects)

4. Surface Detail
- Adding weathering and wear
- Creating surface imperfections like scratches or dirt
- Defining material variations across the model
- Adding decals, labels, or graphics

Think of shading as how light interacts with the surface, while surfacing is about defining what that surface actually is - its color, texture, and material properties. They work together to create the final look of a 3D object.