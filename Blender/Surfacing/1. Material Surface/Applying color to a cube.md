You can add material in material settings, then for Principled BSDF's base color, we set it to red. BSDF stands for Bidirectional Scattered Distribution Function. 
![[Pasted image 20250201051752.png]]

You would have to turn on material preview as well for the viewport which is the final icon of he Viewport Shading icons.
![[Pasted image 20250201051810.png]]