This assumes you already have materials connected to an image texture and that materials view is on

Select an interested face -> Press U -> Unwrap by any of them

That creates an overriding face in the UV editor (when you open UV)

When selecting all faces and you see all possible UV faces, the biggest face (which was a result of unwrapping) overrides the original face in the cross:
![](VcubtSW.png)
