
Here’s a 3d model of a pikachu with UV coordinates
![](yrliNmx.png)

The UV coordinates would correspond to a 2d texture image you can edit inside Blender, or Photoshop or other photo editors.

However it could be several texture images if the model is divided into smaller model parts:
![](01nAyyM.png)

![](rrgEfCj.png)

You can also change the UV to XY corresponding coordinates. 

You can also select parts of the model and it can highlight the regions of the 2d texture map that part of the model correspond to.


![](mjeCadN.png)
