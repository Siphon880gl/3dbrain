
Prepared image:
![](gt2pa2T.png)


Transparent graphics supported? Yes

Go edit mode into face selection (this will help make your life easier later in UV Editing):![](S9WgwGl.png)


Go to properties, switch Base Color to Image Texture (by clicking yellow bubble), then select the image from your files:
![](qPTNM1y.png)


---

But you might not see the image texture in the actual model. Blender’s rendering is lazy. Turn on image texture mode here. 2nd to last option at top right of viewport:
![](R0sSuNL.png)

![](KBgJgzG.png)

---

Then to edit which faces, proportion of the pic to the surfaces, etc you would go into UV Editing
![](F0XLWh0.png)


This will open up UV map editor along side your model.

Make sure your UV editor has the image as the background. If not, select the image from near top middle option:
![](pVKR9JW.png)

Because you had face selection on from prior, click the surface on your model to see which area your image covers. Adjust the vertex/corners to change the appearance of the image texture over the surface(s). You can drag surfaces off the image to make them disappear
![](Bluewp2.png)

![](9UrgzS5.png)
![](9FCMzZu.png)

  

Figured out from. I added face selection:

[https://www.youtube.com/watch?v=jLGWE335J28](https://www.youtube.com/watch?v=jLGWE335J28)
