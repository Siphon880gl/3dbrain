
Prepared image:
![](gt2pa2T.png)

But we're using an unprepared image like:
![](01YLC8N.png)


Transparent graphics supported? Yes

Go edit mode into face selection (this will help make your life easier later in UV Editing):![](S9WgwGl.png)


Go to properties, switch Base Color to Image Texture (by clicking yellow bubble), then select the image from your files:
![](qPTNM1y.png)


--- 

But you might not see the image texture in the actual model. Blender’s rendering is lazy. Turn on image texture mode here. 2nd to last option at top right of viewport:
![](R0sSuNL.png)

![](KBgJgzG.png)

---


Then to edit which faces, proportion of the pic to the surfaces, etc you would go into UV Editing
![](F0XLWh0.png)

This will open up UV map editor along side your model.

Make sure your UV editor has the image as the background. If not, select the image from near top middle option:
![](pVKR9JW.png)

Side note: If you’re making the image of surfaces for a cube in photoshop, this will help guide you (You could have one image, then expand canvas 400% width and height)
![](MnMwtph.png)


Selecting all the faces in Edit Mode will show you which face is mapped to which part of the image texture:
![](RNYYAae.png)

Now lets make one face be completely the blue star without distorting the other faces
1. Select a particular face in Edit Mode:
   ![](HLcl3Dm.png)

2. You can manipulate the UV vertices with g -> x/y/z. Once you start moving the UV vertices of that one face, another set of uv face is created that overrides the original's T for that face:
   
   ![](Rsi9l1N.png)

	- And when selecting all faces, the UV editor shows you the original face in the cross and the face that overrides it:
	  ![](glMmf9t.png)

To revert back to the original cross's face:
Move the overrider overlapping the original face, then make sure the double vertices at each corner is selected (selected 8 vertices total), then right-click -> merge by distance:
![](OnObN5H.png)


![](kphZjwS.png)

Now if your deform the other face's overides in the UV editor, then they can be empty faces in the model