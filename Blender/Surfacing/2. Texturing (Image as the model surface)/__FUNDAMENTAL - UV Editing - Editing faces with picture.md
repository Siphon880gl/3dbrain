
Go into UV Editing tab:
![](PCfkInE.png)

Screen will split into UV Editing and Viewport:
![](qiHxzxe.png)

---

To have the faces in UV Editor, so that the faces can be placed over an image in the UV editor on the left, and then the image will correspondingly skin onto your 3d model on the viewport on the right - Select all points/edges/faces in the viewport in Edit Mode, then a cross of faces will appear on the UV Editor:
![](CUWDLK7.png)

Then you can load in an image from a file on your computer:

![](VcubtSW.png)
