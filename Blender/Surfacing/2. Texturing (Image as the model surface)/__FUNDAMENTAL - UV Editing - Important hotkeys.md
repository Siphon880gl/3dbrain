
Let's call this the cross faces inside the UV Editor:
![](bH8uIcP.png)


You can multi select vertices with SHIFT click or Shift drag select box over the vertices.

The hotkeys that normally work in Edit Mode also works:
- G: You can move the vertices or edges or selections of the cross faces with G then drag.
- A will select all vertices
- S will let you resize (for example, selecting the entire cross)
- R will rotate the cross along the UV plane.
