Sculpt Mode: Shift+Space:

![](lJAXNRx.png)

![](mWYmXFy.png)



You can draw or select an area that wont be affected by the sculpt tools

Mask. Lets you brush where you will mask. You may have to set strength to 100% to notice

![](159PUrp.png)

![](Mv1VNHi.png)

Combined Box Mask with Mask:
![](MuuRftO.png)


---

Clear with:
Sculpt Mode secondary menu Mask -> Clear Mask

Invert with:
CMD+I
Or Sculpt Mode secondary menu Mask -> Invert Mask

![](ZVaAOoA.png)
