Make sure you’re in sculpting module and sculpt mode

![](Lm5vaTd.png)

Adjust brush and brush strength with F and Shift+F or right click

Clear all masks. Press A → Clear Mask:
![](HBIwvtA.png)

Check the vectors/edges by going into Edit Mode. If not enough vertexes/edges, then that’s why too. Hint: Go to sculpting mode, then remesh (Press R, scroll to when there's more details, click to soft commit. Then press CMD+R to hadr commit.)