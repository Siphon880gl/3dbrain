The major collections are 3d modeling, 3d printing, Sculpting, Surfacing, and Animations

After you 3d modeled, often you either go to 3d Printing, or Surfacing, or Animations, or Shading, depending on your use case.

Surfacing includes:
- Materials
- Painting
- Texturing

Animation includes:
- Rigging (Giving bones and joints to move the model)
- Posing

Shading includes:
- Lighting