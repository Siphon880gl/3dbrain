Let's say you want to move an entire face to where the 3d cursor is

Set the 3d cursor exactly at the face you want there to be snapping:
![[Pasted image 20250201050021.png]]

At one dimension (Numpad, 1, 2, or 3, or click dimension at the Gimbal), set the 3d cursor (Shift+Right click) as closed and centered to the edge as possible (You may want to zoom in):
![[Pasted image 20250201050036.png]]

Then at the 90 degree's dimension (Numpad... or click dimension at the Gimbal), set the 3d cursor vaguely centered at the face (does not need to be precise):
![[Pasted image 20250201050044.png]]

... the 3d cursor gets moved to:
![[Pasted image 20250201050053.png]]

While in OBJECT MODE, start moving the to-be-modified object to get in appropriate alignment with body object, probably using dimension view bumpad keys 1,3,7 or clicking the gimbal dimensions:
![[Pasted image 20250201050110.png]]

And with the appropriate face selected in Edit Mode:
![[Pasted image 20250201050119.png]]

Press Shift+S for selection menu → Selection to Cursor (means 3d cursor)
![[Pasted image 20250201050134.png]]

Final result:
![[Pasted image 20250201050308.png]]

