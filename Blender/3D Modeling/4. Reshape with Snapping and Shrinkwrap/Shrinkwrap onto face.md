You have a large shape colored red:
![[Pasted image 20250201050334.png]]
^FYI: You can add material in material settings, then for Principled BSDF's base color, we set it to red. BSDF stands for Bidirectional Scattered Distribution Function. You would have to turn on material preview as well for the viewport which is the final icon of he Viewport Shading icons.

Add Shrinkwrap modifier on the wrapper object, set to Nearest Surface Point / On Surface, and set the Target to the body object:
![[Pasted image 20250201050551.png]]


Then it shrinkwraps to surface:
![[Pasted image 20250201050346.png]]

When you're ready, you can apply the modifier at the modifier option's down caret.
![[Pasted image 20250201051553.png]]