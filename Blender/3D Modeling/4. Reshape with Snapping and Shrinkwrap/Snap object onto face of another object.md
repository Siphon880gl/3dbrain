
![[Pasted image 20250201052600.png]]

Snap “Closest” to “Face Project” and “Align Rotation to Target”. This will make sure when you bring an object onto another object, that it’ll align with the face and will rotate accordingly too.
![[Pasted image 20250201052616.png]]

Make sure Snap is actually on after configuring it:
![[Pasted image 20250201052623.png]]

Then you get this:
![[Pasted image 20250201052635.png]]

If the attaching object moving onto the body object is much larger or same size, it will snap weird or not snap at all.

Problematic:
![[Pasted image 20250201052654.png]]

Better:
![[Pasted image 20250201052702.png]]
