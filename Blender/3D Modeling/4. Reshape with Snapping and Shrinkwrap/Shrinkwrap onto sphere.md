Not good unless you want to shrinkwrap to a face:
![[Pasted image 20250201050334.png]]
^FYI: You can add material in material settings, then for Principled BSDF's base color, we set it to red. BSDF stands for Bidirectional Scattered Distribution Function. You would have to turn on material preview as well for the viewport which is the final icon of he Viewport Shading icons.

Just shrinkwraps to surface:
![[Pasted image 20250201050346.png]]

---

Almost good:
![[Pasted image 20250201050509.png]]

![[Pasted image 20250201050513.png]]

**It’s because there wasn’t enough subdivisions**

Add subdivisions: While in Edit Mode selecting all of the object that acts as the wrapper, right click → Subdivide. At the bottom left subdivide options, you can set the number of subdivisions.
![[Pasted image 20250201050536.png]]

![[Pasted image 20250201050540.png]]

Add Shrinkwrap modifier on the wrapper object, set to Nearest Surface Point / On Surface, and set the Target to the body object:
![[Pasted image 20250201050551.png]]

The shrinkwrap shape seems much better now because of added subdivisions.

Lets add offset until the shrinkwrap is out enough to be visible on the body object:
![[Pasted image 20250201050610.png]]
^ Here we did 0.03m

When you're ready, you can apply the modifier at the modifier's down caret.
![[Pasted image 20250201051553.png]]