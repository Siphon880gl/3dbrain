Even though you can snap an object to a bigger object... and you can make it look more natural by having it rotate to align with the normal, some shapes are too complex to just rotate. What you can do is to allow the attacher object to distort to the body object:

>[!note] Below screenshots from:
> https://www.youtube.com/watch?v=jD0t6kcq7zM

You’ve done Snapped mode onto closest face projection with align rotation to target. Even though it rotated the attacher object to fit naturally with the body object, that attacher object may have rotated but it can’t bend to fit the surface of the body object.

![[Pasted image 20250201053447.png]]

You want the top and bottom faces of your attacher object to conform to the body object. You want it to bend. This is where lattices come in (adding subdivisions to allow bending).

With your attacher object selected and 3d cursor inside of it (or around it - use Shift+Right click), create Lattice (Shift+A)
![[Pasted image 20250201053500.png]]

Use the lattice U,V,W to imagine a new malleability of the attacher object. The higher the number, the more subdivisions, therefore the more edges your attacher object will have to smoothly stick to the surface (snapping face projection):
![[Pasted image 20250201053552.png]]
^ U,V,W just stands for X,Y,Z for the lattice object.

Select attacher object first, the lattice object final, then parent in the form of Lattice Deform (Secondary menu Object → Parent → Lattice Deform, OR Ctrl+P → Lattice Deform)
![[Pasted image 20250201053637.png]]

Then, finally, add a shrinkwrap with nearest surface... on surface.