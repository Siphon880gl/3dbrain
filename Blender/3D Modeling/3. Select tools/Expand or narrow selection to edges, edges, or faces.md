
Expand or narrow selection to edges, edges, or faces, with current selection

![](4o3EWIV.png)

1,2,3 to switch between selection of vertexes, edges, faces
![](C0NfjSc.png)
