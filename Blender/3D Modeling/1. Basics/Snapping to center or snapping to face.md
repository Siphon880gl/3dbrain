
Snap attachments/smaller parts into a bigger body:

**How**
Snap to center, face, and align rotation to target:
![](jombMt1.png)



**Use case: **
Atom model
![](vom7U9O.png)

[https://www.youtube.com/shorts/uWGXBGQN0SI](https://www.youtube.com/shorts/uWGXBGQN0SI)

---

Snapping to nearest face

![](Ye2B5N0.png)

Make sure actually on:
![](kaL3Q5z.png)

Having created a connected vertex from one of the vertexes (either through extrude E or Ctrl+Click)

![](vTjD0Xr.png)

I can more easily drop it close to the face at the top:
![](8Gu0PkP.png)

---

Another use case of snapping to closest face:
