You can click and drag the gimbal, magnifying glass, or hand icon, in order to perform orbiting, zooming, or panning.


![](9bpFu4Q.png)
