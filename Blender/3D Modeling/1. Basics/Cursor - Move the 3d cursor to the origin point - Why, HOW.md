
## HOW
Shift+S -> Cursor to selection

---


## WHY
Why move 3d cursor to origin point of object:

---

Moving the **3D cursor to the object's origin point** is useful in many situations, especially when precision and efficiency matter in modeling, animation, and transformations. Here are some **practical use cases**:

---

### **1. Accurate Object Alignment**

🔹 **Use Case:** You need to place an object precisely in relation to another object (e.g., placing a bolt in a hole).  
🔹 **Solution:**

- Select the object.
- Press `Shift + S` → **Cursor to Selected** (moves 3D cursor to object origin).
- Select another object and press `Shift + S` → **Selection to Cursor** to snap it exactly.

---

### **2. Rotating Around a Specific Point**

🔹 **Use Case:** Your object rotates incorrectly because the origin point is not where you need it.  
🔹 **Solution:**

- Move the **3D cursor** to the object's current origin (`Shift + S` → **Cursor to Selected**).
- Then move the origin to a new location (`Object > Set Origin > Origin to 3D Cursor`).
- Now, when you rotate, the object rotates around the correct pivot.

**Example:** A door hinge should rotate around its hinge point, not its center.

---

### **3. Scaling Without Changing Position**

🔹 **Use Case:** You want to scale an object **from a specific point** instead of its default center.  
🔹 **Solution:**

- Move the **3D cursor** to the object's origin.
- Change the pivot point (`.` key → **3D Cursor**).
- Scale (`S`) using the 3D cursor as the reference.

**Example:** Scaling a column from its base instead of the center.

---

### **4. Snapping Objects Together**

🔹 **Use Case:** You need to attach objects without guessing or eyeballing alignment.  
🔹 **Solution:**

- Move the **3D cursor** to the first object's origin.
- Then snap the second object to it using `Shift + S` → **Selection to Cursor**.

**Example:** Placing gears together in a mechanical assembly.

---

### **5. Precision Modeling with Modifiers**

🔹 **Use Case:** Certain modifiers (e.g., **Mirror, Array**) work based on the object origin.  
🔹 **Solution:**

- Move the **3D cursor** to the current origin.
- Adjust the origin to influence how the modifier behaves.

**Example:** A symmetrical object should mirror from a precise point.

---

### **Conclusion**

Moving the **3D cursor to the object origin** is a **precise way** to control transformations, rotations, snapping, and modifier behaviors. It helps avoid manual adjustments and ensures accuracy in 3D modeling and animation workflows.

Would you like a GIF or images demonstrating these techniques? 😊