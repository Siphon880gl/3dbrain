
Duplicate cube:
Object Mode: Shift+D then drag, then click to confirm placement of duplicated object/shape.
![](AXE846R.png)


Duplicate flat surface off a cube
Edit Mode (because this lets you select a face). Go for selection mode: face/polygon (press 3). Shift+D then drag, then click to confirm placement of flat surface aka plane.
![](P8k8Ntc.png)
