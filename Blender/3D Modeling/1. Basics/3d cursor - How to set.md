Setting it to 0,0,0? Press Shift+C
Mnemonic: 3d **C**ursor to **C**artesian **c**enter **c**oordinates.

Two ways to do this:
SHIFT+Right click to set 3d cursor where your mouse is

Or input the values 0,0,0 at numeric menu (Press N) and going to View side tab:
![](G4hePLI.png)
