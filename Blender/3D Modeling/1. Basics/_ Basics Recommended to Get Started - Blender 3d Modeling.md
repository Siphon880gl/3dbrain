
**Recommended you internalized and used these techniques and hotkeys before continuing in Blender**

---
---

#### Visualization

Using numpad to align to X,Y,Z view or to move view
Using visual gimbal to align to X,Y,Z view (Eg. Click Z)
Know to jump viewport to selected object with numpad period (.)

How to access viewport options including toggling on Face Orientation (Great for preparing for 3d printing)

---

#### Visualization and manipulation with snapping

How to add snapping to the grid
How to snap to nearest edge or face when moving a face

---

#### Visualization hide/unhide out of the way

H to hide an object
ALT+H to unhide all objects
While in object mode: 
- 1 to show only first object (all else hidden at outliner top right panel). 2 to only show second object. 
- SHIFT+1 or some number to toggle hide or show for that object.
- Alt+H stills works to show all objects again (by unhiding).


---

#### Visualization tooling

Shift-Right click to set 3d cursor to new point. Shift+C to set to 0,0,0.
CTRL-Right click or E for extrude from a selected vertex to add new connected vertex
Note that duplicating Shift+D on a selected vertex creates a vertex that’s floating (not connected)


---
---

### Manipulate Overall

G to grab and move,
S to scale
R to rotate
^ Pressing X, Y, Z, or Shift+X, Shift+Y, Shift +Z IMMEDIATELY after pressing G or S or R would bind the movement to move along an axis or to two axes. If you pressed with SHIFT held down, it moves in all axes except the SHIFT axis (so two axes simultaneously).

### Manipulate geometry

Fill an edge (f between 2 unconnected vertices) versus creating an edge (bisect’s, knife, loop edge, subdivision, etc)

Switch between Object and Edit mode with **tab**. Know what Object mode and what Edit mode is.
While in Edit Mode, change selection mode (what geometry element(s) get selected when dragging a rectangular select box (Shift+Space -> B):
- 1 (Edit → Vertex)
- 2 (Edit → Edge)
- 3 (Edit → Face)
Can only go into Edit mode if an object is selected.

#### Geometry basics on edges

Know appearance-only line versus a functional edge
Pressing F between two vertexes will create a line that doesnt separate the face into more faces
However using knife, bisect, etc to create the edge between two vertex will create more faces from one face

---
#### Add/Duplicate/Delete Overall

Shift+A to add shape
Shift+A to add Mesh → Monkey (to practice advanced geometry editing)
CTRL+A to apply scale (needed after resizing)

Duplicate an object or editable element with Shift+D

X to delete whether it’s the entire object or an editable element (vertex, edge, face)

---

#### Tooling

Shift+Space for the same left toolkit icons but described with labels and can be hotkeyed, conveniently in reach from your mouse cursor’s position
- Reworded: Shift+Space → For toolkit options

For example while in Object Mode and you pressed Shift+Space and see a Box Select with shortcut B, you learned you could quickly select the box select tool next time with:
- Common hotkey you should memorize: Shift+Space → B

Ctrl+E for edge options
ALT+E for extrude options

---

### Manipulate Shape Overall


Bool Tool → CTRL+Shift+- the last selected is the body that gets the hole. The reason why this makes sense is you can cut multiple holes or shapes and the last object or the first object would’ve been the designated body that gets carved into.
- However if you do multiple objects onto a subject at once, it could get glitchy (partial cutting thru). So do 2 selections at a time (cuter and subject)

---

#### Manipulate Grouping

Can join (CMD+J) into one object or separate (P) into two objects. Refer to [[Separate - Blender]]
- Key ones: By Selection and By Loose Parts
- Occasionally: By Material
- Mnemonic: Not Command P because that sets the parent

Parent-Child and collections assignment and their purposes

---

#### Mnemonic Menu:
- M moves an object(s) into collection (Mnemonic: Think about organizing objects first before editing)
- E for extrude. ALT+Extrude for more extrude variants. Ctrl+E for edge options (Mnemonic: Think about modifying geometry in the middle of the project)
- N hotkey opens right tabbed menu (Think about proper scaling and placement for exporting later)
- U (Unwrap - creates face overriding selection in image texture UV section) (Last abbreviation for final 3d modeling)
  
  Briefly: N hotkey opens right tabbed menu. M taken to move to collection, E for extrude, so N available next in the word "Menu", U to unwrap (create overriding face in UV editor to choose another area position and scale for that face of the model)