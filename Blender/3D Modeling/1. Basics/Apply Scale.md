
TLDR: Ctrl+A after you resize (aka scaled) in one or two dimensions

On Blender not applying scale after user resized/scaled an object in one or two dimensions (rather than all dimensions), certain actions will become uneven or asymmetrical
- UV mapping
- Many modifiers
- Beveling

Unless you Apply Scale (Ctrl+A) after the resizing

---

An asymmetrical bevel:
![](LikIbCa.png)

For example, this is no longer:
![](YLoUacz.png)

If you want bevel to be symmetrical, you have to Apply Scale:

Apply Scale (CTRL+A -> Scale, while in Object Mode) before starting the beveling.

This resets the dimensions to 1:1:1
![](JXW1FHM.png)

Even if you scale/resize the object, as long as you do so across all axes, you wont have this asymmetrical bevel problem.

This is okay as well:
![](QuNfnvX.png)

---

## Why

### Why You Have to Manually Apply Scale

- **Consistency**: Applying the scale resets the object's scale to `(1, 1, 1)`, ensuring tools, modifiers, and UV mapping work as expected and uniformly across all axes.

---

### Why Blender Makes You Apply Scale Explicitly

- **Flexibility**: Some workflows rely on the object's scale remaining unaltered (e.g., modifiers like _Solidify_ or animations using non-uniform scaling).
- **Control**: It ensures you decide when to finalize the scale, preserving proportional relationships until you're ready to commit.

---

### What You Lose If Blender Auto-Applied Scale

1. **Modifier Breakage**: Modifiers like _Solidify_ might stop scaling proportionally with the object.
2. **Animation Issues**: Keyframed scaling (e.g., squash/stretch) would be overwritten.
3. **Custom Scaling**: Intentional non-uniform scaling would be lost, as the scale would always reset to `(1, 1, 1)`.

---

### Bottom Line

Blender prioritizes control and flexibility. Auto-applying scale could disrupt workflows, so it's left to you to decide when to finalize it.