
By default, the viewport only shows guidelines for X axis and Y axis:
![](uyPHRq0.png)


It's recommended you add the Z-axis guideline overlay as well, so that you have more precise editing.

Go to Viewport Overlays (two circles that overlap in the middle) and you’ll see Z overlay is off:
![](w8AvRhP.png)


Simply turn it on:
![](2lCkOcM.png)

Now Z axis can be seen on the viewport:
![](yewO0ui.png)
