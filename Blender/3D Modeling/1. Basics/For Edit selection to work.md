
For Edit selection to work, you must had selected an object in Object Mode before switching to Edit Mode (with **tab**). Then the vertex/edge/face you can select will be that object.