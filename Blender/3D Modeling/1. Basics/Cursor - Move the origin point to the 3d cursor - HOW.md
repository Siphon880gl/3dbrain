
See that orange dot? That’s where the pivot point for the rotation of the object is.
![[Pasted image 20250201052756.png]]

Lets move the orange dot to the 3d cursor so that the object can be rotated relative to the 3d cursor:

You can set the orange dot  (origin point of the object) to the 3d cursor
While in Object Mode with the object selected, go to secondary menu Object → Set Origin → Origin to 3d Cursor
![[Pasted image 20250201052816.png]]


You should see both orange dot and 3d cursor in one spot:
![[Pasted image 20250201052825.png]]

And rotation will along that 3d cursor! (It's actually because the object's origin point has moved to the same point as the 3d cursor)
![[Pasted image 20250201052839.png]]

For more use cases for moving the origin point to the 3d cursor, refer to [[Cursor - Move the origin point to the 3d cursor - Why]]