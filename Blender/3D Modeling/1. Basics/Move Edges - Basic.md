
Moving an edge without constraint vs with a constraint  

  
SELECTION

Lets go into Edit Mode and select all four edges (with Selection Mode Edge, pressing 2).

![](YIURocW.png)

---

GRAB/MOVE CONSTRAINED

Press G then drag your mouse around:  

Pic skewed because moving without constrained z

![](1wp8eNI.png)

---

GRAB/MOVE UNCONSTRAINED  

Press G->Z then drag your mouse around:  

Pic not skewed and gets taller because constrained z

![](28VbySf.png)

