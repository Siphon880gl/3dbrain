The **3D Cursor** in **Blender** is a versatile tool that serves as a reference point for positioning, transforming, and aligning objects or elements within the 3D viewport. It appears as a red-and-white circle with crosshairs and can be placed anywhere in 3D space.

### **Main Uses of the 3D Cursor in Blender**

1. **Pivot Point for Transformations**
    
    - You can use the 3D Cursor as a custom pivot point for rotating, scaling, or mirroring objects.
    - To activate it, set the **Pivot Point** to **3D Cursor** (found in the top center of the viewport).
2. **Precise Object Placement**
    
    - The cursor can be snapped to specific locations, such as vertices, edges, or faces, for accurate alignment.
    - Objects can also be moved or added to the exact location of the 3D Cursor.
3. **Adding Objects at a Specific Location**
    
    - When you create a new object (`Shift + A`), it spawns at the location of the 3D Cursor.
4. **Origin Manipulation**
    
    - You can change an object’s **origin point** (the point from which transformations occur) by moving the 3D Cursor and using **Object > Set Origin > Origin to 3D Cursor**.
5. **Aligning Objects and Elements**
    
    - The cursor can be used to align multiple objects or elements in edit mode.
6. **Snapping and Precision Workflows**
    
    - You can snap the 3D Cursor to the grid, objects, or specific points (`Shift + S` for the Snap Menu).
    - Useful for modeling, architectural work, and precise object positioning.

### **How to Move the 3D Cursor**

- **Left Click (default selection mode in Blender 2.8+)**
    - Click anywhere in the viewport to move the 3D Cursor.
- **Shift + Right Click (older versions of Blender, pre-2.8)**
    - Moves the cursor to the clicked location.
- **Shortcut: Shift + S (Snap Menu)**
    - Allows snapping the 3D Cursor to the center, grid, or selected elements.
- **N Panel → View Tab → 3D Cursor**
    - You can manually enter coordinates for precise placement.

Would you like a practical example of using the 3D Cursor for modeling or alignment? 🚀