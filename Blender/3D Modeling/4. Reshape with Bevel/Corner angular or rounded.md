## Basics

With the faces or edges selected, while in Edit mode, press CMD+B for bevel tool
![](IEsfL4l.png)

---

A radius handler appears:
![](z1hMeWv.png)

Drag further the radius from center, the closer the beveling shrinks to the center of the object


---

## Angular corner

Rotating 360 the radius handler will lengthen/decrease the bevel surface:

![](3TrCwFV.png)

![](YjIGCjO.png)


---

## Rounded corner

To make the corner rounded, you're in the middle of creating the bevel like the above instructions, but then you increase/decrease the number of polygons by holding SHIFT while scrolling up and down:

![](t9U6cVV.png)
