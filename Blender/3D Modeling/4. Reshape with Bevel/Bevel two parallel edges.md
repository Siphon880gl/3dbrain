
With two parallel edges you can:

Tome:
![](kvpInBt.png)

Mechanical part’s corners:
![](D7y0WhQ.png)

House
![](WyGcFTf.png)

X:
![](0ZU7Cgt.png)

If you had increased the subdivisions while beveling (Scrolled mouse scroller, press S then drag with the circular handle, or press S then a number 1/2/3), you can make:

Military Barrack:
![](WHy4k7Z.png)
  

Loaf of bread:
![](66CEIj9.png)
