
In Blender, bevel usually performs symmetrical on an edge.

When it’s asymmetrical, it’s likely you resized (scale) the object in one direction or two directions. This means the the scale is no longer 1:1:1 or same number relative to each other.
![](LikIbCa.png)

For example, this is no longer:
![](YLoUacz.png)

If you want bevel to be symmetrical, you have to Apply Scale:

Apply Scale (CTRL+A -> Scale, while in Object Mode) before starting the beveling.

This resets the dimensions to 1:1:1
![](JXW1FHM.png)

Even if you scale/resize the object, as long as you do so across all axes, you wont have this asymmetrical bevel problem.

This is okay as well:
![](QuNfnvX.png)
