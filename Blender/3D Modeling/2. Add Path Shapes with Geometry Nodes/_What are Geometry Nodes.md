
It's a primary tab:
![](jH3agTD.png)

You add a series of inputs and outputs, transformative and generative processes, etc in a flowchart manner to affect shapes in the viewport:
![](rkkwuox.png)

And that creates actual objects/shapes in the editor
![](U2Q7cAM.png)


---

Recommended that you turn on Snap so that the cards you add can line up:
See top right for magnet icon:
![](o0xEKkm.png)
Basic controls:

Shift+A to add a new card. Type to search searching already
X to delete
Click and drag nodes at the cards to connect each other