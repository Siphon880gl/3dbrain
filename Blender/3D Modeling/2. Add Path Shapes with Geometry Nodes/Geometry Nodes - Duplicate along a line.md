For:
![](rz0Die6.png)

Use Nodes:
![](t0zeg4C.png)

Think: You're creating a mesh line and at different points of a grid along that line, you have an instance of Ico Sphere

---

Similar to:
https://youtu.be/EFUE5NSUsWQ