
Quick review

Object Mode -> SHIFT+A -> Curve -> Path
Object Mode -> SHIFT+A -> Curve -> Bezier


---


![](PlEOUE2.png)

The left is a path that wasn't finetuned to look like a curve by using curve vertex handles

The right is a bezier

---

Approach A:
Curve Data -> Geometry -> Bevel -> Depth increased

![](gJyhscT.png)

---

Approach B:
Modifier options -> Add Modifier -> Solidify -> Thickness increased

![](DcxQNyw.png)

You may want to bake the modifier in after you're done, especially if you will be joining with another mesh (Down caret next to modifier name -> Apply)
![](p5mVU4N.png)
