
Is known as: Negative Boolean (Named "Boolean Negative" in the notes only for sorting purposes)
Is also known as: Boolean Difference

---

Two ways to perform Booleans:
- Bool Tool
- Blender's Boolean Modifier

---

## Bool Tool

Make sure you have the "Bool Tool" plugin installed/enabled under Edit->Preferences->Add-ons. If you check its shortcuts, keep in mind that Bool Difference is Ctrl + Shift + Numpad Minus. If you just installed now, you have to restart Blender even if it didn't ask you to (else the cutting by Boolean Difference shortcut won't work).

In **Object Mode**, select your **Plane + Model**
**CTRL+Numpad Minus**
This only works if you had the "Object: Bool Tool" plugin enabled under Edit->Preferences->Add-ons

![](K9yWfWb.png)

Overlap two shapes:

![](hQHx8zP.png)

Selected both:
![](zv87ubw.png)

After shift+ctrl+numpad minus:
![](pcDgLJt.png)

![](vegylTt.png)

For settings, you can access the tool's options at the "N" menu by pressing N and switching to Edit and then expanding Boolean:
![](WO7taib.png)


For global settings, Edit -> Preferences -> Addons -> 
Bool Tool -> View Details (if dont see the settings yet)

---

## Blender Boolean Modifier

Another way to apply Boolean is through
![](SQqj3Xi.png)

You may see no changes in the viewport. You have to hide one of the objects in Outliner (top right panel).