While in Edit Mode, with a face selected:
Secondary "Mesh" -> Knife Topology Tool

![](5FLWeOC.png)



The tool is virtually the same as the "Knife" tool on the left toolbar. Refer to [[Knife]]