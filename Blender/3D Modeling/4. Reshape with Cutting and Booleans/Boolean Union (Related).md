Both are conjoined together

Before:

![](ZWtczsH.png)

After (Looks same):
![](ZWtczsH.png)


For how to use, refer to [[Boolean Negative on Blender]] which has two approaches to doing any Boolean.

With the Bool Tool addon, the hotkey is: `CTRL + Numpad +`