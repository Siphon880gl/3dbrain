Where they overlap, that's the new mesh

Before:
![](ZWtczsH.png)

After:
![](Zl2uGB9.png)


For how to use, refer to [[Boolean Negative on Blender]] which has two approaches to doing any Boolean.

With the Bool Tool addon, the hotkey is: `CTRL + Numpad *`