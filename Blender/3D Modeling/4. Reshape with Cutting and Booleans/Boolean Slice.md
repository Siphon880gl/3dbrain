Only available with the Bool Tool. It's not in the Blender Bool Modifier

It performs negative boolean (aka boolean difference) without deleting the body that got cut. So the cut area and the cuttee are available for further manipulation.

Hot key is Ctrl + Numpad Slash (/)

Before:
![](ZWtczsH.png)

After:
![](kpNcehZ.png)


You may want to separate (Press P for the Separate Menu -> By Loose Parts):
![](9OCEqFg.png)

See this moving the cut area away:
![](CxRFIYL.png)

See that they are binded in a parent-child relationship. See outliner:
![](PCUExGw.png)

So if you rotate one object (either the cut area aka slice or the body that got cut), it rotates the other:
![](lTTey4U.png)

  
To remove the parent/child relationship, refer to [[Parent child - _Ungroup]]


Now notice the cut area aka slice is no longer inside the other object at the Outliner. This means the slice and the cuttee body can now move independently from each other instead of syncing to each other's movements in a parent-child way (refer to [[Parent child - Movement of parent child]]):
![](wlxYRkF.png)
