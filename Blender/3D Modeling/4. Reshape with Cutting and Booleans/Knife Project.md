
Project knife lets you use one plane of mesh and cut straight down to another mesh object and it can cut through communicatively or not

Have a small plane or text above the cuttee along the z index (meaning the cuttee is below the cutter straight along z). Make sure this cutter (plane or text) is not part of the same object as the cuttee. If it’s the same object, then Separate (P) → By Loose Parts (In Edit Mode).  

Select cuttee in object mode
Switch to edit mode
Ctrl+click to include selection of the cuter at the outliner panel top right (NOT in the viewport)

Press 7 or ctrl 7 to go into ortho mode. Cutter is above
Go to secondary menu Mesh -> Knife project

![](ruMY25E.png)


At knife project tool options that pops up at bottom left, decide whether to Cut through (it’s a tick box)
Tilt or orbit your view to see a highlighted region on the cuttee that’s in the shape of the cutter because it’s as if a shadow casted down by the cutter. Click that region to confirm the cut

---

Before cut:
![](FZZ04i2.png)

After cut:
![](jjpc3aH.png)


