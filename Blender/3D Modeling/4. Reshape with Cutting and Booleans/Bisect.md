
In edit mode, lets cut a cube with Bisect

Have the face selected (press 3 then click face)

Shift+Space for edit labeled menu -> Bisect (CMD+B)

![](TbjgFCd.png)


![](4Kf1zjk.png)


Another way to select Bisect, go to secondary menu Mesh -> Bisect:
![](lmMfTy2.png)



---


Now it's cutting time with bisect: drag and drop across two edges:

![](9wV9voo.png)


![](ZeGviRw.png)

And you can select the bottom face -> x -> Face to delete that
![](eTzr1mc.png)


---


Use case:

Lets say we have this awkward cube and wedge joined at an edge. Lets make this a more coherent object that can be part of a bigger starwars plane later:


![](2wpqcTr.png)



![](ZZkC51L.png)


![](H4Unlhr.png)


![](eldAtg6.png)

![](DPvkyWu.png)

Biset. Click and drag from top right to bottom left:
![](sSFdhU7.png)


![](1lIlL9i.png)

---


![](WZ1q4pI.png)

Bisected and bisected again:
![](Qtw9Bh0.png)
