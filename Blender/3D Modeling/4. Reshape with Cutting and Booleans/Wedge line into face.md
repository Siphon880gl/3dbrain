
Any filled line (not actual edge that divides a face into two faces) can be converted into an edge which will finally split the face into multiple faces.

Lets run an experiment. Select the two dots diagonally:
![](gBlMamR.png)

Press f to fill a line between the two vertices:
![](Wd1G5Hc.png)

When face selecting on Edit Mode, it’s clear that line is not a proper edge that divides faces:
![](rlJ2aRp.png)

To convert the face into faces, we will WEDGE the line into the FACE. Mnemonic:
![](jZpCbJy.png)
^[https://flexbooks.ck12.org/cbook/ck-12-middle-school-physical-science-flexbook-2.0/section/13.8/primary/lesson/wedge-ms-ps/](https://flexbooks.ck12.org/cbook/ck-12-middle-school-physical-science-flexbook-2.0/section/13.8/primary/lesson/wedge-ms-ps/)


WIth both the face AND the filled line selected (Edit Mode → 3, Edit Mode 2->), go to Face → Wedge Edges into Face
![](cNRVbIU.png)

Now multiple faces adjacent to the line. I’ve grabbed and move one of the triangular faces created:
![](rmwTatH.png)
