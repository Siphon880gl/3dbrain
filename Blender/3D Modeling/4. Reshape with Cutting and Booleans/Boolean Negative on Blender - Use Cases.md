
Use case:
Basic bowl in the form of a half opened sphere.

Hint: Create UV sphere. Create cube. Overlay at center 0,0. Negative boolean. Select face of inner face. X to delete face. R->Y to rotate into upright position bowl.

![](um7jGqh.png)

You can go further:
![](otMs2at.png)
