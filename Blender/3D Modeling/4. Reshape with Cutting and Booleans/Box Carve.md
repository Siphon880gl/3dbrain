Box Carve
Mnemonic: Scissor box

Quick and dirty (imprecise) way to cut while in object mode 

Works at either Object Mode or Edit Mode

You select and drag a box over the model to cut through it, removing all mesh in the Box Carve rectangle you created.

When the rectangle is present, you can make small position adjustments as long as you don’t release the mouse - hold space key while dragging with mouse.

Recommend you cut at one of the axis views: 1, 3, 7, or CTRL+ version
