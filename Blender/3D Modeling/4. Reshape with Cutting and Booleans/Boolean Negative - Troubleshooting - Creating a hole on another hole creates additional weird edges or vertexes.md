Problem Statement: Using Negative Boolean, creating a hole on another hole creates additional weird edges or vertexes.


If using Bool Tool addon, make sure to configure it for exact solver:

Edit -> Preferences -> View Details for “Bool Tool”
![](3QrXhOj.png)

And make sure Preferences have Exact Solver:
![](l4jhxve.png)


