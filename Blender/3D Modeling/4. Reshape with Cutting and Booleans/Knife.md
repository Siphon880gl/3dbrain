Is a click and click tool but you have to click anywhere on an edge then click anywhere on any other edge, You can continue adding more points across different edges to form a piece you can select and remove. Press Enter when done:

![](QTuYufG.png)

Continued.. significance: Now you can select that face (3) and remove it

![](1zk7myT.png)

----

Continued... Challenge combining other skills:

If you had wanted to remove an entire piece off the cube:
![](3hi1m4X.png)

  
Shift select multiple faces (press 3):
![](BReJu0E.png)

Then x to delete faces
![](D5piW4n.png)

---

Continued... Challenge:

Fill in the gaps:

Vertex select (press 1) all the points where gaps started. Reminder Shift click to add more to a selection:
![](392gl2j.png)


Then press F to fill

Result:
![](GOEnqEf.png)
