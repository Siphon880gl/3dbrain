
Inset increases polygon by adding an inner face

![](KlM0u11.png)

Then you can change geometry to add more details to the shape. Here the detail is a bar outwards:

Pressed E after I (so extruded):
![](IMh6rus.png)
