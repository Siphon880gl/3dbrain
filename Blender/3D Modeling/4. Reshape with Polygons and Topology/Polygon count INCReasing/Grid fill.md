
Grid fill:
Just a circle like this - This is not manifold because not found in mankind/nature.
![](795qCR5.png)

Then in Edit Mode:
![](TUrRko9.png)

Now there’s geometry and therefore is manifold and you can negative boolean etc
![](sVw1zAA.png)

![](65Ew7i9.png)

The non manifold edges will go away if you extrude vertically into a cylinder.