
Goal: Add more vertexes so you can refine how the model looks

Adds more vertex points for you to manipulate the shape:
![](iRBihhe.png)

![](azlSDko.png)
![](fuvScys.png)


Subdivide was done twice here.

To perform subdivide: Edit Mode -> Right click selection -> Subdivide

Now you can select the vertexes/edges/polygon faces and transform them to create a different shape (Maybe a spikey cube)