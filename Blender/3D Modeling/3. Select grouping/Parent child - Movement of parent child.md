
Lets say the small slice is a child to the cubic body:
![](k80Wn60.png)

Rotating the cubic body also rotates the small slice alongside:
![](tJMibU9.png)

Rotating the slice will rotate along a pivot:
![](EA0Y51i.png)

![](jCguK0w.png)

![](Ws2UniE.png)
