Select the child object you want to ungroup from its parent.
Press ALT + P to bring up the "Clear Parent" menu. OR go to Object secondary menu -> Parent -> Clear Parent

![](sIw8Slk.png)
