While in Edit Mode, Shift+Space to have a popup menu of LABELED edit tools. Hidden sister tools can be shown by click and dragging just like you would do on the left toolbar.

![](5dQfIpU.png)
