
If viewport annoying, you can lock in an axis (G->X, for example)

Or you can modify the transformation by sliding at a parameter at Object properties → Transform

![](YKmD5ju.png)
