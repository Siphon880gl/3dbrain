What if the angle is all weird and you can't rotate around the object like you wanted?

For example, here I can't rotate or tilt to the other side of the character model no matter how hard I tried
![](NMqax59.png)


![](ioE6coO.png)

![](kFzQdfL.png)




Select Camera in outliner:
![](CucX7W3.png)

Make sure your viewport is set to the camera so you'll see the changes:
![](zoAO3JE.png)

Go into Object mode so you can change the camera's properties (the object you're on is the camera, so object mode will change camera properties):
![](i62Em4u.png)


Here you can adjust rotation and location to fix the angle in your veiwport so you can be more productive

![](p1V2ODe.png)
