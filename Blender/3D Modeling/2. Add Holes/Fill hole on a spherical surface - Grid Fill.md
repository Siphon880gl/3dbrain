
The usual bridge edge algorithm wont work:
![](0H6KSDh.png)
-> :(
![](I9ZoisX.png)

Instead, you want to do Grid Fill:
Viewport secondary menu Face -> Grid Fill

![](GFOgGvi.png)
