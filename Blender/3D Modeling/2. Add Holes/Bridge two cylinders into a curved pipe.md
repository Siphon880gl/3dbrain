
Status: Cursorily
From: https://www.youtube.com/watch?v=Aspm15I6WnI


![](6ygosJj.png)

->
![](0D7mYa4.png)

---

Join both cylinders if not one object

Edge select each cylinder's ending that you want to connect
![](HJksq7j.png)


Right click -> Bridge edge loops
At the Bridge Edge loops properties, incase number of cuts. And adjust smoothness at this properties to get the final angular space you want. Adjust profile (further distortion) if needed.

How to access properties:
![](yJte0TS.png)

![](LbDnL4v.png)
