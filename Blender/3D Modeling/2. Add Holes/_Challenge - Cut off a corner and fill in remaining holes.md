
Goal:
![](M9YODfB.png)

---


Clue:
Loop Cuts

You can cut in the other axis too:
![](50XfKmY.png)

Clue:
Select all faces of one sector:
![](IcZwGv4.png)
Separate by selection
Go into Object mode to move that sector out

Clue:
![](NijRga2.png)

Edge select the three edges here (Shift click)
![](r1Wy7oy.png)

Then f to fill

Close up the remaining with another fill (You can select 4 edges now)

![](ba3I9Vk.png)

Do the same techniques to fill in all holes:
![](Nkwd4EP.png)
