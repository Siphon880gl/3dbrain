

Volumetric shape with hole:
![](Gtsg2Mz.png)

In Edit Mode, with Edge Selection on (Pressed 3), then box selected this top awkward region that’s opened up (B, click and drag a selection box over the area).
![](iXXG6gM.png)

Edit Mode’s secondary menu → Edge → Bridge Edge Loops
![](at4ctul.png)

Final:
![](DY68KaM.png)
