
Side effect: Inside where the two spherical ends get bridged, there will be a tunnel through

Status: Cursorily

Obligated
https://youtube.com/shorts/c5HoO-AFJ8s?si=gxV4D8YHRppN0kPV

![](ntcirtA.png)


Edit Mode’s secondary menu → Edge → Bridge Edge Loops
![](HDiSL0n.png)

---

## Thicker bridge further from center

And if you had expanded the operator at the corner, and you tick on merge...
![](ZyEJVtm.png)

![](qgvbc4R.png)
It’d be an even thicker bridging with vertexes further away from the middle

---

## Tunneling side effect

See inside -

View from top:
![](apS9nJu.png)
