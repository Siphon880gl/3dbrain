
Select round / loop terminal edge from each open ended object:
![](L2TJ3ZU.png)

Viewport secondary menu item Edge -> Bridge Edge Loops

![](Gr8sZK8.png)

The operator box that appears at the corner upon performing the bridge edge loops - allows you to adjust the geometry of the newly bridged mesh:
![](VJoCYhK.png)
