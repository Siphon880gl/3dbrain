
Shift+Space for labeled edit menu -> Edge Slide

Edge Slide tool:
![](skdyio2.png)


![](n3WnOqF.png)

![](f77pW8g.png)


How it works: The four vertexes of the selected face are moved along the axis, rerendering the mesh each time there are new coordinates

---

You can shorten. But if you try to make taller, it'll do this, unfortunately:
![](oM8B0GH.png)

Then in order to change back into a cube, you'd have to rotate (r) that face among (z), then push back out that face (Push/pull)