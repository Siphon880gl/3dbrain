![](07WFh9S.png)

See if you have a end facing faces at the end. Here it’s not grids but it’s zig zags, so you want to select those faces (Edit Mode → Faces 3) and delete them (X)

![](7lfzjeC.png)

![](IBV3wc7.png)

Do a grid fill for a perfect gridded end:
Select all edges

![](8OCPDQM.png)

![](x37owwV.png)

Edit Mode’s secondary menu: Face → Grid Fill

Make rounded cap:
Select middle grid, move it out, then

![](lGYLH2T.png)

Then select all entire cylinder object and apply subdivision modifer level 2
![](VOkJo61.png)
