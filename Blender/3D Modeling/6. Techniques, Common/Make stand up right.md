
![](zfXFY3n.png)

Go to object properties. Change Y rotation to 90

![](HmqJh3s.png)

Snap menu (Shift-S) -> Cursor to World Origin
Rationale: We'll go into a view and need the 3d cursor to help us visualize axes that turned invisible

Viewport angle from left -Y (Numpad 1). Orbit left or right slightly. Your 3D cursor will appear.
![](HjIoESB.png)


![](XRK25eh.png)

Select object again. At Object Properties, slide the Location Z until the item is at and above the 3d cursor.

Scroll your moues scroll up and down so the grids appear again (they were perfectly aligned with your screen to the point they went invisible). You'll see your flat item is now upright and standing on floor
![](ZSYCGWv.png)
