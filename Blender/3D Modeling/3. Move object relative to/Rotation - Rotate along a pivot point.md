Lets say you want the object to move along a pivot point you wanted.

Set that pivot point by setting the 3d cursor (SHIFT+Right click). In this example, it’s been set to the right of the cube:
![[Pasted image 20250201052756.png]]

See that orange dot? That’s where the pivot point for the rotation of the object is.

Lets move the orange dot to the 3d cursor:
You can set the orange dot  (origin point of the object) to the 3d cursor
While in Object Mode with the object selected, go to secondary menu Object → Set Origin → Origin to 3d Cursor
![[Pasted image 20250201052816.png]]


You should see both orange dot and 3d cursor in one spot:
![[Pasted image 20250201052825.png]]

And rotation will be what you want:
![[Pasted image 20250201052839.png]]

It actually rotates along the orange dot which is where it’s location is:
![[Pasted image 20250201052852.png]]