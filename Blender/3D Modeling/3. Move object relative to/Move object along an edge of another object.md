
You can move this selected object along the edge of the other object
![[Pasted image 20250201053217.png]]

With it selected in Edit Mode, press G then B for baseline snapping. Move your mouse over an edge of the other object and click once to confirm edge selection.

As you drag your mouse, the object moves parallel to that edge. Cick again to confirm final placement.

![[Pasted image 20250201053230.png]]