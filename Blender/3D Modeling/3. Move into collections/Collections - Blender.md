
You can select multiple separate objects together, press M, then assign a new or existing collection, which reflects a folder in the navigator at the top right. Pressing m shows this popover menu:

![](26Xh72z.png)

And when you perform Boolean modifier, you can boolean difference against a collection (hence multiple cutters cutting into the body)

And you can logically think through because of naming:
![](QJfAHGe.png)
