
Delete the top face of the cube:
![](LHcKQNP.png)

Rip each side edge. Select all side edges. Press V then click to commit right away (do not drag away). This disconnects the side panels from each other. Theory: It duplicated the vertex in the same position, hence making them disconnected.

![](pNhYRP2.png)

Select each top edge and shrink/flatten (which is resizing along the normal) - with the top edges selected, press ALT+S, or press SHIFT+Space and select Shrink/Flatten. Drag outwards

![](kV9AvMD.png)

-->

![](2K5czeT.png)

