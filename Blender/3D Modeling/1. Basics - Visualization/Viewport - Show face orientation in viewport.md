
Purpose: Fix 3d printing and animation problems

![](WfPs8JC.png)

The faces are blue or red.

---

Here's a circumference shape:
![](ePbYbOv.png)

Let's say we remove all the faces on the interior of the circumference shape. This reveals the hollow space inside the shape, and therefore reveals the opposite sides of the faces. It appears red instead of blue:
![](4lEIlss.png)
