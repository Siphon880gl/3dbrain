
All these are numpad numeric keys:

Side view aligned to -Y or Y:
1 or CRL+1

Side view aligned to X or -X:
3 or CRL+3

Eagle view aligned to Z or -Z
7, CTRL+7, 9

---

Z rotate (airplane rolling):
4 or 6 while in Z view

Orbit vertically (In Z view)
2 8

Orbit horizontally
4 6

Trucking  (In X or Y view0
Ctrl 4 6

Vertical up and down (In X or Y view0
CTRL 8 2

Tilt up down (In X or Y view0
8 2

Useless: Airplane roll (In X or Y view0
Shift  4, or shift 6

---


Shift or CTRL or no modifier keys **on** 4,6,2,8, or mouse scroll usually produces different types of looking around:
- orbit
- tilt
- pan


---

Useless: 0 switch between orgo or perspective