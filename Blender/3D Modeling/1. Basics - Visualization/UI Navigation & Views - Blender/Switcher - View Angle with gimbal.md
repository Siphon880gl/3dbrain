This gives you more control over where you are creating/transforming in space:

Click Y, -Y, X, -X for quick views. 

Click middle of gimbal to switch between two views:
![](gX3Qxbv.png)

Click the right one to rotate around:
![](WpxzUHl.png)

---

## Advantages

Create your model while in any of views, then when you lock transformations/movements along an axis, it’s more predictable

![](JKsCMYC.png)

After a locked X movement, we positioned this example more midbody:
![](PTwib0a.png)

Going to front view easy - you can see some animal figure starting to form (maybe a rabbit, maybe a cat):
![](73pCSnk.png)

By clicking one of the gimbal icons and your model having been aligned to one of those views (because you created the model while in one one of the views), transformations become more predictable:


This also tells you when you lock after a G, S, or R, where the object will be transformed along:
![](e73Pr41.png)

For example, G->Y will mean you’re moving the model closer to the screen / further from the screen.

Pressing G->X will mean you’re moving the model along horizontally

![](jpqfxY4.png)


->

![](UWLsbhT.png)

After a lot of duplicating and reusing. You want to apply all scale so when you sculpt later, the brushes will work to scale of what’s on screen. CTRL+A → Scale