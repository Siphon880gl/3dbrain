
But you might not see the image texture in the actual model. Blender’s rendering is lazy. Turn on image texture mode here. 2nd to last option at top right of viewport:
![](R0sSuNL.png)

![](KBgJgzG.png)
