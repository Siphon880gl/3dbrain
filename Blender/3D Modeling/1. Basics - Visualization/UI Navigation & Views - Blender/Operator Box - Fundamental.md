
Learned from:
https://www.youtube.com/watch?v=wO_jXnolG-Q

Generally after using a tool, that tool’s name appears at a corner:
![](ofExGJE.png)

You can expand that name to access the settings for the action, retrospectively applying a variant effect from the tool:
![](GEETxSq.png)

If the operator box disappeared already, you can summon it with Edit → Adjust Last Operation
![](M11TIRv.png)
