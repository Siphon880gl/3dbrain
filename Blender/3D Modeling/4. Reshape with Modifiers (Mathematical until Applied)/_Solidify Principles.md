Following this tutorial will teach you how Solidify Modifier works.

---

Shift+A to create a circle
![](NrVl4mr.png)


In Edit Mode, will be selecting edges (**2**), and select all the circle edges (**A**) , then extrude along the Z-index (**E → Z**)
![](UgUt8KI.png)


In Edit Mode, will be selecting faces (**3**), and select all the circle edges (**A**) 

Add Solidify modifier at -1 offset (extend inwards hollow)
![](2gieUbV.png)

If it were Offset 0 (extend outwards and inwards hollow):
![](zWQG4M2.png)

If it were Offset 1 (extend outwards hollow):
![](JUZJy7X.png)


---

To learn filled and only rim, lets turn on Face Orientation in Viewport settings:

![](WfPs8JC.png)


**With FILLED OPTION:**
Here's a circumference shape, showing that fill adds shape to the extended faces and the original faces
![](ePbYbOv.png)

With **FILLED OPTION** and **ONLY RIM**:
That removes all the faces on the interior of the circumference shape. This reveals the hollow space inside the shape, and therefore reveals the opposite sides of the faces. It appears red instead of blue. That shows the original faces are removed and only the extended appear (the rim).
![](4lEIlss.png)


Note if you turn off Filled, you don't have the Only Rim option.

---

Complex with fill and only rim
Again, if you turn off Filled, you don't have the Only Rim option.

You can see that the faces added in order to fill between the original faces and the extended faces - those added faces are what appears.
![](Dnxfvfx.png)

Flipping it vertically so that the other filled faces are on the ground. This will be similar face orientations:
![](KBMNxXh.png)
