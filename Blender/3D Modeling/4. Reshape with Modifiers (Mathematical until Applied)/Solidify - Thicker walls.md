
Before:
![](Q5GUY9J.png)

After:
![](gxAScY4.png)


---


Instructions:
1. Hide any faces that will prevent you from box selecting all the walls:
   
   Multi-select with SHIFT+Click, then press H to hide (Later, you can press ALT+H to unhide)
   ![](1iujyGq.png)


2. Then with Edit Mode on Face selection, draw rectangular box to select all faces, and CTRL+F for face options → Solidify face
   ![](btOMPx1.png)



Final Results:
![](gxAScY4.png)

---


YES, this will add a Modifer into the Modifer settings