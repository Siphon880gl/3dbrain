Destructively means that you resized/thickened an item with S->X, for example; but only way to undo it is the undo command; when exited Blender, there's no going back without manualing it.

With nondestructive approach, you create a modifier that has data on your changes. You can toggle off or delete the modifier data. You are not manipulating the actual vertexes directly, but rather, you have the modifier data manipulate the vertex:

---


I have a flat surface aka mesh plane:
![](X3yRe7W.png)

With the object selected, I go into Modifiers properties:
![](vFY0UNj.png)

I add a Solidify modifier:
![](upmmpPQ.png)

Then I adjusted thickness:
![](U9oTPaT.png)

Now in the Outliner I have a new modifier item that applies this size transformation, that I can toggle off, remove, move to another object, etc:
![](2J7Xwif.png)
