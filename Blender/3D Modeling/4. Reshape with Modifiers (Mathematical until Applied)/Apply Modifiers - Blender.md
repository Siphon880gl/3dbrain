
**TLDR**: 
You apply the modifier in object mode:
![](hhaRKOA.png)



---

## Applying modifiers

It’s like baking modifiers

Lets say the the legs on this cat model have been mirrored via a Mirror Modifier. Now you join the legs to the body by clicking the legs, then Shift clicking the body, then right click → Join. 

Oh no, the modifier disappears! That's because after joining them into a whole new model, the last joined is the one that passes the settings down (in this case, the torso that doesn't have Mirror Modifier).  Had you clicked the mesh without a modifier (torso), then clicked the mesh with the modifier (legs), then joined - then the mirror modifier carries over to the newly joined model.

Once you apply a modifier, its settings disappear from the object’s modifier settings; it’s become a permanent mesh for good or for bad.

So a mirror modifier on one designed leg appears as two legs:
![](huxQuh4.png)

--> becomes one legged after joining (CMD+J or right click multiselected objects -> Join), all modifiers are removed:

![](sGZhcRl.png)


Another case in point:
Sphere created with CTRL+2 in object mode for surface modifier level 2, so you created a sphere. Once you join these two items, the surface modifier resets
![](a8J3PGE.png)

Once joined (CMD+J or right click multiselected objects -> Join), it resets the surface modifier so no more sphere -->
![](6pXysfC.png)



Therefore you have to bake in the modifier (Eg. Mirror Modifier) first

How to bake/apply the modifier: Under the leg’s modifier settings (wrench), you click down caret to apply over at Mirror.

You apply the modifier in object mode:
![](hhaRKOA.png)

Now you can join leg to torso without the mirror modifier disappearing because the leg’s mirror modifier would be applied (aka baked in):
![](huxQuh4.png)
