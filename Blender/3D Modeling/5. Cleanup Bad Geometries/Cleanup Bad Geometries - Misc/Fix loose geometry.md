What is it:
Any unconnected vertices, edges, or faces in your model

Not necessary but if curious, while in Edit Mode -> Secondary "Select" -> Select All by Trait -> Loose Geometry
![](5MYk3Jr.png)

To clean loose geometry:
Mesh -> Clean Up -> Delete Loose
![](rePy6M8.png)
