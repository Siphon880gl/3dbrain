If faces on your model is preventing you from accessing the faces/vertexes/edges inside the model, you can temporarily hide the outside faces

Select face then press H.
ALT+H to unhide all.

---

For example, we probably need to fix this shape, but the faces are in the way:
![](Q57zzvl.png)

Selected the top layer faces and hidden them:
![](CGVIsvQ.png)


Now I can fix the inner geometries:
![](Q6LVXjP.png)


![](v7k7aDa.png)

![](RyT9O9W.png)
