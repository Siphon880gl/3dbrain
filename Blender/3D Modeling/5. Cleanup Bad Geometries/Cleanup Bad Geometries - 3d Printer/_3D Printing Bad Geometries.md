3D Printing Bad Geometries
## Introduction

Open the 3d Print's Geometry checks. Refer to [[3. Check for bad geometries]]

You're constantly checking for errors as you're fixing them (to see if it's getting worse or better)

![](Or1z8gk.png)



Here's a quick summary of bad geometries that 3d Print Tool reports:
- **Non-Manifold Edges**: Checks if your model is manifold (free of holes / exists in reality).
- **Intersect Faces:** Identifies intersecting faces.
- **Bad Continuous Edges**: Adjacent faces facing opposing views, so the edges shared between them are having continuity problems
- **Zero Edges** / **Zero Faces**: ... Refer below
- **Thin Faces**: Refer below
- **Non-flat Faces**: ... Refer below
- **Sharp Edges**: ... Refer below
- **Degenerate Faces:** Detects problematic faces that could cause printing issues.
- **Overhangs:** Checks for overhangs greater than 45 degrees that may require supports.
	- You will try to minimize Overhangs in Blender, but the slicing software (Like Cura by Ultimaker) can automatically add supports.
	- Without proper support, during 3d printing, these corners, especially 90 degree corners, could end up collapsing or sagging. Often there's an infill support that you may design to be removal

---


## **Automatic Fixes**

After running the 3d Print Toolbox's **Clean Up** option, at the subpanel "Cleanup":

1. At 3d Print Cleanup sub-panel, if there are Non Manifold Edges errors:
	- Click **Make Manifold** to automatically attempt closing holes and making the model solid.
    - After this, if you find the "Check All" errors increased (Click "Check All" again to re-check), just hit undo and skip to the next step.
    - Or if your shape looks messed up, just hit undo and skip to the next step.
      
2. At Mesh Cleanup, run through two rounds of actions:
   
   ![](wfDa810.png)

   
	- Mesh -> Clean up -> All the actions from top to bottom. 
		- Undo (CMD+Z) an option that messes up your shape or increases the "Check All" errors (Click Check All after each action). Proceed with the next action on the Mesh Cleanup menu.
	- After one round of mesh clean up, repeat one more round from top to bottom. 
		- Same - if an action messes up your shape or increases the "Check All" errors - then hit Undo (CMD+Z) and proceed with the next action on the Mesh Cleanup menu.
3. Then, attempt to fix each error at "Check All" individually. Also, you can click one of the error names to select those errored vertices/edges/faces. Refer to the below section "Manual Fixes"

---


## Manual Fixes

Note on these errors, one error may have more than one way to solve, and you may need to go through all the approaches until one approach solves it. Blender does not subclassify the error any further.

### Non-Manifold Edges

In the simplest terms, a manifold solid would not leak if you filled it up with water.

You can have, for example, 6 perfectly aligned planes that look like a cube but are not. It will have 24 vertices, not 8, i.e the corners are not shared, so, not manifold.

You can download complex models that look like they're manifold when they're not when you check the vertexes more closely and see that grabbing and moving the vertices does not deform the shape (but rather leaves behind holes).

In edit mode, select > select All by trait > Non Manifold. This will show all the holes. Deselect every hole but one, press F to fill. Repeat until all holes are gone.
![](Rk2LTSm.png)

You can also try the "Make Manifold" button in the 3d Print Toolbox subpanel "Clean Up" which attempts to fix all manifolds in the entire object (by filling holes, flipping surfaces that are causing bad contiguous edges, etc.).


---


### Intersect Faces:

Bad:
![](yUMNHKy.png)

Good:
![](61IJmci.png)


---

### Bad Contiguous Edges:
![](ExkgDgp.png)

Show the face orientation at Viewport settings -> Face Orientation
![](6WWGokK.png)

Select the red normal facing (You had viewport overlays enabled Face orientation overlays). Select the red face → Mesh → Normal → Flip

---

### Zero Edges

**Try all approaches**. Try all approaches until one fixes the error.

#### Zero Edges - Approach 1: Merge multiple vertices

Toggle on Zero Edges at report, and you see **highlighted edges** at both ends of a face. It’s not exactly symmetrical along a face (could be on an adjacent face’s end instead), however it’s a problem of having thin faces between adjacent faces! Doing this fix reduce both zero faces and zero edges

![](VJpTYeq.png)

![](2mDVfpo.png)

For those three vertices: Merge at center: m->center

Likely have to do the same at the other end of that face

![](uMDBDGP.png)

#### Zero Edges - Approach 2: Double Vertices in the same coordinate

Double vertices in the same coordinates (therefore those two vertices connected to create an invisible 0 length edge). Remember that when you clicked the error, it selected it. You can merge by center or collapse after pressing m on the selected double vertices.


#### Zero Edges - Approach 3: Make longer slope

Let's focus on this in front of us:
![](NYFEy0f.png)

Select the edge and slide it a bit to create a less sharp angle (Use Edge Slide tool).
![](PEpGCrG.png)

![](U1NhUpM.png)


---

## Non-Flat Face

Here clicking Non-Flat Faces highlights two faces:
![](mUpX5ZI.png)

On one of the faces, if we unhighlight, we get to see the problem:
![](H2aIdOx.png)

![](htM7EYT.png)

There are two unnecessary vertices that could be making this face non-flat. Select both vertices:
![](HEIR4Rh.png)


With the two vertices selected:
Merge (m) → Collapse

![](1yyB3r1.png)


Now there is one less redundant vertex. You may choose to collapse that redundant vertex with one of the corner vertices.


---

### Zero Faces
- **What it means:** These are faces with no surface area, often caused by improper geometry or unnecessary elements.
- **Note naming**: If researching online, **zero faces** and **zero-area faces** refer to the same issue. These are faces with no measurable surface area.
- **Try all approaches**: Try all approaches until one fixes the error.
![](C9gQ2vg.png)

- **Approach 1:** **Faces with zero-area**
    1. Select your object in Edit Mode (`Tab` key).
    2. Enable **Mesh Analysis** by pressing `N` > **Viewport Overlays** > **Face Orientation**.
    3. Locate the zero-area faces (they might be very small or invisible).
    4. Delete them:
        - Select zero-area faces manually or use **Select** > **Select by Trait** > **Faces by Area**.
        - Press `X` > **Faces** to delete them.

- **Approach 2: Degenerate Dissolve
	- First select all faces (typically selecting the entire mesh is fine).
	- Access this menu item from the 3D View: `Mesh -> Cleanup -> Degenerate Dissolve` to remove zero area faces, zero length edges.
	- Using dissolve is useful because it won't make holes where the faces used to be, it simply removes the face and merges surrounding edges.


- **Approach 3: Simplify the geometry by removing extra vertices
![](wUabIGz.png)

![](v6Ye5vs.png)



- **Approach 4: Simplify the geometry overall in a localized area

	- Selected all inner faces of his negative boolean modifier applied area
	
	![](9kcvaZv.png)
	^ Then merged by distance
	
	- Note if you make merge distance less precise... you get less zero faces error, but at the cost of simplifying mesh to look jagged:
	![](tm1cRo8.png)
	
	- Toggling Zero Edges show edge problems which also gets fixed by Mesh → Cleanup → Merge by Distance, the less precise, the more goes away:
	![](aaEOOTq.png)
	
	![](FekBuqF.png)

---

### Thin Face

- **What it means:** Thin faces have a width below acceptable thresholds for 3D printing. They may not print correctly and could cause weak spots in your model.

If you know your 3d printer can handle the current width of your walls/faces, you can ignore this error by adjusting the thickness threshold:

![](3RRLbmD.png)

Otherwise, you need to extrude/scale face(s), or solidify simultaneous faces with solidify modifier’s thickness.
        - Use **Extrude Region** (`E` key) or **Scale** (`S` key) to widen them.
        - OR: Go to the **Modifiers** tab, add a **Solidify Modifier**, and adjust the **Thickness** parameter. And apply the modifier once satisfied (applying is necessary before exporting the 3d model).



---

### Sharp Edge

![](JtGjX0J.png)



---

### Overhang faces

**What it means:** An overhanging face exceeds the 45° rule and may require support material during printing.

**3d print related**: Blender is a very powerful tool to modify 3D files, but it does not (as of 1/2025) have a visualization of what is an overhang. It is good design to _think_ of areas that will become overhangs in printing, but it's ultimately the slicer that will show what is an overhang and what not.

Click the error label "Overhang Faces" to select all Overhang Faces.

You may follow this video or continue the tutorial:
https://youtu.be/0rgrLWFUjlk?t=694

**Approach 1: Introducing more slope:**
![](mIVVrBO.png)


False overhang faces:
Note not all overhang faces are true overhang faces. The bottom overhang here will be on the printer plate so it's OKAY!

Fix true overhang faces:
![](XeaMByn.png)

Now you should be good

CAVEAT:
There's a bug in Blender where if you're working at mm precision, and your object is passed 2000mm from the world center, it'll report thin faces that don't exist. Just move the object to within 2000mm from the world center (Youtuber Keep Making shows this bug workaround: https://youtu.be/0rgrLWFUjlk?t=751)

**Approach 2: Introduce more geometries**
    - Use **Bevels** (`Ctrl + R`)
	    - **Why**: Smoothens sharp corners and transitions
    - Add **Edge Loops** (`Ctrl + R`)
	    - **Why**: By adding edge loops near overhangs, you create more vertices that can serve as anchor points, supporting the overhanging structure better.
	    - **Reduce Sharp Angles**: Place edge loops near steep overhangs to distribute the geometry and reduce the steepness of the angle.
	- Add **Inset Faces** (`I`) to reinforce overhangs.
	    - **Why**: Reduces the steepness of overhangs by shrinking problematic areas.
	    - **Use Smooth Shading**: If the inset changes the shading, switch to smooth shading for a seamless look.
	- Add **Subdivisions**: (Right click -> Subdivide)
		- **Why**: By adding more vertices and edges, subdividing allows for finer adjustments to the mesh. This can help smooth out steep overhangs and create gradual transitions.
		- Use with **Subdivision Surface** (Ctrl+1 or 2, etc, or add Modifier: Subdivision Surface)
		- Use **Smooth** to refine shapes and further reduce overhang issues.
		- Note if 


**Approach 3: Relegate it to the slicing software**
	- Add **support structures** in your slicer software if geometry changes aren’t feasible.
	

----
---

## Aim for as little as errors possible

As you fix the geometries and you click he "Check all" at the 3d Print Toolbox panel at the top right, then you'll see through time, that the geometry errors decrease (Look from 1 to 17)

| 1                | 2                | 3                | 4                                    |
| ---------------- | ---------------- | ---------------- | ------------------------------------ |
| ![](E8uhYYe.png) | ![](KVDRdJb.png) | ![](YYSzmaN.png) | ![[Pasted image 20250212010558.png]] |
| 5                | 6                | 7                |                                      |
| ![](IjHnVXG.png) | ![](wJaSqZp.png) | ![](HYFXWPG.png) | ![](OmhpmXp.png)                     |
| 9                | 10               | 11               | 12                                   |
| ![](pGBJ7cQ.png) | ![](O08UwFO.png) | ![](owOFgNx.png) | ![](yuMZp0X.png)                     |
| 13               | 14               | 15               | 16                                   |
| ![](XJx2kCY.png) | ![](ILiCyq4.png) | ![](xQz5KD2.png) | ![](VJ19zqN.png)                     |
| 17               |                  |                  |                                      |
| ![](Sg45SY8.png) |                  |                  |                                      |
No - you do not need 100% removed all errors from 3d print toolbox’s report. But it increases chances of a good print. Even having some overhang faces is fine because your slicing software that will convert the 3d model file into a gcode for your 3d printer will have automatic support generation.

Note Blender may have false Overhang Faces because it won’t know you’re using a plate for the 3d printer.