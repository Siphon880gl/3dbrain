Are you struggling with finding your objects (even with numpad period .) and objects you create with Shift+A needs to be found and at the correct zoom level?

Here’s how to simplify editing by having 3d cursor, viewport, and object at 0,0,0

Set 3d Cursor to 0,0,0 at numeric menu (Press N) and going to View side tab:
![](G4hePLI.png)

Above those options, lock view to 3d cursor:
![](NJ1qUxp.png)

For your object, set location to 0,0,0:
![](r3IwCBS.png)

If still can’t find the object, zoom in and out until you find it. Or, change the dimensions (still on the same options panel):
![](lcftzkT.png)

If you change dimensions, make sure to apply scale with Object Mode: CTRL+A → Apply → Scale.