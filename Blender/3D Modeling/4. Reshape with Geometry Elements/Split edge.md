Here we split an edge of a cube in two:
![](T0wpUeQ.png)

Consequently, there's a vertex midway:
![](YExanvy.png)


---

How to do (different approaches):
- In Edit mode with edge selected, use Bisect tool and drag a cut in the middle
- In Edit Mode with edge selected, right-click, then click Subdivide