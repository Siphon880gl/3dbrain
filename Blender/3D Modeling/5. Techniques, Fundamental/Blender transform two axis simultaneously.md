
Translate along two axes simultaneously
Rotate along two axes simultaneously
Scale/resize two axes simultaneously

Lets use scaling as an example:
You press **S → Shift+the excluded axis**

Eg.
**S -> Shift+Z**

For example:
![](9VG1Rxu.png)

You can resize it into a thinner diamond by resizing X and Y axis (Object Mode → S → Shift+Z)
![](XTqINUX.png)

Yes this can apply to moving/grabbing (G).