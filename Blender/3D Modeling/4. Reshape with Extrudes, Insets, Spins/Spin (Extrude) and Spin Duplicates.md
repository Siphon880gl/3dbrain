
Lets have a right edge of a planar mesh selected:
![](u1NU7vM.png)

Then turn on spin tool (which will extrude)
![](6BoLVvu.png)

Here you can choose the axis:
![](4cspLm5.png)


Choosing X, then click-dragging a plus handle
![](J860990.png)


Choosing Y, then click-dragging a plus handle
![](pn1pl7p.png)


You can choose more than one axes to simultaneously spin by holding SHIFT and selecting another axis.