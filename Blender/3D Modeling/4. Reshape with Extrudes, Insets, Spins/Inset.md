
Edit Mode -> Inset (i)

![](T45d1p8.png)

Creates another face inside the face. You'll have to move the mouse which resizes the inset face, otherwise it appears as if nothing happened when in fact a new face is overlapping with the old face.

How it works: New face duplicates and resizes parallel to the face mesh but old face mesh keeps intact.

---

Often coupled with:

Extrusion either direction:
![](9Yil38t.png)

![](bddBD0C.png)

Which can create holes/windows or overhangs/smaller structures.