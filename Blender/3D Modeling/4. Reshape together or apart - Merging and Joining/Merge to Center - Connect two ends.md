
Learned from:
https://www.youtube.com/shorts/JkPX21_rY_M

Example:

![](Jd1kZeu.png)


Becomes:
![](DNA2lXk.png)


---

![](jf6MTbG.png)


Becomes:
Select the two vertexes that merge into one vertex:
![](3bEKf0T.png)

The press m for merge -> to center

Repeat on the other two vertexes that need to be merged into one vertex
![](6KhR4eJ.png)


Result:
![](1KbjXzT.png)

---

Using same technique you could:
![](E3xAS3n.png)

![](7YIUUEj.png)

Result:
![](Wsbr5SK.png)
