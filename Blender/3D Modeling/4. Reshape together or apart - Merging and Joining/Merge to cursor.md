
Lets build a pyramid out of a cube

Set 3d cursor (Shift+Right click) to above a cube. Try to aim for center hovering above the cube

![](zdqa7Wd.png)

In edit mode, select the top face
![](DtQpgTk.png)


Press M for merge -> Select to 3d Cursor
![](Tk2AxHh.png)


While in Edit Mode, select vertex (1) at the apex of the pyramid. Press G, then grab move it into a position where the pyramid looks more natural:
![](yzFKtV5.png)
