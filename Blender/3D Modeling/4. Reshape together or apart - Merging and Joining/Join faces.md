

Lets say you want to join the two faces on left:
![](Qtw9Bh0.png)

![](WVeGtej.png)


Press F with the two faces selected to join them into one face:
![](Xio5wo9.png)

Result: But it doesn’t get rid of the extra vertices:
![](Xtnyqw4.png)
^ Cosmetically, the edge is gone. But there are still vertices

You have to dissolve them by selecting the extra vertices → Mesh → Delete → Dissolve vertices
![](pPrTY2d.png)

---

Dissolve vertices also works like this:
![](2o3uFOJ.png)

![](XjvpnaO.png)


