Final result:
![[Pasted image 20250201042404.png]]

Add cylinder (Shift+A) at 0,0,0

Select one end's face (Edit Mode -> 3 -> Select face). Make sure the face is still selected (when viewing from the plane, you see the edge is highlighted, selecting that the face at the end is still selected):
![](Vv2y7f0.png)

Get into proper view with numpad 1, 3, or 7.
![](65zEEKu.png)

Set 3d cursor at pivot like so (Shift+Right click. If doesn't let you, you probably activated the Spin Tool too early - go back to Select tool):
![](uUC3jBG.png)

Activate spin tool:
![](gPZycac.png)

Drag radial handle to extend the tube in a curve.
Here we know that the blue Z handle is not correct. The axis overlay missing is the green Y axis, because we're dead centered looking at it, making the Y axis collapse into visually non-existence. Pan around to reveal the green axis (Reminder: To know what colored axis is X/Y/Z, refer to the Gimbal at the top right)
![](zw1GHTS.png)

^Here I am dragging along the Y axis. See that Y handle is green, Y is "ON" at the top left Spin Tool options, and that the Y axis guide overlay is "coming at you":
![](pnzAbDi.png)

^You can change which radial handle appears by switching to that dimension at the Spin tool's options top left:
![](jfcOXPk.png)


Adjust the spacing inside the curve by dragging these handles
![[Pasted image 20250201042431.png]]

![[Pasted image 20250201042440.png]]


Final result is:
![[Pasted image 20250201042449.png]]