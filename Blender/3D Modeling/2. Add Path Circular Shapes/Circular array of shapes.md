Press 7 for Z view. Select all in Edit Mode on the cube object

![[Pasted image 20250201045741.png]]

Set the 3d cursor to where would be the center of the circular path
![[Pasted image 20250201045753.png]]

With all of the object selected in Edit Mode, open extrude menu with ALT+E, then click Spin
![[Pasted image 20250201045811.png]]

^If however you get this, you may need to adjust at the Spin tool options at the bottom left:
![[Pasted image 20250201045819.png]]

By not ticking “Use Duplicates” and/or decreasing the steps, you can have more spaced out cubes:
![[Pasted image 20250201045826.png]]

If you want to adjust the circle size a bit:
![[Pasted image 20250201045840.png]]
