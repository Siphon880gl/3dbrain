
Scenario:

You created a new vertex with ctrl + LMB but it's just floating in space and not contributing to the geometry or look of your model

Here you see the new vertex to the top left of the cube (audience view).

![](uHzsPAd.png)

---

You can connect the cube to the new vertex using an edge or a polygon

An edge: Be in Edit Mode (Press tab), Vertex select mode (Press 1). Select a vertex from the model and select the floating vertex together (Shift click). Then press F for fill. It'll fill an edge

![](QiRqWAP.png)

![](lYcoxrx.png)

---


A polygon face: Be in Edit Mode (Press tab), Vertex select mode (Press 1). Select two nearby vertexes from the model and select the floating vertex together (Shift click). Then press F for fill. It'll fill an edge.

![](GlWQvtY.png)

![](AF7bc9d.png)
