
![](vxJLbbe.png)

Edit Mode: With the 3D Cursor set at a mid corner, then I press A to select all

Then I used spin tool, lock onto X axis only, then spin along
![](xVqpCuD.png)

In addition, I can set the number of steps for more geometries (which allows me to further re-shape the model later, or have the model look more smooth when rendered rather than boxy):
![](btsVPGh.png)

![](rqIzzge.png)
