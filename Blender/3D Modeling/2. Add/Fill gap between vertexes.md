
## Triangular Face

![](1zcLC7E.png)

![](JRRCRoM.png)


Here we have three vertexes selected.
Press f for fill. 

That will bridge the vertexes FILLING IN mesh into a triangular face

---

## Rectangular face

![](tMeo2cd.png)

![](G0elo2I.png)

With four vertexes selected, F will fill in a plane mesh

---

## Edge

![](iWflSKa.png)

![](ml4opEm.png)

With two vertexes selected, F will fill in an edge


---

## Create-and-connect way.

For even faster creation of edges and rectangles, especially if connects to a previous vertex or edge.

If you have a vertex or edge selected:

![](2MF1OHs.png)

You can create another vertex with CTRL+RMB, and it'll fill in an edge right away (no need to press F)

![](LL4HkGi.png)


Similarly can be done for rectangles, if you had an edge selected, then you create a new vertex with CTRL+RMB2

![](Qsg8RZ2.png)
