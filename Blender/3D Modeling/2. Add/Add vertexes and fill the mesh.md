
While in Edit mode, CTRL+Right click at an empty space
(Mnemonic: Shift+Right click was for 3d cursor, but Ctrl+Right click is for adding vertex)

![](DIaUwrz.png)

Then select the three vertexes in that area, and press f to fill.

![](8FqFoOA.png)


![](XV3N5cC.png)


Challenge:
Can you create a triangular wedge connected to an edge on the cube?
![](6zpb3sR.png)

---

If you had one or more vertexes selected, then CTRL-right click an empty space, it'll immediately create an edge or a filled polygon to the new vertex.

---

If you want to add vertexes on the mesh, you have to make it not select the mesh (otherwise you're force to create vertex on empty space then move to mesh). Turn off X ray so you dont see vertexes of behind the model, and turn off solids:
![](SuU6dLx.png)
Here X ray is off and we are in front wireframe mode

You may also want to snap edges to the project's surface:
![](jqP2UBL.png)
