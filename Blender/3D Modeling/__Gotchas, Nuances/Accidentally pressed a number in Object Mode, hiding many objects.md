
So you accidentally pressed 1, 2, or 3 in object mode when you intended to do that in Edit Mode (for switching to selecting vertex, edge, or face).

And now a lot of objects have appeared to disappear from the viewport. What you've actually done:

While in object mode: 
- 1 to show only first object (all else hidden at outliner top right panel). 2 to only show second object. 
- SHIFT+1 or some number to toggle hide or show for that object.
- Alt+H stills works to show all objects again (by unhiding).
