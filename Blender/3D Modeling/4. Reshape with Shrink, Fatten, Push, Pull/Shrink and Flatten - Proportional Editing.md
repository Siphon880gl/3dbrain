Proportional editing:

Not -
![](pekNm9q.png)

But what you want should be:
![](GOmUT5q.png)

^ And you adjust with Offset and Proportional Fall Off

---

![](uSe0TlC.png)

With Proportional Editing ticked on, you can choose to affect only the connected vertexes or also the unconnected vertexes. Eg. Ticking Connected means the proportional editing shrink/flat will only affect the selected vertexes that are connected and will not affect the eyes which are not connected.

https://www.youtube.com/watch?v=DLZrFZegi5U