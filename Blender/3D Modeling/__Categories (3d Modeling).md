
Reshape with Cutting is intentionally making cuts into a face eg. **Knife**. by cutting, you introduce more polygons.

Although "Reshape with Polygon and Topology" has cutting happening which also increases polygons - With the case of "Polygon and Topology", eg. **Subdivisions**, it's a more automatic process where you do not intentionally make cuts.

Therefore, those categories are made.