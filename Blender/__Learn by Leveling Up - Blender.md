
These collections is a dual level up and classification.

For finding help in a specific category, this is how:  [[Blender - How to use notes (Categories)]]

For leveling up:
- You should get the minimum basics of 3d Modeling learned at [[_ Basics Recommended to Get Started - Blender 3d Modeling]]
- Then you can learn the rest of the "3d Modeling/" tutorials following the sequence of numbered folders.
- And then you may choose to go onto 3d printing, sculpting, surfacing, shading, and/or animation, which might have their own sequence (numbered folders).

**Endgoal:**
The folder structure is designed to reflect specific actions in 3D modeling, such as Reshape with Cutting, Reshape with Rip/Shear/Separate, and Adding. Inside each folder are note files named after the Blender tools that accomplish that particular action.

For a practitioner nearing mastery of Blender, this structure provides a clear and intuitive workflow: first, conceptualize what you’re modeling, then determine the necessary actions, and finally, identify the appropriate tools within those actions. As you browse the notes, pay attention to the folder names and their contained note files—they serve as a guide to efficiently selecting the right tools for achieving your desired 3D model.

Here's an example folder (Action) with its file notes (Blender tools):
![](9uUU1Is.png)


Then next level is having a model in mind then knowing vaguely what actions are needed or precisely what tools are needed, and be able to reference these notes to reach your 3d modeling needs.

Take inspirations from your surroundings or internet photos, and choose something to model. Plan out how to model it and what tools to look up in these notes.