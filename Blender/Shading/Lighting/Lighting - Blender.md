Shift+Right click to set the 3D cursor to an area on the space. The light object you add will be added at the 3D cursor

![](MwAzAYu.png)

Light has Light Data properties
![](6pMlcpa.png)

Have viewport shading turned on to see the light’s effect on surfaces
![](FxcfALR.png)
