Aka Get Started

You add a series of inputs and outputs, transformative and generative processes, etc in a flowchart manner to affect shading/textures/materials in the viewport:
![](TD8lqlM.png)

And filled with image file:
![](s1ILIAr.png)
Access here:
![](TIj4SUg.png)

New:
![](kJbNCIb.png)

Shift+A to insert new card
x to delete card

Btw the above Shader nodes is one way to apply a background image to the skin of a model (Another way is to [[_Add an image to cube or wall at one face using an unprepared image]]

Notice that when you created the shader nodes by clicking New, there was a slot number. This created data inside the Materials settings for the object:
![](QkpgjoX.png)

Note if the texture is not showing on viewport (outside of Shader Node editor), make sure you've enabled material preview:
![](lHNvXxI.png)
