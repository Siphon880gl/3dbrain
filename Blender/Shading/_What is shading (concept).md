
Shading in 3D modeling is a complex process that determines how light interacts with surfaces to create the final rendered appearance. Here are the key components:

1. Material Properties
- Diffuse shading: How the surface reflects light in all directions (matte appearance)
- Specular shading: Mirror-like reflections and highlights
- Roughness/Glossiness: Controls how sharp or blurry reflections appear
- Transparency: How light passes through the object
- Subsurface scattering: How light penetrates and scatters within translucent materials

2. Light Interaction Models
- Phong shading: A basic model calculating diffuse and specular reflection
- Physically Based Rendering (PBR): More accurate simulation of real-world material properties
- Ray tracing: Simulates actual light paths, including reflections and refractions
- Global illumination: Accounts for indirect light bouncing between surfaces

3. Technical Calculations
- Normal mapping: Simulates surface detail without adding geometry
- Ambient occlusion: Darkens areas where surfaces are close together
- Shadow mapping: Determines which areas are in shadow
- Fresnel effects: How reflectivity changes based on viewing angle

4. Additional Effects
- Bump mapping: Creates the illusion of surface detail
- Displacement mapping: Actually modifies the geometry for surface detail
- Environment mapping: Reflects the surrounding environment
- Emission: Makes surfaces appear to give off light