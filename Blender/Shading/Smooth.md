
After changing the model's pose using vertices and polygon faces under Geometry Nodes, for instance, rotating the arms from A pose into I pose, you may get sharp extrusions

![](UOfHZvJ.png)

You can select elongated faces on one side of the sharp line, and the equivalent of two faces on the other side of the sharp line:

![](MzfNvgg.png)

Then use the soften tool
![](kQGwS42.png)

Drag the yellow picker far. The farther you drag, the smooth it becomes!
![](bYMgqCD.png)

Short: Another way is to use Sculpturing tab and its various sculpturing tools