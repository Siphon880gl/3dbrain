If applying Boolean modifiers, the solver must be exact (because exact geometry matters in 3d printing):
![](SQqj3Xi.png)


---

If using Bool Tool addon, make sure to configure it for exact solver:

Edit -> Preferences -> View Details for “Bool Tool”
![](3QrXhOj.png)

And make sure Preferences have Exact Solver:
![](l4jhxve.png)



---


If performing Box Carve, must be exact solver:
![](LKo5fTK.png)

