
Problem description:
You’re editing small mm objects for 3d printing AND the vertex wont move as you drag unless you drag very far (while zoomed out) in which case it snaps to a new position far away from where you intend to drag it to.

Solution:
You may have accidentally turned on Snapping either through shortcut or mis-clicking. So the vertex snaps to the next point or doesn't move it all if not close to the next point. Just turn off Snapping:
![](yyOkbLY.png)
