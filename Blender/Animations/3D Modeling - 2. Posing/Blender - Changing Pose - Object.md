
A part of the model might have no vertices or armature. Likely a component of the model, it can be transformed in Object Mode

For example, the hammer on this Thor model.

Object Mode → Transform
![](3C10WAZ.png)

![](lSvw5OX.png)

![](A9pJogl.png)

![](vTyDLrK.png)

You can also use the g, r, and s shortcuts, or the sequential shortcuts (g then x, g then y, etc) if want to constrain in a plane. Refer to [[Move a part of the model - Fundamental]]