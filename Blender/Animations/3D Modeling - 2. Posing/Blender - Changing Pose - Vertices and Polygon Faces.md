
If your model doesn’t have armature/bones/pose, but it has vertices and polygon faces, and you are changing its pose, you have to select all the relevant vertices and transform them as a group.

Use case:
**You have a FBX file from Sketch Fab, 3DFree, etc but the posture of the arms are wrong**
You want to pull the arms down to a more neutral standing position. Sure!

## Selection

You will select all the vertices/polygon faces that need to be translated/rotated together

Go into Edit Mode:
![](66WOKVW.png)

![](hT1sFGg.png)

![](6IEYs3D.png)

You can drag in the axes, zoom, and hand panning to orbit around the character
![](7v0lM5h.png)

You have to make sure you don’t miss any vertices/polygon faces
![](W5nY22K.png)

You select an area of vertices/polygon faces with:
SHIFT+drag over area

But when you over selected vertices that shouldn’t be included:
Shift+click can deselect the particular vertex

You can also use a paint action to add vertices (Like in Windows Paint). You click the Select Circle in Edit Mode, then click and drag over vertices (if adding to previous vertice selection, hold Shift while you click and drag)
![](1hhiXQi.png)

## Transformation

Once the vertexes are selected, you can transform:

![](pAlhoN0.png)

---

![](lBctLSA.png)

![](CsIqaZC.png)


---

![](OSETgGe.png)

![](M6Qav8q.png)

![](XTAWI47.png)

![](HYyOfZr.png)


---


Rotate:
![](zuhqTpF.png)

Move:
![](PNbEgZZ.png)

I missed a polygon on his left arm (off his wrist) - noo! Then you can either undo (there's Undo History) and restart this or try deleting that one-off vertex to see if it affects the model's appearance

You may find that after rotating and translating the arm, the shoulder might have sharp extrusions. Refer to this for fixing: [[Smooth]]