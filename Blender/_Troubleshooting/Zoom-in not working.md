Can’t zoom in anymore?  Try all these fixes in order from top to bottomuntil fixed:
- Make sure to select an object then press . (period key) to zoom in on object selected
- Make sure when you press N for side menu that View tab’s View Lock isn’t locked to any 3d Cursor or Camera to View. 
- Make sure when you press N for side menu that View tabs:
	- Clip start is precise enough that you can zoom in to the level you want to zoom in.
	- Eg.:
	  ![](vNLgsIS.png)
