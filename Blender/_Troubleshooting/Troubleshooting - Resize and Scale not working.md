While in Object Mode (press tabs), open Transform, and click Options
![](imtXzVK.png)

Make sure Locations is not ticked
![](QWQdicL.png)

Should work now.