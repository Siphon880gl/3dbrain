![](3au5zwG.png)



While in Object Mode, select the mesh in question.
Go in Sculpt Mode with that mesh selected


Then press R. Then drag and click to commit on a level that has the grid sizes you would like (they’ll create vertex points at the corners of the grid sizes)

![](95iZkXO.png)

Then press CMD+R to commit.

Go back to Edit Mode. Your model now has vertexes

![](0fcHhZy.png)
