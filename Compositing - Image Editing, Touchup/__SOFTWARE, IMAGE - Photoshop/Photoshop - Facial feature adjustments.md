
![](ltr8Ayp.png)

## Prerequisite: Enable the graphics processor

As a prerequisite to using Face-aware Liquify functionality, ensure that the graphics processor is enabled in your Photoshop preferences.

1. Choose Edit > Preferences > Performance.
2. In the Graphics Processor Settings area, select Use Graphics Processor.
3. Click Advanced Settings. Ensure that Use Graphics Processor To Accelerate Computation is selected.
4. Click OK.

These settings are enabled by default when you launch Photoshop for the first time.

## Adjust facial features using on-screen handles

1. In Photoshop, open an image with one or more faces.
2. Select Filter > Liquify. Photoshop opens the Liquify filter dialog.  
3. In the Tools panel, select (Face tool; _keyboard shortcut_: A). The faces in the photo are automatically identified.

Read: [https://helpx.adobe.com/photoshop/using/liquify-filter.html](https://helpx.adobe.com/photoshop/using/liquify-filter.html)