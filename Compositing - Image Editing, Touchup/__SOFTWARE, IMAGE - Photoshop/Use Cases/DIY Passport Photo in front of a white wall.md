
You took a pic in front of a white wall for passport photo (work or online passport renewal that requires submission of a quality photo)

but there’s shadows or the white wall isn’t white enough

----

1. Use PS 2025
2. Select subject
3. Duplicate it (it’ll have transparent around the subject)
4. Underneath subject layer, create white background
5. Check that now you have three layers top to bottom:
	- subject
	- white background
	- original picture
  6. Change opacity of the white solid layer so the subject doesn’t look so much like a cut out against a 100% opacity white. And at the same time, this makes sure the edge of the subject blends well to look natural.