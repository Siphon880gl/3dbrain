Drew a line to continue the red line at the bottom right (Tool is highlighted on the left):
![](V37X9Xo.png)

Made sure the tool setting's mode is on Shape, and I had stroke color maroon at width 2px:
![](4NlgCyz.png)

Final result - the red line is extended like desired:
![](sCeXAyt.png)
