Say you have this:
![](yBECAhM.png)


Black is hidden away so you CANNOT draw pixels into it. White lets you draw pixels into.


Setup the mask like so (many ways but this is one approach):


Right-click image and go to Mask All Objects:
![](w4MpslA.png)

The palette changes into this (note down caret means it's expanded)
![](iyxKiAD.png)

It's essentially a folder.
![](S4jAzRD.png)


Lets create a new layer inside the folder. You click the new layer button WHILE the masking palette is selected:

![](5KM7neG.png)

Select the new layer that's inside:
![](Ia4C4L4.png)


If you draw a horizontal line across the image, you'll see that only the white area can be drawn pixels into:
![](qZo0T84.png)
