You can invert which region is black and which region is white on the mask. That will affect where you can draw pixels into (white) and where you cannot (black)

With the mask selected in layers, press CMD+I. You will see the palette will switch black and white colors.