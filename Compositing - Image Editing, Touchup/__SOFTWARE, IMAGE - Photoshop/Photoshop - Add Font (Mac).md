
Look for font on font website, eg. Google Font Monsterrat
https://fonts.google.com/specimen/Montserrat
There's a "GET FONT" button at the bottom right.

Download, unzip, and install:
Right click each ttf file → Open With → Font Book

Once installed, double check Photoshop has the new font:
![](8nTwTdc.png)
