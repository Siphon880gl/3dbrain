
Window → Contextual Task Bar
![](JB9wUl5.png)

Select Subject:
![](n42CtRH.png)

Notice at the Layers panel, it's created a new layer from the hands only. See Exhibit 1 and 2--
- Exhibit 1:
  ![](KYgDDBl.png)

- Exhibit 2:
  ![](nmWQV9q.png)
