
Pathways: Follow this video or this text tutorial. Contents just rewritten.
[https://www.youtube.com/watch?v=sbS5oUuGbGY](https://www.youtube.com/watch?v=sbS5oUuGbGY)  especially 16:25

At the Layers panel, click the new Mask Layer icon to convert a picture into a layer. This picture would be partially overlayed into the layers below it. It'll be partially because as a mask layer, you will paint what disappears from view. The Mask Layer icon:
![](58fFRNd.png)

You will see a second "sublayer" on the newly converted Mask Layer:
![](xvFy5n1.png)


Select the second image “sublayer” by clicking it in the layer's tile
![](BVtOf3t.png)

Then paint black or white. White area allows you to draw pixels into. Black area does not allow you to draw pixels in.

Another benefit to mask layer painting rather than overlaying normal layers: By painting, you can create a more natural mask edge (eg. A person shapeshifting into a tiger from side to side)
