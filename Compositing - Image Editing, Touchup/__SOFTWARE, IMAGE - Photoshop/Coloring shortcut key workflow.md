Swap background/foreground:
- Click the x key to swap between foreground and background colors in the picker. The advantage is easily selecting the more relevant color on-the-fly when painting, which minimizes you having to mouse the mouse elsewhere to click another area, instead of focusing the mouse cursor on painting inside the image area.

![](hkPRcQu.png)

---

Change foreground color to the pixel's colors
Press 'I' for eye dropper. Whatever pixel you click with this tool on, that pixel's color will become your foreground color.
Use Case: A common need is painting more on your picture using other colors found on the picture.

---

Fill region:
Shift-F5 to open Gradient/Fill tool's dialog. Options include: Foreground/Background, or Color, or Black/White 