When creating a new layer from a current layer and copying any pixels that belong to that layer, Do not use shortcuts select all on a layer, then copy and paste, because it could be shifted a few pixels off - use duplicate instead. You right click the layer and go to duplicate:

![](z0A3mCg.png)

You may notice the shortcut here SHIFT+CMD+D, but that’s in fact customized. Photoshop doesn't have a default shortcut key for duplicating an image, but you can create your own custom shortcut. Here's how you can assign a shortcut key to duplicate an image. 

**Recommendation: Good one for Duplicate layer is SHIFT+CMD+D because the D can be remembered as duplicate, and when asked to resolve Reselect,  you can just set that to empty (because the Reselect command by default is SHIFT+CMD+D which conflicts with this recommendation, and Reselect is usually not a commonly needed shortcut)**

----

### Steps to Create a Custom Shortcut:

1. **Open the Keyboard Shortcuts Menu**:

   - Go to **Edit** in the top menu.
   - Select **Keyboard Shortcuts** or press `Alt + Shift + Ctrl + K` (Windows) or `Option + Shift + Command + K` (Mac).


2. **Navigate to the Command**:
   - In the **Keyboard Shortcuts** window, under the **Shortcuts For** drop-down, choose **Application Menus**.
   - Scroll down and find **Image** in the list, then click the little arrow to expand the options under it.
   - Look for the command ****Duplicate Layer****.

3. **Assign a Shortcut**:

   - Click next to **Duplicate** where it says **None** (if no shortcut exists).
   - Press the key combination you want to assign, such as `Ctrl + D` (Windows) or `Command + D` (Mac). Make sure it’s not conflicting with other important shortcuts.

4. **Save the Shortcut**:
   - Click **OK** to save the new shortcut and close the Keyboard Shortcuts menu.

Now, you’ll be able to use your custom shortcut to quickly duplicate an image in Photoshop!
