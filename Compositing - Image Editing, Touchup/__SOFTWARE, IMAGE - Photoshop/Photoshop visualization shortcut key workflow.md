## Zooming

CMD+/- to zoom in or out. Notice bottom left tells you the zoom level and the actual size:
![](ammhRsZ.png)

Mac trackpad also allows more precise zooming with the two finger spread gesture.

---

## Canvas

CMD+H to hide or show guidelines

If you want to change the canva outside region because your image is also the same shade of gray:
- Right click outside region and select desired color
- ![](C31gqsS.png)

- Result:
  ![](NKFIg22.png)

