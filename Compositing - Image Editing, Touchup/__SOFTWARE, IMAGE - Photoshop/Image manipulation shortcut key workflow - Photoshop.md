
Marquee (Select region of picture): M

Adjust the selection position: Drag or use arrow keys for micro drags while still in Marquee (M) mode.

Cut and move the region: V (move tool) on the selected region, then drag to new area
- If you want to drag in a line (so you wont be pixels off): Drag+SHIFT
- You could make micro drags by pressing arrow keys instead

Duplicate region: V (move tool) on the selected region, then drag+OPT dragging to new area
- If you want to drag in a line (so you wont be pixels off): Drag+OPT+SHIFT
- You could make micro drags by pressing arrow keys instead

---

Selected region into a new layer, **removing** the selection from the old layer: 
- With the region selected, SHIFT+CMD+J
- Equivalent to menu choices: Layer -> New -> Layer Via Copy
- Mnemonic on the Shift: It's an extra key because you really are sure about destroying a part of the old layer

Selected region into a new layer, **copying** the selection from the old layer: 
- With the region selected, CMD+J
- Equivalent to menu choices: Layer -> New -> Layer Via Copy

---

New layer (same size as the old layer):
- CMD+SHIFT+N

---

Fill region with color: 
- Approach Shift+F5
	- With selected region, press Shift+F5
	- Then you can easily choose Foreground/Background, or Color, or Black/White
- Approach
	- Press G to go into Paint Bucket Tool (Make sure it's not Gradient that's selected on the left toolbar because the G is two tools collapsed into one, and the active one is what's been selected and what G will use)
	- Click in the selected region