
With these shortcuts memorized, you can draw all kinds with paint and eraser

In **Photoshop**, you can easily change the brush size (or the eraser size) using shortcut keys:

- **Increase brush size**: Press the right bracket key `]`
- **Decrease brush size**: Press the left bracket key `[`

These shortcuts work for both the **brush** and **eraser** tools. You can also hold down **Shift** while pressing the bracket keys to adjust the **hardness** of the brush or eraser:

- **Increase hardness**: `Shift + ]`
- **Decrease hardness**: `Shift + [`
- Dont see any values in the toolbar updating? It’s not shown as a quick access item on the toolbar by default. You can open paintbrush options anytime to see the hardness did in fact change. When you’re more experienced, you wont have to because you’ll paint on the screen while changing the hardness (you can undo anyways)

- You can adjust the **opacity** by typing numbers on your keyboard:
    

- Press `1` for 10% opacity, `2` for 20%, and so on up to `0` for 100% opacity. Think 0 for reset to 100%.

- For affecting **flow**, 

- Press numeric keys while holding Shift key. Same press SHIFT+1 for 10% opacity, SHIFT+2 for 20% opacity. Think 0 for reset to 100%.
- To set more precise values, quickly type two digits. For example, typing `25` will set the flow to 25%. If you type too slowly, it’ll think you’re setting to 20% then finally setting to 50%.