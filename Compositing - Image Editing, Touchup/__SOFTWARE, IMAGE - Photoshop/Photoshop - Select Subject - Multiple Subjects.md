
Goal: Select multiple subjects.

Required knowledge: Know how to perform subject select. See [[Photoshop - Select Subject]]

---

For this layer, it will select the person:
![](Qa9twOh.png)

![](Zlk4FjN.png)

But let's say you want it to select the lamp? I won't. The other way is to create a layer that only has the lamp:

1. Select that area of the lamp (a), then perform a copy and paste. This creates a new layer (b) of just that lamp area (the rest is see through):
   
   \*a:
   ![](uoTd69K.png)
   
   \*b:
   ![](AxFIHnr.png)

Now with the lamp layer selected, clicking Select Subject will select the lamp!
![](J2npSiQ.png)
