
How to assign shortcut to different actions

2. Go to **Edit** in the top menu.
3. Select **Keyboard Shortcuts** or press `Alt + Shift + Ctrl + K` (Windows) or `Option + Shift + Command + K` (Mac).
4. In the **Keyboard Shortcuts** window, under the **Shortcuts For** drop-down etc, navigate to your desired command
5. Press the key combination you want to assign. Make sure it’s not conflicting with other important shortcuts.