Similar instructions to Photoshop's [[Photoshop - Add Font (Mac)]].

Just make sure to restart Illustrator for the font choices to be available.