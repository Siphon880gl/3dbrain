## Arc Path

There’s an arc tool at the toolbar. Use it to create a simple arc, doesn’t need to be the exact shape you want.

Then manipulate the arc into the correct path. Very likely you’ll have a L shaped curve. If you want more of a U shaped curve, there’s more manipulation. The below tricks will be sufficient.

There’s selection and direct selection tools. 
- With selection tool, you can select the entire arc and there are four resize handles at each corner. If you need to flip horizontally or vertically the arc path, you can do so by dragging a corner handle way beyond the space that the path takes.
- Direct selection tool allows you to select the individual nodes and move them

Using pen tool:  
- You can affect how much of an arc.
- With the pen tool, you can add another point. Usually with arc, you have better control if there are three points (left, mid, and right) (or top, mid and right). 
- Smooth vs angular nodes: The pen toolbar that floats on the path node lets you change from regular to smooth path at a node. This allows more for an arc than angular paths when at a node.
- When at a node using pen tool, you can drag the extended handle to manipulate the path before and after that point. 

---
## Arc Text

To add text to the path, have the TEXT tool active in the side toolbar, then move your mouse cursor over the path. If you see the mouse cursor change to a sine way icon, then click, then start typing. The new text will be along the arc path you created.

You may choose to resize or adjust spacing between the characters.