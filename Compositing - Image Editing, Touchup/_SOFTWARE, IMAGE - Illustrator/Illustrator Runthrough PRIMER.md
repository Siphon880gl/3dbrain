Aka Get Started

**AI:**
Suggested you combine with Firefly AI image generator which will have layers that you can edit in Illustrator

---

Illustrator has vectors and pen node editing.

It also has Layers much like Photoshop (Window -> Layers)

---

Selection Rectangle / Marquee Triangle (Shortcut: M)

Selection is a bit tricker. If you click and drag to create a rectangle selection, if you dont see that, then it could be a lot of visual elements are getting in the way. You may have to isolate layers (eye icon in Layers panel) and/or ungroup and/or ungroup all (Ungroup options are on right clicking) 