In photography, bokeh is the aesthetic quality of the blur produced in out-of-focus parts of an image, caused by circles of confusion. Bokeh has also been defined as "the way the lens renders out-of-focus points of light". Differences in lens aberrations and aperture shape cause very different bokeh effects

![](q8ERN7K.png)


![](gXa73om.png)
