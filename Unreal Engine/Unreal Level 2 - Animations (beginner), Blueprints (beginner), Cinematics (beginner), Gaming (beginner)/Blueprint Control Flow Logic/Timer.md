"Set Timer by Function Name" node.

It will call the function every X seconds.

---

Open Level Blueprint. After BeginEvent, start a timer that calls a function that then prints the time

![](aK7rmBu.png)


At the left My Blueprint (Window → My Blueprint)
![](DUnzCOl.png)

Double clicking the function at the side panel you define nodes for that function:
![](2BlNWk7.png)

Your nodes are Get Game Time in Seconds → String (Integer) → Print String

Reworded: There is a "Get Game Time in Seconds" that you can cast into a string for "Print String"