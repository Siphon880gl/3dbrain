
In addition, it can animate to move towards you

Can also do transparent images (extra steps)

1. Import image into Content Drawer

2. While at Content Drawer, right click the image so you can create a material out of the image.

![](rOFGSDd.png)

If image has transparent parts:
3. Modify material (double click the file in the Content Drawer). In the material editor, make sure Blend mode is Masked
   
   ![](akZdDQG.png)

If image has transparent parts:  
4. Also in material editor, make connect Texture Sample’s Alpha to Material’s Opacity Mask

![](MtzHhl4.png)

  

5. Create plane shape. In the plane shape’s Details, change material to your new material.
   ![](3TYusYX.png)
  

Don’t see your picture? Rotate around because you may be on its invisible side

Learned from:  
[https://www.youtube.com/watch?v=g9x1xljJau0](https://www.youtube.com/watch?v=g9x1xljJau0)