When I play a level in unreal editor, there are two circles at the bottom corners. How to hide them. I have to mentally ignore them. What do they mean and how to remove them?

Edit → Project Settings → Search: Touch Interface → Untick “Always Show Touch Interface”

The annoying circles:
![](rKFQpp7.png)
