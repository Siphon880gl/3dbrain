
In Unreal Engine, Static Meshes and Skeletal Meshes are both used to represent 3D objects, but they serve different purposes and have different capabilities:

### Static Meshes

1. **Definition**: A Static Mesh is a 3D model that consists of vertices, edges, and faces that define its shape. These meshes are called "static" because they do not change shape or move parts independently. They can be moved, rotated, and scaled within the scene, but their geometry remains constant.

2. **Use Cases**: Static Meshes are used for objects that do not require animation or deformation, such as buildings, furniture, terrain elements, and other non-moving parts of the environment.

3. **Performance**: Generally, Static Meshes are more performance-friendly for the engine to render because they do not involve complex calculations for animation or deformation. This makes them ideal for objects that make up the bulk of the scene but don't need to change shape or move parts individually.

4. **Features**: They can have materials and textures applied to them, can be lit by the environment, and can cast and receive shadows. They also support level-of-detail (LOD) models to improve performance at different viewing distances.

IMPLICATIONS: More performant 

### Skeletal Meshes

1. **Definition**: A Skeletal Mesh is a 3D model that is rigged with a bone structure (skeleton) that allows it to be animated. The skeleton is a hierarchy of bones that can be moved and rotated to deform the mesh in a realistic manner, simulating the movement of living beings or mechanical parts.

2. **Use Cases**: Skeletal Meshes are used for characters, animals, and other objects that need to move in a complex way, such as walking, jumping, or using tools. They are also used for objects that require deformation, like a flag waving in the wind or a character's facial expressions.

3. **Performance**: Skeletal Meshes are more computationally intensive than Static Meshes because they require additional calculations for bone transformations and vertex skinning (the process of deforming the mesh according to the skeleton's movement). However, Unreal Engine provides tools for optimizing these processes.

4. **Features**: In addition to supporting animations, Skeletal Meshes can also use Inverse Kinematics (IK) for more dynamic movement, morph targets for facial animation or other deformations, and can have different materials and textures applied to different parts of the mesh. They also support LODs and can interact with physics in a more complex way, such as ragdoll physics.

IMPLICATIONS: Skeletal Mesh has moving parts (eg. limbs). There’s a  “skeletal editor” that lets you repose or create animation inside Unreal by adjusting around the joints!

In summary, the main difference between Static Meshes and Skeletal Meshes in Unreal Engine is their ability to be animated. Static Meshes are used for non-moving parts of the scene, offering better performance for static objects, while Skeletal Meshes are used for animated or deformable objects, providing the flexibility needed for complex animations and interactions.