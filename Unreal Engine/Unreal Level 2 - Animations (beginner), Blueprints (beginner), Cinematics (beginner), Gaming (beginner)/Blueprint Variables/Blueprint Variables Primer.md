
Eg. Lets make a variable to an object in the level. I have a lightning asset in mind (it has alpha channels along a cylinder shape)
![](iSKRmar.png)

At the Blueprint, I create a variable on the left. I can name it Lightning and making sure it's the same **type** as that from the Outliner:
![](daU9tXG.png)

And I make sure it's public (See eye icon on the right):
![](ffUx3pW.png)

On the right: I compile, otherwise I can’t add the Default Value:
![](TZpVmwI.png)


Now I can select a Default Value based off of the Level's Outliner's same types:
![](L5gpNKq.png)


I can drag and drop from the left sidebar into the Event Graph to create the variable (get) or change its value (set):
![](ny78140.png)
