Other than integer, string, etc. Say you want a variable to text, camera, skeletal mesh, actor types of them, or actor blueprint types of them

Refer to [[Blueprint Variables Primer]] for the basics.

Here are additional things to keep in mind. For example I am going to save a variable to a Text Render Actor Blueprint (I had converted this object to Blueprint)

Make sure you have the variable type as the standard class, NOT object:
![](PyQFbSv.png)

Compile as usual so it allows you to set the Default Value

Look for the "Item Label" at Outliner:
![](6JUHnAY.png)

Select that item in the Default Value:
![](01GdqmL.png)
