To save X Y Z in a variable, the type is Vector
![](0sWXVn6.png)


You can easily copy and paste location coordinates from the editor viewport into the Default Value of the vector variables in the blueprint event graph
![](cKl05mk.png)


![](tgONsyM.png)
