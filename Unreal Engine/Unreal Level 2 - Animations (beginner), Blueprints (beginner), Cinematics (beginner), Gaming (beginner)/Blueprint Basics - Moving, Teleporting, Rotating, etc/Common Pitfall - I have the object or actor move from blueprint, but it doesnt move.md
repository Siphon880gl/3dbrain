
You’ve programmed for the object/actor to move from blueprint event graph? Check that the item is movable (not static):

![](MLTgBVL.png)
