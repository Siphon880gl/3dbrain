Future lesson will be in level 2 with physics and collusions so player can interact with door, and door has a limited range of motion (rather than 360 degrees)

Window → Place Actors
![](tgkPEGD.png)


At "Place Actors" panel, search for Cube because you can easily transform it into rectangular objects such as a door

Drag and drop into Level Design. Transform the cube into a flat, standing up object.

![](gpY9Rj4.png)

Rename in Outliner for easy searching later:
![](MuUEVR3.png)

---

Just a white door. Lets design it.

Go into QBridge, search for door, then download and/or drop the design you like into the Static Mesh of the cube “door” object. The setting is in the Details subpanel with the cube “door” selected from Outliner.

Go open up QBridge, download, then drag and drop asset into the "Static Mesh" right slot

![](rWUAaS4.png)

![](Z3vM6nj.png)

Result, this door against a forest wall:
![](zmQ0Bk7.png)
