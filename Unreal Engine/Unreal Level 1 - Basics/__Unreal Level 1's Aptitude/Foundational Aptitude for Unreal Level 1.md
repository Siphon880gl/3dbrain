
Spatial and associative thinking:
Able to reason through and visualize moving, resizing, and rotation along any axis. 
![](NVPnQcO.png)



Matching the desired axis' color in viewport to the axis setting's color in Outliner → Details. Click and dragging in the Details panel rather than manipulating object's transformation in viewport is less laggy and more precise

![](0qqNXaj.png)

Matching bottom left as well:
![](a9qQupt.png)
![](BQ8QGva.png)
![](ecJFxEl.png)



Multiple meshings: Combines two in a morphing way. Can imagine in your head how the result will be before the selections.

Programming thinking (aka Script thinking)
Thinking in terms of nodes at the Blue Print → Graph Event.

Node thinking:
able to think in formulas of how nodes connect to achieve a goal in unreal