
The technical notes are organized by categories that are ranked by increasing levels of difficulty. When looking for a specific help topic (for example, animation), think about if it's basic or complex, to know where to go:
![[Pasted image 20250304184330.png]]

Level up?
This wasn't designed for someone to quickly orientate themselves to Unreal by following tutorials in the sequence of numbered folders, like my Blender tutorials are.

---

Recommended to open on desktop. 

On a wide enough screen, you can see the topics aligned along with their levels. For instance, _Animation_ appears in both Level 2 and Level 3, clearly indicating that this collection of Animation skills is beginner friendly and that other collection of Animation skills is of advanced complexity. 

![](Rm1ll2w.png)

If using Obsidian to view the notes, resize the Navigation sidebar so that the titles are not clipped.