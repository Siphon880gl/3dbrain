

Swap out base color input with a material node called Constant3Vector

![](9YV1saF.png)


![](4s4ZWKw.png)

Mnemonic: This is a vector of 3 dimensions. The adjective is after the word constant.