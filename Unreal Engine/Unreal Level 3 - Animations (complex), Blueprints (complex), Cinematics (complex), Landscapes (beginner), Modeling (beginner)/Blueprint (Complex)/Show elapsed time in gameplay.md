Sometimes you want to show elapsed time, for example, discovering what are good timepoints for your animations

The problem with counting up is you'll see multiple lines on the same screen.

But printing a space effectively flushes the strings on the game like a console clearing. This assures only one line appears at any time

![](FPodgI0.png)


![](qqTJIsM.png)
