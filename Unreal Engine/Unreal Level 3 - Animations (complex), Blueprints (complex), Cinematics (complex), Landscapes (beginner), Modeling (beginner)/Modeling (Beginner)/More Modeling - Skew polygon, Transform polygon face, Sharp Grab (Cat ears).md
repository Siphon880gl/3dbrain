
## Unreal - Skew polygon face
Model
Polygroup Edit
Select a vertex, then the overlay gimbal lets you move, etc

![](QmTfPqK.png)

![](cqz9Wnd.png)

---

## Unreal - Transform polygon face

  
Transform a face

Mesh → Triangle Edit

![](h5XOyic.png)

---

## GrabSharp (Cat Ears)

Modeling Mode → Deform → Vector Sculpture → GrabSharp

![](ADet88E.png)
