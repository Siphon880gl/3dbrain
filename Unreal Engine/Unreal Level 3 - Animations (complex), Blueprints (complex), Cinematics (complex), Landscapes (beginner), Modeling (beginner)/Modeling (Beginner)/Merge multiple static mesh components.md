
Merge multiple static mesh components into one selectable mesh component

![](zXgfsML.png)

Shift click each

Modeling Mode → XForm → Merge
![](UcpzDLC.png)
