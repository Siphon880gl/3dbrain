
Similar to how you can keyframe camera location, rotation, scale - you can keyframe the joints/bones of a character that's been rigged, also by using the Sequencer

Fk control rig is basically bringing the bones/rigs into your sequencer so you can do key framed transformations

![](j1zoIQ5.png)


Right click
![](rhbEZn4.png)

Edit with FK Control Rig:
(Background: FK stands for forward kinetics. We are controlling the rigs aka bones/joints):
![img](T0MvLED.png)


Select Reduce Keys. Otherwise, you get all possible keys which becomes hard to manage

Open FKControlRig for all the bones:
![](QtuloP3.png)

Then keyframe the bone you want to position in time

If you’d like to save it as an animation file, go back to the right-click context menu after you’re done and select → Bake Animation Sequence
Btw, you can also bake poses (Bake Pose Asset)

---

If you want to loop:

![](TXNkMrK.png)

---

Learned from:

[https://youtu.be/MRppXvChNdM](https://youtu.be/MRppXvChNdM)



----

Background:

FK?


1. **Forward Kinematics (FK):** This is a method of animating where you manipulate each joint of a character or object directly. In FK, you rotate or move each joint individually, starting from the root (usually the hip or torso) and moving outward to the extremities. This method is often preferred for rotational motion, like swinging an arm or turning a head.
    
2. **Control Rig:** In Unreal Engine, a Control Rig is a scriptable rigging system. It gives animators and technical artists a more robust and intuitive way to create, test, and maintain complex rigs for characters and objects. This system is highly flexible and can be used to create rigs with custom behaviors and setups.
    
3. **Integration with Unreal Engine:** The FK Control Rig in Unreal Engine is integrated with the rest of the engine, allowing for real-time animation and rigging within the Unreal Editor. This integration helps streamline the animation process, making it more efficient and interactive.