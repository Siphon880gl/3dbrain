
https://fb.watch/oHzXTjlfOX/?mibextid=cr9u03
(Weng backed up as: Sample Light Quality Masterclass.mov)

Motivation, Sodium, egg crates, and water noodles lol

Motivate: In film, you're saying a light motivates a lamp if the lamp is meant to be in frame and appears to light up the character sitting next to the lamp. You're using an off screen light to light up the character, and hence the audience thinks it's really light from the lamp that hits the character.

“Sodium vapor lights”

The egg crate is the grid that is placed on the front of the softbox
It PREVENTS some of the dispersal of light
The egg crates still contain the light but wraps it a little better
And that the white grid creates a more rounded illumination.

In film lighting, motivation refers to the logical or realistic source of light within a scene. It's the concept of justifying where the light is coming from in a way that makes sense within the context of the story. Properly motivated lighting helps create a more immersive and believable visual experience for the audience. For example, if there's a window in a scene, the light coming through that window would be the motivation for the lighting in that particular shot.