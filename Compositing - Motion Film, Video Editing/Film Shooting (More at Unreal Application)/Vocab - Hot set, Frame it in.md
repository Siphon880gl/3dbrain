
it’s a hot set (dont be moving around/making noises/touching things - hot)

---

frame it in (figuring out the camera angle and whats on screen)

In filmmaking, the term "let's punch it in" refers to the decision to film a closer shot of a scene or subject. This usually means moving the camera closer to the subject or using a lens with a longer focal length to get a tighter frame. This technique is often used to capture more detail, emphasize emotions or reactions, or focus the viewer's attention on a specific aspect of the scene. It's a common practice during shooting to adjust the visual storytelling.