Crane: Moves the camera up and down while keeping its orientation along its Z-axis.
Dolly: Moves the camera forward and backward along its local Y-axis.
Truck: Moves the camera left and right along its local X-axis. Truck is textbook. In practice, its just dolly left and dolly right.
Orbit: Rotate around a subject or a scene, giving a gyroscopic feel to the camera movement. It's a common feature in 3D software for inspecting objects from various angles or for navigating around a scene. It’s a relatively newer concept, some people calling it gyroing or arching.
Tilt: Mouse scroll up / down