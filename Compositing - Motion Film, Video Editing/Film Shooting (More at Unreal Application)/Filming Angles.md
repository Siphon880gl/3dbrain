In photography and filmmaking, various types of camera shots and angles are used to create different effects and convey different emotions or information. Some of the common types include:

1. **Three-Quarter View**: This is when the subject is turned slightly away from the camera, showing three-quarters of their face. It's often used in portraiture to add depth and interest to the subject's features.
	Related: 3/4, 1/2, 1/4 and Full-Body Headshot or Full Body Portraits
	https://thelightcommittee.com/blog/what-is-a-3-4-1-2-1-4-and-full-body-headshot/
    
2. **Close-Up**: A close-up shot tightly frames a person or an object. Close-ups are used to capture detail and are often used to convey emotion in a subject's face.
    
3. **Extreme Close-Up**: This takes the close-up a step further, focusing on a very small area of the subject, like the eyes or mouth.
    
4. **Medium Shot**: This shot captures the subject from the waist up. It is often used for interviews as it allows viewers to see the subject's expressions while still maintaining a sense of their environment.
    
5. **Long Shot (or Wide Shot)**: This captures the subject fully, from head to toe, and often includes their surroundings. It helps to establish the setting and context.
    
6. **Establishing Shot**: Typically a wide shot that shows the overall setting of the scene. It's used at the beginning of a sequence to establish the location and environment.
    
7. **Over-the-Shoulder Shot**: This shot shows the subject from behind the shoulder of another person. It's commonly used in conversations to show interactions between characters.
    
8. **High Angle**: The camera looks down on the subject, often to make them appear smaller, weaker, or more vulnerable.
    
9. **Low Angle**: The camera looks up at the subject, which can make them seem more powerful or imposing.
    
10. **Bird's Eye View**: The camera is placed directly above the subject, giving an overall view of the scene and can convey a sense of detachment or God-like perspective.
    

Each of these types of shots serves a specific purpose in storytelling and can be used in various combinations to create a visually interesting and engaging narrative or presentation.