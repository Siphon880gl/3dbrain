
B roll

Pauline: Movies / tv shows usually have wide angle (shows everybody that are relevant and the setting, and usually shown first), and medium shot, and an over the shoulder shot (behind a character and you can see their shoulder), and a portrait shot (side of a character). And they show the wide shot first, then switch between different shots.

Having two footages your screen recording and your webcam may be what’s needed.

You want more than one angle (webcam, and an off angle) so when you have to edit audio and the video no longer syncs with your lips, you can cut to another shot. (your off angle shot - maybe use a small short tripod with your iphone attached for recording, simultaneously with your mac mounted webcam) (OR, cut to your full screen to make it not noticeable your audio cut off)
The cutting to another shot takes care of abrupt sound changes too.