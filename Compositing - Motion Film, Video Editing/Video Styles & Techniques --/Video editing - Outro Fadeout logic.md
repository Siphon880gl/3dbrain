All slides were

5 seconds 100 to 120% zoom.

  

Now we want:

Final slide to extend 2 seconds and fade out during those 2 seconds. Zoom can keep 120%

  

Previously the audios were 5 seconds x number of slides duration. No fade out previously. Now have to consider the fade out for audio too (you don’t fade out both narration and background music, btw)

  

---

  

  

Last slide will extend by 2 seconds (Industry standard: min 2-4 secs to extend for a proper fade). Fade out through those extended 2 seconds

  

Dont freeze the last frame. Zoom level at the new final frame can be kept the same because people wont notice. 

  

Your narration will end before the 2 seconds, but your music will continue. The continued background music fades out along with the frames. Narration does not cut off

  

So the end result:
![](bOfbdpk.png)
