
Rotoscoping allows you to either:
- Have a stroke outline effect on a moving character
- Animate cartoon or 3d character from a source material (such as a person dancing becomes a cartoon dancing)

In Māori/Polynesian/NZ, "roto" means "the inside, in, within, interior"

What is:
https://www.nfi.edu/rotoscoping/#:~:text=Animator%20Max%20Fleischer%20invented%20rotoscoping,sped%20up%20the%20animation%20process.

How to do:
https://www.youtube.com/watch?v=eE-0ssLqGbE