A script supervisor reveals the video clips, then marks the relevant timemarks with comments and stars (like three out of four stars). A comment could be "preferred first clip", and could be emphasized with font formatting, etc

By doing this, the script writer passes it to the video editor who will have some directions on how to compiliate the video. In addition, the script writer would have continuity considered. Continuity: https://youtu.be/NAvn7CNpdB8

Often done in Excel or a software, like:
![](NfVaAZr.png)
https://peterskarratt.com/

![](lZFzE7a.png)



---

## Filename of the video clip

In this example, "SW 2nd Floor __MED 3:35_" is the filename. 


----

## Rating System

- **How It Works**: After each take, the script supervisor, director, or another designated crew member will select the number of stars that best represents the quality of that take. This is usually done using a dropdown menu where they can select from 1 star (poor) to 5 stars (excellent), or a similar scale.
    
- **Use in Post-Production**: These ratings are then used in post-production to help the editing team quickly identify the best takes for each scene.

---
## Common Abbreviations

Notice MED stands for medium shot, CU for close up, NG for no good

1. **WS** - Wide Shot: Captures the subject within a large frame, showing much of the surrounding environment.
    
2. **MED/MS** - Medium Shot: Shows the subject from a medium distance, often framing them from the waist up.
    
3. **CU** - Close-Up: Focuses closely on a subject or object, highlighting details.
    
4. **ECU** - Extreme Close-Up: Very tightly frames a subject or part of a subject, like just the eyes or mouth.
    
5. **LS** - Long Shot: Similar to a wide shot, it shows the subject fully, from head to toe, within their surrounding environment.
    
6. **OTS** or **OSS** - Over The Shoulder Shot: Shows the subject from over the shoulder of another person, often used in dialogue scenes.
    
7. **POV** - Point of View: Shows the perspective of a character, as if the camera is their eyes.
    
8. **HA** - High Angle: The camera looks down at the subject, often conveying a sense of vulnerability or smallness.
    
9. **LA** - Low Angle: The camera looks up at the subject, often giving them a sense of power or dominance.
    
10. **NG** - No Good: Indicates a take that is unsatisfactory or unusable.
    
11. **VFX** - Visual Effects: Indicates elements in a scene that will require visual effects.
    
12. **SFX** - Special Effects: Refers to practical, in-camera effects.
    
13. **MOS** - Mit Out Sound (without sound): Indicates a scene shot without recording sound.
    
14. **INT** - Interior: Indicates that the scene is inside.
    
15. **EXT** - Exterior: Indicates that the scene is outside.
    
16. **FX** - Effects (can refer to either SFX or VFX, depending on context).
    
17. **EST** - Establishing Shot: A shot, usually a wide shot, that sets up the context for a scene.
    
18. **2S** or **TS** - Two Shot: A shot featuring two characters.
    
19. **3S** - Three Shot: A shot featuring three characters.
    
20. **XLS** - Extreme Long Shot: Similar to a wide shot but the subject is even smaller and more distant.

---

![](TeRWUeg.png)

![](uhxBVAy.png)

![](MbPVASN.png)



