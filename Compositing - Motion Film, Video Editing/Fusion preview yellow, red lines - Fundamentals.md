Τhe yellow bars are the in and out frames of the footage you are working with and the red is the playhead
![](mHtSAgb.png)

Clip in / out (yellow lines) can be set in two ways:
- CMD shortcut: CMD left click hold. Where you start holding will become left start. Where you release will become right end
- Slide the numeric input fields:
  You can adjust more small unit precisely by. dragging left and right on either of the two numbers for left and right end.
  ![](hlGrrZc.png)
