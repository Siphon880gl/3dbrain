
Weng’s personal:
https://chatgpt.com/share/66ea8ac7-158c-800f-9460-743cf7fcd693


To get an idea on what LUT you can apply to your motion film or video based on genre, dramatic effect, or to give off a specific movie's vibes, refer to this list.

Please note that specific usage of LUTs in movies is often not publicly disclosed due to the proprietary nature of post-production workflows. However, these are close approximations.

## LUT List

---

### **1. HDR ST 2084 LUTs**

- **Usage**: These LUTs are used for mastering High Dynamic Range (HDR) content following the SMPTE ST 2084 (PQ) standard.
- **Genres**: Action, Sci-Fi, Fantasy, and any genre that benefits from enhanced dynamic range and vivid visuals.
- **Notable Movies**: While specific LUT usage isn't disclosed, films like *"Blade Runner 2049"* and *"Mad Max: Fury Road"* have HDR releases that would utilize similar LUTs during post-production.

---

### **2. ACES (Academy Color Encoding System)**

- **Usage**: ACES is a color management framework that standardizes color workflows from production to distribution.
- **Genres**: Widely used across all genres for its flexibility and consistency.
- **Notable Movies**: *"Spider-Man: Homecoming"*, *"The Lego Batman Movie"*, and *"The Martian"* employed ACES in their workflows.

---

### **3. Film Looks (e.g., Kodak 2383, Fujifilm 3513DI)**

- **Usage**: Emulates the look of traditional film stocks to give digital footage a cinematic feel.
- **Genres**: Drama, Indie, Period Pieces, and any film aiming for a classic or nostalgic aesthetic.
- **Notable Movies**:
  - *"Joker"* used film emulation to achieve its gritty, vintage look.
  - *"La La Land"* employed similar techniques for a nostalgic feel.

---

### **4. ARRI LUTs (e.g., Arri Alexa LogC to Rec709)**

- **Usage**: Designed for ARRI cameras, converting LogC footage to Rec.709 for standard viewing.
- **Genres**: Common in high-budget films across all genres.
- **Notable Movies**:
  - *"Skyfall"*
  - *"The Revenant"*
  - *"Gravity"*
- **ARRI cameras are industry favorites for their dynamic range and image quality.**

---

### **5. Sony LUTs (e.g., SLog2/SLog3 to Rec709)**

- **Usage**: For Sony cameras utilizing S-Log profiles to maximize dynamic range.
- **Genres**: Documentaries, Commercials, Indie Films.
- **Notable Movies**:
  - *"Concussion"* used Sony F55 cameras.
  - *"Oblivion"* incorporated Sony's F65 cameras.

---

### **6. Blackmagic Design LUTs**

- **Usage**: Specific to Blackmagic cameras, converting their Film Log gamma to standard color spaces.
- **Genres**: Indie Films, Music Videos, Low-Budget Productions.
- **Notable Projects**:
  - *"The Peanut Butter Falcon"* utilized Blackmagic cameras.
  - Various episodes of TV shows and indie projects.

---

### **7. RED LUTs**

- **Usage**: Tailored for RED Digital Cinema cameras, known for high-resolution imaging.
- **Genres**: Action, Sci-Fi, Adventure.
- **Notable Movies**:
  - *"The Hobbit" Trilogy
  - *"Guardians of the Galaxy Vol. 2"*
  - *"The Girl with the Dragon Tattoo"*

---

### **8. DJI LUTs**

- **Usage**: For aerial footage captured with DJI drones, converting D-Log profiles to standard color spaces.
- **Genres**: Action, Documentary, Nature Films.
- **Notable Uses**:
  - Aerial shots in movies like *"Captain America: Civil War"*.
  - Nature documentaries produced by BBC and National Geographic.

---

### **9. Panasonic LUTs (e.g., V-Log to V-709)**

- **Usage**: For Panasonic cameras using V-Log to capture a wider dynamic range.
- **Genres**: Documentaries, Indie Films, Corporate Videos.
- **Notable Projects**:
  - Used in various independent films and documentaries.
  - Popular among videographers for commercials and short films.

---

### **10. VFX IO LUTs**

- **Usage**: Facilitate the conversion between different color spaces for visual effects integration.
- **Genres**: Sci-Fi, Fantasy, Action—any genre requiring heavy VFX work.
- **Notable Movies**:
  - *"Avengers: Endgame"*
  - *"Star Wars: The Rise of Skywalker"*
- **These LUTs help ensure that CGI elements match the color space of live-action footage.**

---

### **11. Invert Color LUTs**

- **Usage**: Creates a negative of the image for stylistic effects.
- **Genres**: Experimental, Horror, Music Videos.
- **Notable Uses**:
  - Certain scenes in *"Under the Skin"* and *"Mandy"* use color inversion for surreal effects.
  - Music videos and art films frequently employ this LUT for creative expression.

---

### **12. Canon LUTs (e.g., Canon Log to Rec709)**

- **Usage**: For Canon cameras using C-Log profiles.
- **Genres**: Documentaries, Short Films, Web Series.
- **Notable Projects**:
  - Widely used in documentary filmmaking and independent projects.
  - Web series and online content creators favor Canon for its color science.

---

### **13. DJI LUTs**

- **Usage**: Specifically for footage shot on DJI drones or cameras.
- **Genres**: Aerial Cinematography in various genres.
- **Notable Uses**:
  - Establishing shots in films like *"The Wolf of Wall Street"*.
  - Travel documentaries and adventure films.

---

### **14. Astrodesign & Olympus LUTs**

- **Usage**: Less common, used for specific camera models and workflows.
- **Genres**: Specialized applications, possibly in tech-centric films or projects.
- **Notable Uses**:
  - Niche usage in projects requiring specific equipment.

---

### **15. LMT (Look Modification Transform) LUTs**

- **Usage**: Part of the ACES workflow for applying creative looks while preserving colorimetric integrity.
- **Genres**: Any genre where a specific creative look is desired.
- **Notable Movies**:
  - Films using ACES may employ LMTs for look development.

---

### **16. Cintel LUTs**

- **Usage**: Used in film scanning and restoration processes.
- **Genres**: Restoration of classic films, Archival Projects.
- **Notable Projects**:
  - Restoration of older films for re-release in higher resolutions.

---

### **17. DCI LUTs (e.g., Rec709 to DCI-XYZ)**

- **Usage**: For converting content to Digital Cinema Initiatives (DCI) standards for theatrical distribution.
- **Genres**: All genres for theatrical releases.
- **Notable Uses**:
  - Ensuring films meet the color space requirements for cinema projectors.

---

### **18. VFX IO LUTs**

- **Usage**: Managing color space conversions in VFX pipelines.
- **Genres**: Heavy VFX films—Sci-Fi, Fantasy, Superhero movies.
- **Notable Movies**:
  - *"Avatar"*
  - *"The Lord of the Rings"* series.

---

### **19. Panasonic V-Log to V-709**

- **Usage**: Converts Panasonic V-Log footage to Rec.709 for standard viewing.
- **Genres**: Versatile across genres where Panasonic cameras are used.
- **Notable Projects**:
  - Used in indie filmmaking and television production.

---

### **20. Sony SLog3SGamut3.Cine LUTs**

- **Usage**: Designed for Sony's S-Log3 and S-Gamut3.Cine color spaces.
- **Genres**: Feature Films, Commercials, Music Videos.
- **Notable Uses**:
  - Popular among filmmakers for its cinematic look.

---

**Note**: While we've associated these LUTs with general practices and genres, the specific LUTs used in individual movies are typically part of the colorist's proprietary workflow and are not publicly disclosed. The above associations are based on standard industry practices and the typical applications of these LUTs in film production.

---

If you have specific movies or genres you'd like more detailed information on, or if you're interested in how to apply these LUTs for certain looks in your projects, feel free to ask!

----

Thank you for bringing that to my attention. I realize that I missed covering some of the LUTs you listed in my previous response. Let me address those now to ensure we have a comprehensive overview.

---

### **1. **HDR Hybrid Log-Gamma LUTs**

- **Usage**: These LUTs are used for mastering HDR content following the Hybrid Log-Gamma (HLG) standard, developed by the BBC and NHK. HLG is designed for live broadcast and is backward compatible with SDR displays.
- **Genres**: Live Events, Concerts, Sports Broadcasts, and any content intended for HDR broadcast.
- **Notable Uses**:
  - **Live Concerts**: Broadcasts like *"Glastonbury Festival"* use HLG for live HDR streaming.
  - **Sports Events**: Major events like the *Olympics* and *FIFA World Cup* have experimented with HLG HDR broadcasting.

---

### **2. **LMT Day for Night.xml**

- **Usage**: This Look Modification Transform is used to simulate nighttime scenes from footage shot during the day. It adjusts the luminance and color balance to create a convincing night effect.
- **Genres**: Horror, Thriller, Adventure—any genre requiring nighttime scenes without the logistical challenges of night shooting.
- **Notable Uses**:
  - **Day-for-Night Techniques**: Films like *"Mad Max: Fury Road"* and *"Jaws"* have employed day-for-night shots to great effect.
  - **Television Series**: Shows with tight schedules may use this LUT to save time and resources.

---

### **3. **LMT Neon Suppression.xml**

- **Usage**: This LUT is designed to manage and suppress overly saturated neon colors that can cause clipping or artifacts in digital footage.
- **Genres**: Sci-Fi, Cyberpunk, Urban Dramas—any genre featuring prominent neon lighting.
- **Notable Movies**:
  - *"Blade Runner 2049"*: Heavy use of neon lighting where colorists manage neon saturation carefully.
  - *"Tron: Legacy"*: Features intense neon visuals requiring precise color control.

---

### **4. **Canon Log to Cineon.ilut**

- **Usage**: Converts Canon Log footage to the Cineon Log color space, which is useful when integrating Canon footage into film-based workflows or VFX pipelines that utilize Cineon.
- **Genres**: Films combining digital and scanned film footage, or those with heavy VFX.
- **Notable Uses**:
  - **VFX Integration**: Ensures consistency when Canon footage is combined with CGI or film scans.
  - **Archival Projects**: Useful in restoring or combining old film footage with new digital content.

---

### **5. **Data to Video with Clip.olut**

- **Usage**: This LUT maps full-range data levels to legal video levels with clipping, ensuring that the footage conforms to broadcast standards.
- **Genres**: Broadcast Television, Commercials, Any content destined for platforms with strict level requirements.
- **Notable Uses**:
  - **Broadcast Compliance**: Ensures that commercials and TV shows meet the necessary technical specifications.
  - **Platform Delivery**: Used when delivering content to streaming services that require video level compliance.

---

### **6. **Astrodesign LUTs (ALog to ARRI Log C.cube)**

- **Usage**: Converts Astrodesign ALog footage to ARRI Log C, facilitating a unified color workflow when combining footage from different camera systems.
- **Genres**: Specialized productions using Astrodesign cameras alongside ARRI systems.
- **Notable Uses**:
  - **Technical Productions**: Possibly used in high-resolution video capture for scientific or special effects purposes.

---

### **7. **Cintel Negative to Linear.ilut** and **Cintel Print to Linear.ilut**

- **Usage**: These LUTs are used when scanning film negatives or prints using Cintel scanners, converting the footage to a linear color space for digital post-production.
- **Genres**: Film Restoration, Archival Projects, Classic Film Remastering.
- **Notable Projects**:
  - **Film Restoration**: Remastering classic films like *"Lawrence of Arabia"* or *"The Godfather"* for modern formats.
  - **Archival Footage Integration**: Incorporating historical footage into new documentaries.

---

### **8. **Invert Color.ilut** and **Invert Color.olut**

- **Usage**: Creates a negative image effect by inverting the colors. Useful for creative effects or inverting scanned negatives to positives.
- **Genres**: Experimental Films, Music Videos, Artistic Projects.
- **Notable Uses**:
  - **Artistic Expression**: Used in avant-garde films and music videos to create striking visuals.
  - **Film Scanning**: Inverting color negatives to positive images during digitization.

---

### **9. **LMT Kodak 2383 Print Emulation.xml**

- **Usage**: Emulates the look of the Kodak 2383 print film stock, giving digital footage a classic cinematic appearance.
- **Genres**: Drama, Period Pieces, Films aiming for a traditional filmic look.
- **Notable Movies**:
  - **Digital Films Seeking Film Look**: Movies like *"Black Panther"* and *"John Wick"* have utilized film print emulation for aesthetic purposes.

---

### **10. **VFX IO LUTs (Additional Detail)**

- **Usage**: Includes LUTs like **Cineon Log to Linear.cube** and **Linear to Cineon Log.cube**, essential for VFX workflows.
- **Genres**: High VFX content films where linear color space is crucial for compositing and CGI.
- **Notable Movies**:
  - **Visual Effects Heavy Films**: *"Transformers"*, *"Pacific Rim"* where accurate color representation is vital for VFX integration.

---

### **11. **DCI LUTs (Additional Detail)**

- **Usage**: Specific LUTs like **P3 to DCI-XYZ.dat**, **Rec709 to DCI-XYZ.cube**, and **sRGB to DCI-XYZ.cube** are used to convert various color spaces to DCI-XYZ for digital cinema projection.
- **Genres**: All films prepared for theatrical release.
- **Notable Uses**:
  - **Theatrical Distribution**: Ensuring that films are correctly projected in cinemas according to DCI standards.
  - **Film Festivals**: Submissions often require DCI-XYZ compliant files.

---

### **12. **Data to Video with Clip.olut** (Further Explanation)

- **Usage**: Clipping data levels above legal video range to prevent issues in broadcast and distribution.
- **Genres**: Television, Advertising, Any content requiring strict adherence to broadcast standards.
- **Notable Uses**:
  - **Compliance Checks**: Used during final QC processes before distribution.

---

### **13. **Canon Log to Video.ilut**

- **Usage**: Converts Canon Log footage directly to standard video gamma, suitable for quick turnarounds.
- **Genres**: News Gathering, Run-and-Gun Documentaries, Event Videography.
- **Notable Uses**:
  - **Time-Sensitive Productions**: Where speed is prioritized over extensive color grading.

---

### **14. **Sony SLog2 to Rec709.ilut**

- **Usage**: Converts Sony S-Log2 footage to Rec.709, useful for cameras like the Sony A7S series.
- **Genres**: Indie Films, Online Content, Corporate Videos.
- **Notable Projects**:
  - **Independent Productions**: Favored by indie filmmakers for its balance of quality and affordability.

---

### **15. **Canon Log to Rec709.ilut**

- **Usage**: Similar to the previous Canon LUT, but specifically targets Rec.709 color space.
- **Genres**: Broadcast Television, Corporate Videos, Educational Content.
- **Notable Uses**:
  - **Standard Delivery Formats**: Ensures footage is compatible with Rec.709 displays.

---

### **16. **Blackmagic Design (Additional LUTs)**

- **Usage**: Includes LUTs like **Blackmagic Gen 5 Film to Video.cube** and various camera-specific LUTs for Pocket Cinema Camera 4K/6K.
- **Genres**: Indie Films, Student Films, Web Series.
- **Notable Projects**:
  - **Emerging Filmmakers**: Provides cinematic quality on a budget.

---

### **17. **Arri Alexa LogC to Rec709.dat**

- **Usage**: Converts ARRI LogC footage to Rec.709, facilitating viewing and editing in standard color spaces.
- **Genres**: High-End Cinema, Commercials, Television Series.
- **Notable Series**:
  - *"Game of Thrones"*: Used ARRI cameras extensively.
  - *"Westworld"*: Leveraged ARRI's dynamic range.

---

### **18. **Panasonic V-Log to V-709.cube** (Additional Detail)

- **Usage**: Essential for converting Panasonic's V-Log footage to a standard video gamma.
- **Genres**: Documentary Filmmaking, Live Events.
- **Notable Uses**:
  - **Broadcast Media**: News outlets using Panasonic cameras for field reporting.

---

### **19. **Invert Color.ilut** and **Invert Color.olut** (Clarification)

- **Usage**: Both LUTs serve the same purpose but may be formatted differently (.ilut vs. .olut).
- **Genres**: Experimental Filmmaking, Visual Art Projects.
- **Notable Uses**:
  - **Creative Effects**: Used in title sequences or dream sequences to disorient the viewer.

---

### **20. **Data to Video with Clip.olut** (Emphasized)

- **Usage**: Important for ensuring that video levels conform to broadcast standards by clipping out-of-range values.
- **Genres**: All genres requiring broadcast delivery compliance.
- **Notable Uses**:
  - **Final Mastering**: A last step in the post-production pipeline before content delivery.

---

**Summary**:

- **LUTs Not Previously Covered**: I have now addressed the **HDR Hybrid Log-Gamma LUTs**, **LMT Day for Night.xml**, **LMT Neon Suppression.xml**, **Canon Log to Cineon.ilut**, **Data to Video with Clip.olut**, **Astrodesign LUTs**, and additional details on other LUTs.
- **Coverage Confirmation**: With this additional information, we have now covered all the LUTs you listed.
- **Note on Specificity**: While I have provided general associations with genres and potential uses, the exact usage of these LUTs in specific movies is often confidential and varies based on the colorist's creative decisions.

---

If there's any other LUT you'd like more details on, or if you have questions about how to apply these LUTs in your projects, please let me know!


-----

Thank you for your patience. Upon reviewing the list once more, I noticed that I missed covering the **Olympus LUT**. Let me provide details on that to ensure we've comprehensively addressed all the LUTs you listed.

---

### **Olympus LUT**

- **LUT File**: **Olympus OM-Log400_to_BT.709_v1.0.cube**

- **Usage**: This LUT is designed to convert footage shot in Olympus's OM-Log400 logarithmic gamma profile to the standard BT.709 (Rec.709) color space. OM-Log400 allows Olympus cameras to capture a wider dynamic range, preserving more detail in shadows and highlights.

- **Genres**: Travel Videos, Documentaries, Indie Films, Wildlife Filmmaking—any projects utilizing Olympus cameras for their unique features.

- **Notable Uses**:

  - **Travel and Adventure Filmmaking**: Olympus cameras are renowned for their portability and ruggedness, making them a favorite among travel filmmakers who need durable equipment in various environments.

  - **Documentaries and Nature Films**: The extended dynamic range provided by OM-Log400 is beneficial for capturing natural scenes with varying lighting conditions.

- **Additional Notes**:

  - **Color Science**: Olympus has a distinct color science that this LUT helps to translate accurately into the BT.709 color space, ensuring that the unique characteristics of Olympus footage are maintained.

  - **Post-Production Workflow**: Applying this LUT provides a good starting point for further color grading, allowing colorists to build upon a balanced image.

---

### **Final Confirmation**

With the inclusion of the **Olympus LUT**, we have now covered all the LUTs you provided in your list. Here's a quick summary of the LUTs and their coverage:

1. **HDR ST 2084 LUTs** - Covered
2. **ACES LUTs** - Covered
3. **Film Looks** - Covered
4. **ARRI LUTs** - Covered
5. **Sony LUTs** - Covered
6. **Blackmagic Design LUTs** - Covered
7. **RED LUTs** - Covered
8. **DJI LUTs** - Covered
9. **Panasonic LUTs** - Covered
10. **VFX IO LUTs** - Covered
11. **Invert Color LUTs** - Covered
12. **Canon LUTs** - Covered
13. **Astrodesign LUTs** - Covered
14. **LMT LUTs (Day for Night, Neon Suppression, Kodak 2383 Print Emulation)** - Covered
15. **Cintel LUTs** - Covered
16. **DCI LUTs** - Covered
17. **Data to Video with Clip.olut** - Covered
18. **HDR Hybrid Log-Gamma LUTs** - Covered
19. **Olympus LUT** - **Now Covered**

By checking these sites and communities, you’ll be able to find a wide range of cinematic looks, many of which may emulate the tone, color grade, or mood of specific films or genres.

---

**We're now done** with covering all the LUTs you listed. If you have any more questions or need further details on any of these LUTs, feel free to ask!

---
---

## Movie not on list?

If you want to give off the vibes of a movie that's not on this list, you can google:
- Google: identify LUT of movie \_\_\_
- Google: Recognize LUT of movie ___


---
---

## Apply LUTs

For DaVinci: [[DaVinci Luts - Apply Luts to Multiple Clips]]

