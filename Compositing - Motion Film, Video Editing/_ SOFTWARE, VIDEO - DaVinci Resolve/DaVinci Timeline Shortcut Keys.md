
Edit Screen's timeline shortcuts:
- a - selector
- b - blade
- m - markers
- CMD+Right to go to next clip in timeline
- Shift+Backspace for ripple delete (slide the subsequent clips backwards to fill in the newly created gap)
- Backspace for lifting aka deleting (DO NOT slide the subsequent clips backwards to fill in the newly created gap)

Edit Screen's timeline clip selection shortcuts:
- CMD+Up/right/down/left will select the adjacent clip from the currently selected clip.
	- Eg. CMD+Right:
	- Before:
	  ![](fxzdY68.png)

	- After:
	  ![](vQ1D5PE.png)

- Opt+Up/down will take the current clip and move it into the another track, overriding any existing clip in the destination track (otherwise, insert into empty area of the destination track). Recommend you DO NOT use this shortcut.
- SHIFT+V selects the clip on the **current** video track from where the playhead is at
  ![](TRdaleI.png)

- Y selects all clips forward on the **current** video track from where the playhead is at
  ![](5ohlIUO.png)

- OPT+Y select all clips forward across **all** video tracks from where the playhead is at
	  ![](TEkfkZy.png)
- CMD+Y selects all clips backwards on the **current** video track from where the playhead is at
  ![](BV8umMy.png)

- OPT+CMD+Y select all clips backwards across **all** video tracks from where the playhead is at
  ![](uzEIKlD.png)


Recommended you assign these shortcuts:
-  Set "CMD+SHIFT+N" to create new fusion clip