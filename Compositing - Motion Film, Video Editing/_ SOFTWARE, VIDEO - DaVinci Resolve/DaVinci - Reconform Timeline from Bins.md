
## Why

Conforming to bins in DaVinci Resolve is an important process for several reasons:

1. Updating references: If your media files have moved or been renamed, conforming to bins can help Resolve locate and relink to the correct files.
2. Leveraging the project as a template: If you want to use the same project as a basis for your future videos because your videos are all very formulaic needing the same number of clips, same duration for each clip, same motion effects (fusion), and same transitions, reconforming bins can be helpful. You can create separate bins (folders in Media Folder) named after your different video projects. When starting a new video project, name the bin after your project, and upload the media but rename them to the same filenames as the other bin(s). Then you can duplicate the old timeline, and then you can reconform this new timeline to this bin, swapping out old clips with the new clips.
3. Preparing for color grading or rendering: Proper conforming ensures that you're working with the highest quality source material for color grading, rather than proxy files or lower-quality versions.
4. Resolving timecode issues: Conforming can help fix timecode discrepancies between your timeline and source media, which is crucial for maintaining edit accuracy.
5. Organizing projects: It helps maintain a well-organized project structure, especially in collaborative environments where multiple editors or colorists might be working on the same project.
6. Handling mixed frame rates or resolutions: When working with footage of different frame rates or resolutions, conforming to bins can help Resolve correctly interpret and process the media.
7. Troubleshooting: Conforming can often resolve issues with mismatched timecodes, offline media, or other timeline irregularities.

---

## Instructions

Switch out por re-link clips in a timeline based on their clips' filenames matching to the files in the Media Pool

Remember the Media Pool can be divided into folders called bins. You'll select the media pool's folder you want to sync

First, unlock all clips in the timeline for reconforming. Select all the clips in the timeline, right-click one of them, and make sure is unticked: Conform Lock Enabled

![](KLV2p25.png)


![](OQ7hnBA.png)




Select timeline asset in media pool → Reconform From Bins... → Make sure Filename “Tight filename match” is ticked

![](qOiIaSc.png)


![](GEfO36n.png)
