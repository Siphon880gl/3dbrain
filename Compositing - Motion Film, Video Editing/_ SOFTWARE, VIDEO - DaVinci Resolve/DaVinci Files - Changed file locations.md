Changing folder locations but keeping filenames the same? Usual scenarios are changing folder location to keep more organized, migrating from a different computer, or you recently migrated your assets from your computer to an external harddrive. 

You have to relink selected clips by right clicking the clips in the media pool → Relink Selected Clips. 

A quicker way than manually linking each asset file is to Relink to Select Clip. See 1:03 
https://youtu.be/UOzZkYCOka4?t=60

Quick: If changes are NOT showing in timeline, close and reopen DaVinci.