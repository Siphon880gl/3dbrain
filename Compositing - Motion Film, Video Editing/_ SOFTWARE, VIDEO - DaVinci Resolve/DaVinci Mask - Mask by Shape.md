In Fusion, you add Rectangle or Polygon (SHIFT+Space)

![](aCuoDIz.png)

So this is the result:
![](fTAwtxE.png)

The mask being see through is adjusted by Level:
![](TdaEixd.png)

Which results in:
![](Uvwcz26.png)
