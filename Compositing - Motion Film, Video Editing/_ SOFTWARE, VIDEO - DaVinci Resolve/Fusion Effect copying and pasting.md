
You can CMD+C or CTRL+C on selected node(s) at Fusion then paste to another clip's Fusion. If you pasted to a text file, you'll see it's just code. So Fusion sends code to your clipboard and it receives code from the clipboard; and when it receives the code, it renders those nodes on the Fusion screen. Btw that code is called Fusion Script, which is a markup language.

You can save that Fusion Script in a .setting file extension. This allows you to drag and drop into the Fusion screen!

If you're using DaVinci Resolve API to load Fusion scripts, those scripts have to be file extension .comp, although it's the same code as the above .setting file.