You can use your own LUTs in DaVinci Resolve! To make your custom LUTs available in the media pool when you right-click on clips, follow these steps:

### Steps to Add Custom LUTs:
1. **Locate Your LUTs:**
   Make sure your LUTs are in a `.cube` format, which is the standard for DaVinci Resolve.

2. **Move the LUTs to the Right Folder:**
   Copy your custom LUT files into the following directory:
   - On **macOS**: `/Library/Application Support/Blackmagic Design/DaVinci Resolve/LUT`
   - On **Windows**: `C:\ProgramData\Blackmagic Design\DaVinci Resolve\Support\LUT`
   - On **Linux**: `/opt/resolve/LUT`

3. **Refresh LUTs in DaVinci Resolve:**
   - Open **DaVinci Resolve** and go to the **Project Settings** by clicking the gear icon in the bottom right corner.
   - In the **Color Management** section, find the **Lookup Tables** tab.
   - Click the **Update Lists** button to refresh the LUT library.

4. **Apply the Custom LUT:**
   Now, when you right-click on a clip in the media pool and go to the **LUT** submenu, your custom LUT should appear in the list, ready for you to apply it to your clips.

This method integrates your custom LUTs directly into DaVinci Resolve’s LUT selection menus, making them easily accessible during your editing process.