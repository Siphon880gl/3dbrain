
What it looks like:
The frame loss feels similar to when a Nintendo Switch game struggles under heavy visual effects, causing brief, jittery pauses as everything slows down. But with your rendered video, it slows down throughout it.

How to fix:
If your video clips are higher FPS than your timeline’s FPS, then you will have jittery or loss of frames

FPS for your clips (Right click a clip in the media pool → Clip attributes)... should match:
 - FPS in your timeline (settings gear icon at bottom right of timeline)

Caveat: You can only change project frame rate (and by extension timeline fps if you have default settings - explained soon) while there is nothing on the timeline, moment you put something even if you remove later there is no changing of framerate. to get around this you could create new timeline, uncheck use project settings, set your framerate and copy entire contents of first timeline there.
- You can have a timeline NOT use the project settings when creating a new timeline at File → New Timeline.
- Untick “Use Project Settings” at bottom left:
	![](3pyX868.png)


- Then on the same New timeline dialog you can change the FPS:
  ![](dUZlOUn.png)

- After creating the timeline, before you insert clips, make sure to click the timeline settings gear icon at the bottom right, and under Master Settings check if it’s the fps you want (it reverts back on the FPS settings sometimes - a DaVinci quirk!):
  ![](gjBjUFK.png)
