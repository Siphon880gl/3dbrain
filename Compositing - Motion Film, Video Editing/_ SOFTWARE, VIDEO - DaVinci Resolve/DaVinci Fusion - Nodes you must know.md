https://youtu.be/RiRXYuj1GIE

0:44 - Background
1:40 - Text+
3:10 - Merge
4:20 - Polygon Mask
6:35 - Transform
7:32 - Color corrector
8:00 - Tracker
9:45 - Displace
11:20 - Planar Tracker
12:45 - Gaussian Blur
13:55 - DVE