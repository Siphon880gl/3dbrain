At the top left tabs when you're editing, you click Effects, then on the left you open Titles:
![[Pasted image 20250302030607.png]]

To preview a title asset, drag your mouse left and right on a selection tile - it'll override the preview panel while your mouse is over the selection tile:

1. Mouse over:
   ![[Pasted image 20250302030646.png]]
2. Moving mouse right-ward
   
   ![[Pasted image 20250302030701.png]]
   
   ![[Pasted image 20250302030708.png]]