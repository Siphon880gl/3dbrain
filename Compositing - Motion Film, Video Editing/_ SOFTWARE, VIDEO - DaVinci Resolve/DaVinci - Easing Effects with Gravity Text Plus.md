Let's follow the example of text that appears on the screen and then falls down. Gravity Text+

How Gravity Text+ could look
[https://m.youtube.com/watch?si=xF9uQivJMlSDZByz&v=g2ctOFxDBk0&feature=youtu.be](https://m.youtube.com/watch?si=xF9uQivJMlSDZByz&v=g2ctOFxDBk0&feature=youtu.be)

Setup:
In Fusion nodes, have a Transform node. Then set the center position at first frame and middle frame of the duration of your clip. And at the 75% of the duration's frame, have the text moved down pass 25% of the canvas (So Y at -0.25)

So it’s 0.5/0.5, 0.5/0.5, 0.5/-0.25

Now when you preview the text animation, the text falls down in a linear manner which doesn’t imitate the real life motion of an object falling and accelerating to the ground due to gravity

We can create an acceleration affect by modifying in the Splines panel so that the speed of the transformation changes at different points of the clip. Click Spline at the top right

Recommend lining up the Spline panel’s frames with the preview panels’ frame numbers.

![](yJeBvzy.png)


So you can easily see the keyframes we lined up with the Spline's frames:
First frame
![](9P1D4Q9.png)

Middle frame
![](PFaJWXY.png)

Then there's a 75% of the way into the clip's frame. And when it's all adjusted for an accelerated movement down:
![](9LwWigp.png)

Notice this is an exponential curve where after the middle point of the clip, the text starts moving slowly at first before speeding up

---

The below are some tricks of modifying Spline
You have photoshop pen tools to create points, delete points, manipulate points, which manipulates the path.
![](HSQ3gD9.png)


You can select a point and switch to “Ease In / Out” mode to make that dot’s angle and the adjacent pathing closer to an easing curve for acceleration or deceleration motions

Note you may have to move/rotate the handle a bit to see the changes.
![](03Fw5oL.png)

You can drag the handle to morph the curves
![](9iADHOR.png)


You can select two points for it to create a specific pathing (Linear is straight line):
![](tBlxfIJ.png)
