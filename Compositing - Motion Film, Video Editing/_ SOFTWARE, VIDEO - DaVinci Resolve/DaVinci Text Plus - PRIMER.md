
Why Text+? It gives you creative control over the text or title overlay on the video.

In Fusion:
Shift+Space -> Search for Text+

In Edit:
Effects panel -> Search for Text+ (You'll need to make sure Toolbox is selected, and not a particular type of effects selected)
![](t6ovbKL.png)
