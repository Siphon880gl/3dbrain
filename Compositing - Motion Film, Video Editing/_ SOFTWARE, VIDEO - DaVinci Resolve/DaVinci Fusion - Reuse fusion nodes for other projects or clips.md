
Select all nodes. Right click the final node - MediaOut1 → Settings (towards the bottom) → Save As.
It will save as a .setting file. You’ll drag and drop the setting file into your next project’s Fusion page. Then you have to reattach MediaIn and MediaOut. Then make sure to click the left circle on media 1 and right circle on media 2 to resync the preview monitors.
(In other words, 1:22 at https://youtu.be/yH0ZxMTMkZw?t=82)

If you're immediately editing this new project or other clip, you can copy and paste the Fusion nodes instead.

The two ways are equivalent because if you had pasted into a text editor, you'll see it's Fusion scripting language and it'll match the contents of the .setting file.