
Create a Text+ Node (if your clip has the text, you'll cut and paste that text to the Text+ Node)

Under Tools ->Shading, select Element 2 which is the Outline shading type. Tick "Enabled" if no properties appear.

Change the background color to black or your desired color under Properties. So far this will create an outline because the Outline shading type's subtype is Outline by default.

Change the subtype from outline into a rectangular background:
1. Under Properties -> Appearance -> Select the solid icon instead of the outline icon
2. Play around with the "Extend Horizontal" and "Extend Vertical" in the same properties area. Adjust until the gaps between words has a background, if you desire to have a background on each line rather than each word. And adjust until you have your preferred rectangular size.

![](mUZ6OJd.png)
