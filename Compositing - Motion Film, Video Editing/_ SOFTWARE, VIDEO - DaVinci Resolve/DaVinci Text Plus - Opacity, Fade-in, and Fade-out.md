
Select your Text+ node in Fusion. At Inspector, go under Shading Tool.
For Element 1, make sure blend is on Composite.

Here you can adjust the opacity
![](VWMxrkx.png)

You can also keyframe it by hitting the diamond button at the Opacity line. You'll have finer control of at what time is at what opacity level in order to create your fade-in or fade-out animation, if you go into the Keyframes panel:
![](8UbpMGp.png)
