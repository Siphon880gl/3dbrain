
Titled: How to change resolution aka dimensions

Note newbie mistake: Cropping in preview monitor wont affect render resolution

---

You change the resolution on the Cut page’s timeline setting

1. Click setting icon on the bottom right
	![](Ih2rrce.png)

2. Then change resolution here under "Timeline Resolution":
   ![](VfWoPmj.png)

3. When exporting, you can confirm the dimensions did change under "Resolution" (First stat item):
   ![](XkLrbtv.png)

