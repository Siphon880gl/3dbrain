
Drag and drop all the pics to have the same duration? Or have future drops with the same duration?
![](Bqr9R3t.png)

Set the standard still duration before drag and dropping:
Davinci → Preferences ->
![](ZhjRoHh.png)
