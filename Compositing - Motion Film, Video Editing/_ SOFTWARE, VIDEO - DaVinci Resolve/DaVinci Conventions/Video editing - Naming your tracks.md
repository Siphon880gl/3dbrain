
This is Weng's convention

See some schemas below on ways to name your tracks:

![](v58uoUR.png)

![](beowof2.png)
An advantage is it's easier to collaborate with other video editors AND if you come back to the same project 3 months later to re-do the video or use it as a template for another video project, you have the tracks named so you can easily orientate yourself to where the clips are on the timeline.