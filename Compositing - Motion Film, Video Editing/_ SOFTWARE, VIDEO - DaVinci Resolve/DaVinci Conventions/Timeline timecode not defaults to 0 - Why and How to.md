It's a practice used to allow space for extraneous clips, such as the slate closing or other lead-in material, which can easily be trimmed off during editing. This starting point provides a buffer before the actual content begins.

---

Pathways: 
Watch this video are follow below
https://www.youtube.com/watch?v=A7S9DZ7r5Lk

Here's how you start a new timeline at 00:
Right click media pool empty area -> Create New Timeline -> Adjust "Start Timecode"

Once you have a timeline with clips in it, you cannot change the starting timecode of that timeline.

**Global default**: You can set the default to 00 so you don't have to change it from 01 to 00 everytime you are on a new project:
Resolve Properties -> User -> Editing -> Start timecode