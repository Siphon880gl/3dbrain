
The term "lift" in video editing has its origins in analog film editing. In the days of physical film, editors would "lift" a piece of film out of the reel when they wanted to remove it without affecting the surrounding frames.

Here's a bit more context:

1. **Physical Film Editing**: In the early days of film editing, editors worked directly with reels of film. If they wanted to remove a portion of the film, they would literally cut the film at two points and "lift" the section out. This action left a gap in the sequence, which is similar to what happens in modern digital editing when you "lift" or delete a clip—no automatic rearranging happens, and a gap is left behind.

2. **"Lift" in Modern Editing**: In digital editing software, "lift" became the term for removing a clip while leaving a gap, preserving the duration of the timeline. It contrasts with "extract" or "ripple delete," which pulls the surrounding content together to close the gap. The terminology carried over from the physical process, preserving its meaning and function.

So, the term "lift" has historical roots tied to the manual editing process of physically removing sections of film without changing the overall sequence structure.