
Fusion tab lets you layer, mask, and transform for a clip. For a history of Fusion, refer to [[Fusion History]]

Fusion is only one part of DaVinci Resolve and it is an advanced part of DaVinci Resolve. If you actually want the primer for the rest of DaVinci Resolve or your a total beginner, refer to [[_ DaVinci Resolve Beginner PRIMER]]

**How to use**: Recommend you open a screen-persistent Table of Contents so you can navigate this document more easily.

---

## Basics

**Q: Im on the Edit Screen. How to edit a particular clip's Fusion?**
Make sure the clip in the timeline is selected and that the playhead (red line) is on that clip. Then open Fusion tab. Or right-click the timeline clip → Open in Fusion Page


---

## Basics - Fusion Nodes
Optional - Video explanation: https://www.youtube.com/watch?v=IxPaoQsjO50

**Q: What are Fusion Nodes:**
Nodes to combine effects, masking, and transformations, with or without keyframes, and to have layers inside this clip as a Fusion clip

**Q: Im on the Fusion screen but how do I start adding more nodes than the MediaIn1 and MediaOut1 I see?**
Press SHIFT+Space and search for the type of node you want.

**Q: What are the triangles and squares on a node:**
Triangle pointing the node means input. It only allows one pipeline in.
Rectangle means output and can allow multiple pipelines out
![](KSJai28.png)

The number of triangles on the sides is how many input pipelines that node can receive, meanwhile the output can be many pipelines out:

![](2CV7AWf.png)

![](HxGoZ3q.png)

However, the triangles are actually specific types of inputs. Move your mouse over and pay attention to the hover tooltip and/or the status bar (bottom left area):
- Effect Input:
  ![](Re9aoyZ.png)
	Eg. When clipping a media to be a smaller area, that Rectangle clip node (called "Rectangle" node) would be applied as an input to the blue triangle (or any triangle - it'll move it to the correct triangle for you).
	
- Background Input and Foreground Input

![](sCjgN7T.png)

![](ERvX8Ba.png)

There are other input types which limits what types of nodes are acceptable as inputs, but the above are the most common ones

**Q: I can keyframe zooms and pans on timeline clips in Edit, can I do the same for some of the nodes in Fusion?**
Yes, you can. You'll have similar options and diamond button (which activates keyframing) for nodes like Transform. 
- And as a bonus, you have access to the Keyframes panel and the Spline panel while on the Fusion screen
	- Fusion's Keyframes panel: Edit the keyframes in a timeline manner.
	- Fusion's Spline panel: Edit the keyframes with lines and points (you can convert to curves).

Q: The nodes are all over the place. Can they align to a grid?
Yes. Right click an empty area in the Fusion Nodes editor -> Arrange Tools -> to Grid.


---

## Fusion Preview

### Preview Range

Select the range that you will loop when using preview monitor on the Fusion screen. This lets you focus only on a portion of the Fusion so you double/triple check your node's effects.

![](tnOiW3a.png)
You can adjust the preview start and preview end (aka in and out) by dragging the left yellow line and the right yellow line. Or on Windows, you can CTRL+drag and where you started and where you released are the new in and out. On Mac, you can CMD+drag instead. There are two input fields from left to right for in and out, respectively: You could also click and drag left and right starting from hovering the mouse over the input field. That red line is just the current play position. Press Space and it'll play from the "in" to the "out" and loop infintely. 

It's confusingly called Render Range because DaVinci has to calculate Fusion node's settings and re-render.

You can watch these instructions at https://www.youtube.com/watch?v=hFK8zV4M7Lg

### Preview Monitor
There are two bubble icons for any node you select. Left bubble will set the left preview monitor for the node. Right bubble will set the right preview monitor. Often you select two different nodes for the two preview monitors to see the "before" and "after".

![](8Nn7jbJ.png)


---


## Fusion Node

### Merge node, Background node, Transform node
Merge node: Allows you to combine two clips
Background input could be MediaIn1
Foreground will be the Background node, so that the Background clip overlays the media clip
![](13oLPPv.png)

Make sure to resize the background (You have to go into Image tab and tick off "Auto Resolution" before it allows you to adjust the width and height):
![](FBFx7cU.png)

The result is this:
![](PCju7Od.png)

But you probably want to move that black bar to the top left before putting text in there. You have to add a Transform node where you readjust the center x and y:
![](oS8xCAv.png)

Result:
![](t34QmMF.png)

Then you can add Text in there at a later time

## Rectangle or Polygon Mask

Let's use "Rectangle" node. This is an effect that you apply on a clip. When applied, parts of the clip gets seen through

Rectangle node is resized to your desired clipping (or leave alone because it is small by default, so you can still see the effect). Make sure the Rectangle node pipes into the Effects input of your clip:
![](mLrcVSS.png)

Result:
![](U1WCclw.png)

### Text Node:
![](nUe3ck7.png)

This is INCORRECT:
![](kQ0Cdkn.png)

## Grouping

Useability: You may want to group nodes, especially if the nodes go into the merge node - with CMD+G, then rename the group with F2.
![](FxHlSQ8.png)


---

## Nodes - Advanced Examples

Eg. Slide in black box with fading in white text

Here was have a slide in black bar then a fade in course name “Notion Crash Course”.. The Notion icon would’ve been hidden by the black slide in bar, so we used a polygon mask to mask in the icon on a duplicate media.

![](eQ3mgIV.png)

![](Vqkt8F6.png)

### (*Collapsible)*
### Example slide in black box with fading in white text
![](EEnwMRG.png)

![](uEzEfzo.png)


---

## Copying nodes into other clips or projects

You can click a text node, then CMD+C then CMD+V in that clip's fusion or another clip's fusion

If you had pasted CMD+V into notepad you see:
```
{
	Tools = ordered() {
		Text1 = TextPlus {
			CtrlWZoom = false,
			Inputs = {
				GlobalOut = Input { Value = 103722, },
				Width = Input { Value = 1668, },
				Height = Input { Value = 1080, },
				UseFrameFormatSettings = Input { Value = 1, },
				["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
				Center = Input { Value = { 0.959, 0.64 }, },
				LayoutRotation = Input { Value = 1, },
				TransformRotation = Input { Value = 1, },
				Red1 = Input { Value = 0, },
				Green1 = Input { Value = 0, },
				Blue1 = Input { Value = 0, },
				Softness1 = Input { Value = 1, },
				StyledText = Input { Value = "TOTAL FILESIZES:\n102.83 MB\n27 images\n1457 characters text\n1m 36sec video\n", },
				Font = Input { Value = "Open Sans", },
				Style = Input { Value = "Bold", },
				Size = Input { Value = 0.0512, },
				VerticalJustificationNew = Input { Value = 3, },
				HorizontalLeftCenterRight = Input { Value = 1, },
				HorizontalJustificationNew = Input { Value = 3, },
			},
			ViewInfo = OperatorInfo { Pos = { 321.333, -8.39394 } },
		}
	},
	ActiveTool = "Text1"
}
```


---

## For Polygots
  

DaVinci Resolve fusion nodes are very similar to Blender nodes and Unreal Material nodes, how much dissimilar to Unreal Blueprint nodes