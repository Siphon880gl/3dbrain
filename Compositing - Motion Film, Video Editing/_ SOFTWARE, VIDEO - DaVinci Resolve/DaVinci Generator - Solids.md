
Effects Panel for either:
- Texture background (For gradient and smoke etc)
- Solid Color

Or as a Fusion node

---

Curiosity - Can you force a texture background into a solid color?

Yes, although if you want to achieve a solid color background, just search for "Solid Color" in Effects

But if you choose to force a Texture Background into a Solid Background:

Choose Two Color mode and assign both colors white

![](5Jn5CKA.png)


---

Fusion way:
You can SHIFT+Space and search for a "Background" node. Make sure to merge it along the way of MediaIn to MediaOut

![](on3INFg.png)

Let’s say MediaIn is an empty clip (black screen)

You can adjust the color of Background node at its Inspector side panel. Adjust the color to all white. The result should be an all white screen.
