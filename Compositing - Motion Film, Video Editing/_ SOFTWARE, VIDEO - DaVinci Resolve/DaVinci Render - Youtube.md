## Upload to Youtube


DaVinci Resolve →  Preferences → Internet Accounts

Delivery page (Rocket icon at the bottom)

You select Youtube from the top left choices (not the custom codec exports)
You set the title (both filename and youtube titles are separate)
You can have “Chapters from Markers’
Set the description.
Set the privacy and category
You can set thumbnail preview

Make sure “Upload directly to Youtube” is checked

Dont forget to hit “Render all” at bottom:
![](LEFpSBO.png)

Video instructions: https://www.youtube.com/watch?v=TVJgXMKvSTo

Visit your Youtube uploads at “Youtube Studio” and check everything's fine.


---

 ## Youtube: Your upload "got abandoned"?


 If you have an error message at Youtube Studio about the video processing being abandoned:

Increase your video length limit
By default, you can upload videos that are up to 15 minutes long. Verified accounts can upload videos longer than 15 minutes.

To verify your Google Account:

On a computer, go to https://www.youtube.com/verify.
Follow the steps to verify your Google Account. You can choose to get a verification code through a text message on a mobile device or an automated voice call.
Make sure you're using an up-to-date version of your browser to upload files greater than 20 GB.

![](N72Pr3E.png)

---

## Youtube: Black bars on video after uploaded

[https://www.youtube.com/watch?v=wIksd88AJN4](https://www.youtube.com/watch?v=wIksd88AJN4)