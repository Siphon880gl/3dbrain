
   - On **macOS**: `/Library/Application Support/Blackmagic Design/DaVinci Resolve/LUT`
   - On **Windows**: `C:\ProgramData\Blackmagic Design\DaVinci Resolve\Support\LUT`
   - On **Linux**: `/opt/resolve/LUT`
