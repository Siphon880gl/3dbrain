In Inspector of the Text+ clip, go to Shading and select shading element #2.

Now you can adjust the thickness and color. Thickness may look limited by the slider but you can manually enter a large number.

Instructions from:
https://youtu.be/cbHAGOLBgJU