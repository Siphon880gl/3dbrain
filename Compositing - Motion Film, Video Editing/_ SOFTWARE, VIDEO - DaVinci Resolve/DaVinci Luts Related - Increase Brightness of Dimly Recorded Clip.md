
Problem: I have a video clip that’s shot in the dark. It looks too dark. I want to brighten it up by making it look like the light sources had been slightly more intense


I. Creating the nodes in Fusion
Select the clip in the timeline and make sure the playhead is inside it

Go to Fusion tab.

Between MediaIn and MediaOut add these two nodes in order of piping. You press SHIFT+Space then search for the nodes to add.

Range → Brightness/Contrast

The range stands for light range but you can’t find “light range” - can only find “range”
![](ZJ407rg.png)

  

**II. Adjust Range**

At range, adjust the shadows / midtones / highlights 

- **Shadows**: These are the darkest areas of the image. Adjusting the shadows controls how much detail or darkness is present in those areas. Increasing the shadows makes them brighter, revealing more detail, while decreasing them deepens the darkness, emphasizing contrast.
    
- **Midtones**: These are the intermediate levels of brightness, the middle gray areas that aren't too dark or too bright. Adjusting the midtones can affect the overall exposure and balance of the image. Brightening the midtones can make an image look lighter without losing contrast in the shadows and highlights, while darkening them can deepen the image's tone without fully affecting shadows or highlights.

- **Highlights**: These are the brightest parts of the image. Adjusting the highlights controls how bright or intense these areas are. Increasing highlights can make bright areas even brighter, while decreasing them can recover lost detail in overexposed sections of the image.

![](13CbiHW.png)



For a more brightened picture, you can adjust the curve to be logarithmic instead of exponential. You have 8 dot handles to control the shape of the curve


See Inspector sidebar on the right for the Range settings:

  

**III. Adjust Brightness/Contrast**  

Next, you adjust BrightnessContrast  
  

Here are the fundamentals:

- **Increase Brightness**: This makes the image brighter, but can often cause the image to appear "washed out" or lose some clarity and contrast.
- **Then increase Contrast**: By increasing contrast after adjusting brightness, you're enhancing the difference between the darker and lighter areas of the image. This helps recover some of the lost focus and definition caused by boosting brightness.
- **Then lower Gamma if appropriate**: If the image appears washed out after adjusting brightness, tweaking the gamma can bring back details in the midtones without affecting the shadows or highlights too drastically because it actually affects the midtones: Lowering the gamma can reduce the washed-out look. 
- Optionally, use **Lift** to fine-tune the shadows if they get too dark or light during the process. 
  Increasing lift brightens the shadows, while decreasing it darkens them. It can be useful for bringing up shadow detail or making the shadows deeper while leaving the midtones and highlights less affected.


See Inspector sidebar on the right for the Brightness/Contrast settings:
![](fDzOZmq.png)

Rationale for Range to BrightnessControl. Range lets you control the shadows, midtones, highlights at a general way. Then Brightness/Contrast gives you finer control.