
DaVinci Resolve Python API ReadMe and Script Snippets are at (if Mac):
```
cd "/Library/Application Support/Blackmagic Design/DaVinci Resolve/Developer/Scripting"
```

DaVinci Resolve Fusion Scripting is for Fusion screen (where you have nodes that affect animation and filtering of your clips)

For more information, refer to Weng's Devbrain DaVinci Coding Tutorials:
wengindustries.com/app/devbrain/?open=_%20DaVinci%20API%20vs%20Fusion%20Scripting