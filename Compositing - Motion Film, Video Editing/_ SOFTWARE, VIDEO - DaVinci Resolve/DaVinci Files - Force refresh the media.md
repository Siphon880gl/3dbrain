
You updated a picture or file in the same folder (for example, updated a picture in photoshop)? You have to force refresh the picture/clip in the media pool so the cache clears and you get the most updated clip in the timeline. You relink this way:

Right click the file in Media Pool → Link Full Resolution Media. Then go back to the same menu item to Unlink Proxy Media (has been changed from “Link Full Resolution Menu”)