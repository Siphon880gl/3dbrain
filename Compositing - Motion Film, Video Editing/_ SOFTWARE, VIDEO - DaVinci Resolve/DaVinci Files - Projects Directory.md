If on Mac, it's located on:
```
/Users/wengffung/Library/Application Support/Blackmagic Design/DaVinci Resolve/Resolve Project Library/Resolve Projects/Users/guest/Projects
```

If your project is not found, you can go higher in the folder hierarchy and see if there are more DaVinci users:
```
/Users/wengffung/Library/Application Support/Blackmagic Design/DaVinci Resolve/Resolve Project Library/Resolve Projects/Users
```