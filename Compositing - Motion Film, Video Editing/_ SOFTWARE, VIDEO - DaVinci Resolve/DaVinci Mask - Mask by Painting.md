
This is great for many cases.

One use case is covering autosuggestions on a screen recording. Generally speaking, that's - placing a part of an image over the video. This is better than cutting the overlay because the paintbrushing allows for a more natural blend where the edges are.

Let's say you have a screen recording of how to sign up on an app's form.

You'd imagine a way to do this is to have another video layer on top of the main screen recording. This clip at Video 2 will have a freeze frame of the signup before the recorder starts typing into an input that reveals the gnawing autosuggestions. That freeze frame would be partially erased so only the area that hides the autosuggestions will show.

Here's the annoying autosuggestions
![](1xN7zN2.png)

Here's a video track 2 that we will 
paint in the pixels that hide the autosuggestions yet leave alone the area we want to show through from Video 2 where the user types into the input and moves their mouse.
![](63rkQH0.png)

Make sure Video 2's clip is selected and go into Fusion screen. Press Shift+Space, then search for Mask Paint. Have Mask Paint pipe into the Media1

![](g5Ffwxq.png)


Before you start painting white (which are the pixels you want to appear), make sure you're on the first frame (red line should be set to the first frame) (as a quick review, red line is playhead and the two yellow lines are the clip in and out)
![](nOu6VpN.png)


Make sure Mask Paint is selected. Under Inspector Side-panel, you'll see under Tools -> Controls paintbrush options. Leave the color white. 
![](3UPZWjw.png)

In the preview panel, you can start "painting" with your mouse cursor.

You can start painting in the pixels to show on top of the original screen recording. We'll hide the autosuggestions that appear:

![](ewl75Wr.png)

^Challenge: Let's say above Password is Email. We want to hide Email autosuggestions. How would you paint it properly? I'd use eraser and erase the password area and leave in the area below the password input and the "Confirm Password" label. I would paint further down in case the autosuggestions were longer.

After you paint in the first frame, unfortunately this applies only to the first frame then the rest of the clip in Video 2 track will cover your entire screen recording. So the next step is to freeze Video 2 clip on the first frame (which is the frame you applied masking to)

Right click Video 2 track's clip and enable "Retime Controls" which will add more controls on your clip:
![](1Ng04Jz.png)

Then make sure your playhead is on the first frame of the clip. Click the Retime control (usually "100%") and then Freeze Frame
![](pdI0g0U.png)

The result looks like:
![](a2K80X5.png)

---

Troubleshooting:
Q: Freeze Frame option grayed out
Make sure your red playhead in the timeline is over the clip you're freezing

Troubleshooting:
Q: Mask Paint Controls is empty, saying `<No Object selected>`
![](OWdnM3r.png)

A: Switch to Paintbrush (See above red circle):
![](N4b6sgj.png)

Troubleshooting:
Q: Preview panel shows green strokes
![](uKVfcm7.png)

A: You've selected your paintbrush strokes with the mouse tool. Switch to Paintbrush (See above red circle):
![](B5BBib2.png)
