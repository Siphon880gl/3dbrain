
Instructions here:
https://youtu.be/U4ehKSTBnBY

But key things the video missed is it has to be inside Fusion, and it has to be a fresh Text node (If you're editing the fusion page of a Text+ clip, it's not going to work)


Full instructions here:

Fusion setup
1. Whether it's a Text+ clip or normal clip, go into Fusion page
2. Create Text Node (SHIFT+Space for node library)
3. Create Merge Node
4. Your clip pipes into merge. Merge pipes into MediaOut1. However, Merge also receives the TextNode
5. If applicable: If your clip is a Text+ clip, cut the text from the MediaIn into the TextNode
   
Character level styling
6. At Inspector panel for TextNode, right click the Text box and click "Character Level Styling"
7. At this Inspector panel, switch to the new tab "Modifiers"
8. Select the characters you would like modified from the preview panel. Then adjust the style at the Inspector

![](LndiRgQ.png)
