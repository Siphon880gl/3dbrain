You can open up audio left or right:

Right click audio clip in timeline, go to Clip Attributes, go to Audio tab, switch from Mono to Stereo
![](JLZrjwf.png)

![](foTJmTT.png)




---

OR

You can control precisely audio left or right at the Fairlight (Audio) tab

Add a plugin to the Bus:
![](91fry78.png)

The specific audio plugin you're adding is: Spatial → Stereo Width
![](Dd2S4z8.png)


And you adjust the stereo with: Stereo Width
![](DfC0jfm.png)


