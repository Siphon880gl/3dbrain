
Select the applicable clips in the Media Pool -> LUT -> Select your desired LUT

![](FCv0gpg.png)

Note if the clips have already been placed in a timeline, the LUTS will apply to the timeline clips as well.

To get an idea on what LUT you can apply to your motion film or video based on genre, dramatic effect, or to give off a specific movie's vibes, refer to this list:  [[LUTs for Genre, Theme, Dramatic Effect, or Giving Off the Vibe of a Specific Movie]]