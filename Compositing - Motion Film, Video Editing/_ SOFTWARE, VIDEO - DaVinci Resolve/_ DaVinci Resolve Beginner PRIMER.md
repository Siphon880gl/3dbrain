## Why DaVinci

Better workflows. Previewing is more efficient than FCP and Adobe. Node system shows it’s programming heavy and most likely will have potent AI in the future. Powerful color grading and even visual noice removal (paid version though). As of 2023, it’s a one-time pay and not subscription models as others. Automatic chapters for Youtube.

## Screens

- Cuts vs Edits page. 
	- Cuts are for more simple edits. 
	- Most editing requires the Edits page. 
	- See [[DaVinci Cut Screen Advantages]]

- Fusion tab lets you layer, mask, and transform for a clip. For a history of Fusion, refer to [[Fusion History]]
	- For Fusion information, refer to: [[_ DaVinci Resolve Fusion PRIMER]]
## Editing

Alphabetically, here are the most common techniques in editing in general, and how it's done in DaVinci:
- Compound clips: Select the clip(s) in timeline -> New Compound Clip
- Crossfade/Fade in/fade out: 
	- How: Open Effects panel at top left. Then search for effect. Drag and drop effect to clip(s). If applying between clips, drop it between the two clips. Otherwise you drop it to the beginning or the end of a clip.
	- You could use handles that appear at the edges of the clip when you mouse over. They offer an additional intermediate node so you can control how concaved, convexed, linear, steep or flat the audio fading is. For example, for more of an attack fade in, you go for a logarithmic audio curve. 
- Detach or unlink tracks: audio from video: Select, then right click, then untick “Link Clips”
- Markers
	- Jump marker to marker with SHIFT + Up / Down
	- Delete a marker: Select it. Backspace.
- Mask
	- Mask by shape. See [[DaVinci Mask - Mask by Shape]]
	- Mask by paintbrush. See [[DaVinci Mask - Mask by Painting]]
- Playhead
	- It's this:
	  ![](QvMeaat.png)
	- That's the redline that shows you where you are on the timeline. Note the redline can be dragged to any frame or seconds on the Edit screen's timeline. But on the Cut screen's timeline, the redline is always centered on the timeline, but you'll notice a source tape panel above the timeline and it too has a red line, and that red line you can drag freely to any frame or seconds - details at [[DaVinci Cut Screen Advantages]]
- Ripple delete vs lifting:
	- Shift+Backspace for ripple delete (slide the subsequent clips backwards to fill in the newly created gap)
	- Backspace for lifting aka deleting (DO NOT slide the subsequent clips backwards to fill in the newly created gap)
		- Why called lift? History in [[Video editing - Lifting]]
- Shortcut keys while editing:
	- a - selector
	- b - blade
	- m - markers
- Transitions: See "Crossfade/Fade in/fade out" in this same list.

---

## Needed background knowledge:

Masking layers:
They can be rectangle, polygon, or other shapes. What’s inside the dashed border of your selection are what’s allowed through to the screen output. What’s outside are blocked from the screen output. In other words, you select to mask in, and pixels not selected are masked out.

Checking audio's before rendering:
- You want to have headphones on with 90% audio so you can pick out ambient noise that abruptly stops when you cut audios of “umms”. One time I had my headphone on 40% then didn’t realize if the audio was loud, it would have these abrupt stops.

For affecting curves (speed, fading, etc):
- Exponential vs Logarithmic curves


---

## Paid Version

DaVinci Resolve is free version. DaVinci Studio is paid version.

Paid version as of Oct 2024 is $300 one time payment

Paid version includes:
- More complete API (for Python or Lua scripting). Python scripts can run externally (rather than have to run code in Workspace -> Console or drag and dropping python scripts into the console)
- More features. See effects here when you scroll passed far enough and try to use a transition:
  ![](Vj8wqfO.png)
