
You can SHIFT+Space and search for a "Background" node. Make sure to merge it along the way of MediaIn to MediaOut

![](on3INFg.png)

Let’s say MediaIn is an empty clip (black screen)

You can adjust the color of Background node at its Inspector side panel. Adjust the color to all white. The result should be an all white screen.

For Background1 (white background) turn off Auto Resolution. Now you can adjust width and height:

![](yo1hUvT.png)

You can adjust the width and height
![](t5Ph7op.png)

For moving it from the center, you need Transform node:
![](W06c9nM.png)


You can try moving the white rectangle to the side:
![](ABT8QoH.png)


---

## Reworded

This is a more practical example

Add a Merge node between MediaIn and MediaOut
Foreground will be the Background node, so that the Background clip overlays the media clip
![](13oLPPv.png)

Make sure to resize the background (You have to go into Image tab and tick off "Auto Resolution" before it allows you to adjust the width and height):
![](FBFx7cU.png)

The result is this:
![](PCju7Od.png)

But you probably want to move that black bar to the top left before putting text in there. You have to add a Transform node where you readjust the center x and y:
![](oS8xCAv.png)

Result:
![](t34QmMF.png)

Then you can add Text in there at a later time