
Titled: DaVinci Timeline Clips - Retime Curve - Speed up and slowdown at different parts of a clip

Retime Curve - speed up and slowdown at different parts of a clip

Right click a clip → Retime curve

You can add new points along the line (OPT+Click part of the line). Then you can keyframe them
![](j2uNDok.png)

Drag up and down the point to adjust the timecode of the frame you want it to be frozen or slowed down or sped up to, from the previous keyframe

The bottom line (less emphasized line) shows you step wise how much of a difference:
![](Qloc6ph.png)

You can add points at the bottom line (the less emphasized one) if you want fine tuning horizontally.

---

Btw, if you want to freeze frame you want to adjust the second keyframe it so it’s closer to flat
![](fY6h8Cp.png)
