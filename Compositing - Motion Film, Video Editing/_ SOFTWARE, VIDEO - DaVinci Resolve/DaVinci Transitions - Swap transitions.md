Problem:
You applied a transition to the start or end of a clip, or you applied a transition between two clips. But that's not the transition you wanted. You want to swap it out with another transition you find in the Effects panel.

Just drag and drop the preferred transition onto the transition in the timeline.