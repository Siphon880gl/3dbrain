
Let's say you have a video clip but you want the first frame to be frozen for the duration of that video clip. Or you have a frame you wish to freeze on.

Right click the video clip and enable "Retime Controls" which will add more controls on your clip:
![](1Ng04Jz.png)

Then make sure your playhead is on the first frame of the clip. Click the Retime control (usually "100%") and then Freeze Frame. Note if you hadn't placed the playhead correctly, "Freeze Frame" would be gray (unselectable).
![](pdI0g0U.png)

The result looks like:
![](a2K80X5.png)

---

Troubleshooting:
Q: Freeze Frame option grayed out
Make sure your red playhead in the timeline is over the clip you're freezing