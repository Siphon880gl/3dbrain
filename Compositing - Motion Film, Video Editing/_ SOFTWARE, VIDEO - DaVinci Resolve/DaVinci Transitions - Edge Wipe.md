
How:
You access at Effects panel and search for Edge Wipe. Then drag and drop into the start/end of a clip or between two clips on the timeline.

Changing the side the wipe comes in from:
- Firstly go to the "Transition" tab:
  ![](AtoqOfz.png)

- Remember these degrees:
	- 0 push up
	- 90 push right
	- -90 push left
	- 180 push down