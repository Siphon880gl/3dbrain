Bug - Importing in timeline breaks fusion associated with clips on that timeline

In fusion I can see the media in on preview monitor 1 and media out on the monitor 2. I see there are changes as expected from my fusion nodes that zooms the image. But when I scrub the preview frames.. I dont see the keyframing.
I see that the keyframes are negative in the fusion clip!!

Mines was even from -86400 to -86335
![](4pPzi8u.png)

Other people have reported huge numbers of 107892.0 - 108041.0 instead of 0 - 149 though they’re caused in other ways. Ironically it’s adjustment clips causing problems for this user (though adjustment clips is how we will fix it)
https://www.reddit.com/r/davinciresolve/comments/qiqsvw/make_an_adjustment_clip_start_at_0_in_fusion/

When I add an adjustment clip and bring it into a timeline above the clips, the duration of the clip is no longer something crazy like that.

This error can be replicated by importing any edl or otio timeline file. the edl and otio are very standard. starting timecode at 0. has transitions tho but bug replicates even without transitions in the timeline file. 

But I discovered this is an old bug that makes fusion starts at a huge number like -86400, some huge negative number, or some huge positive number. No I did not accidentally adjust the length of the clip on the left side to introduce negative frames

The general fix was to add an adjustment clip in a second video track, then remove that video track. It's been a bug since 2021 and DaVinci hasn't fixed it. 

However for the fix that’s caused by importing timeline, you have to keep the adjusted clip, and it’s one adjusted clip per timeline clip, at same duration and time positions. There’s no removing the adjustment clips because once you do, the problem comes back.

How:
Add Adjustment Clip from Effect

![](tCmUWFx.png)

Drag to then copy and paste, and snap into place, and resize
![](MafOmVx.png)

**So looks like:**
![](l8fl6DF.png)

Then apply your fusion nodes into Video 1 track. Video 2 track is to keep the frame start back to 0.

Yes you can programmatically reset the start frame (SetStart) which might or might not fix the problem in the paid version. Free version doesnt support SetStart and will fail quietly.