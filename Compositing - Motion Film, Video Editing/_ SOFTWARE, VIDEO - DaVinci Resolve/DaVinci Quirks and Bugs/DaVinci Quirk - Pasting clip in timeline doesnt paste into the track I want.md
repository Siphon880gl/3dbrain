Titled: Pasting clip in timeline doesnt paste into the track I want

Problem:
When I paste a clip, it doesn’t paste to the track I selected (instead, it typically pastes in place of another track’s clips)

Solution:
Hold ALT (Windows) or OPT (Mac) and click the `<>` button on the track you’re editing so other tracks dont get accounted for your operations. This makes the other tracks de-highlighted. Make sure to toggle it back to the way it was after you’re done editing (ALT/OPT+Click the same `<>` button to perform an inverse selection, then normal click again to re-include the current track).
![](5ygQGWX.png)


1:17 https://youtu.be/D535CC4ObJo?t=76