Titled: Relink Selected Clips in Media Pool Failed

**It won't let me relink assets (happens with aiff files)**
- Then the cleanest way to do this is to create a compound clip of the "Not found" clip (whether it's audio or video in question), then open that compound clip in a new timeline. In the new timeline, you import the replacement asset and make sure waveform (if audio) or frame pictures (if video) match up. Then exit back to the main timeline (Your Timeline is accessible in the Media Pool -> Master)
- Reworded: My workaround was creating a compound clip of the broken audio clip, then opening the compound clip in the new timeline, then making sure when I re-import the new audio file location, I can drop directly below the broken clip and match them up
![](1YUMDh0.png)

- If the waveforms and graphics are missing, it's too late because the cache is cleared. You're gonna have to manually make it sound or look right again with the new re-imported.
