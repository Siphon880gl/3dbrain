
Some audio clips when dragged from the Media Pool into the timeline becomes mono

Right clip the audio clip in the timeline / audio track → Clip Attributes → Audio track → Change format from Mono to Stereo

![](sAygbAi.png)
