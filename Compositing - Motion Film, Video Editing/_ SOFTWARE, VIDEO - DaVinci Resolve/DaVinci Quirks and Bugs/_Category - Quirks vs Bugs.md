Quirk is when a UI isn't working as well as you'd like or the ux flow is counter intuitive, often requiring more steps to perform a task correctly.

Bug is a feature that's supposed to work but doesn't work quite right.