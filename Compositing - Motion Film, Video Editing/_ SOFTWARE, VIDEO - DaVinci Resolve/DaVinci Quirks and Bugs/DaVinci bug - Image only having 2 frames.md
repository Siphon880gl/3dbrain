Sometimes when you drag and drop an image into the timeline, even if you make the image duration longer (by click and dragging to extend the duration), going into Fusion page you only see 2 frames. 

This sometimes happens and is a bug. The frames should increase as you increase its duration. You may need more frames especially if you're keyframing effects in Fusion.

In order to reset the frames to the duration of the timeline clip, convert it into a new fusion clip: Right click the timeline clip -> New Fusion Clip