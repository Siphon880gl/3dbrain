
Titled: Old fusion clip that's been split with blade now not working with Fusion

Problem:
I used a blade tool a split a clip into subclips, but I can’t seem to apply Fusion node effects on the subclip now. 

You should make the subclip you need to edit in Fusion into a Fusion clip.
Right click clip → New Fusion Clip

