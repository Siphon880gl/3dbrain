
At Fusion screen, add a Paint node (SHIFT+Space):
![](jVYQh7S.png)

With Paint node selected:
![](UIwVdnL.png)

Make sure Tool’s Apply mode is on Color (That’s the paint bucket icon). The two-paintbrushes icon is actually cloning
![](xGIC5Je.png)

You can adjust brush size under Modifier, but remember to switch back to Tools mode when you’re actually drawing on the screen, otherwise it won’t draw
![](ErR1HFg.png)

If you want to draw straight lines, add the dots for which they connect, in polygon mode:
![](8PGiN2T.png)

If you mess up, you delete a stroke or polygon inside Modifiers tab
![](S41b6WK.png)

