
A more proper way of zooming. Proper because you have the option of adding blur which more matches what's done in film for psychological what's expected.


![](efh5vUk.png)


[https://youtu.be/t5a3m7aP4D0?si=Yl-Wiy9jzst0uHkf](https://youtu.be/t5a3m7aP4D0?si=Yl-Wiy9jzst0uHkf)