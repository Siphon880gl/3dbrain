Make sure in Edit tab:
![](sVSBx54.png)



Media Pool -> Right click Timeline asset -> Timelines -> Starting Timecode

![](nvBIcWM.png)


From
https://www.youtube.com/watch?v=AOsxjdxGPzo


---

## Background

Timecode starting at 1 hour is normal. _Some broadcast programs will use a start time of 10 hours. This comes from the days of linear editing using tape where syncing to timecode cannot use negative values. It has become the accepted norm and allows space before the program start for things like color bars, test tones, program information, and countdown clocks.

In summary: The timecode in DaVinci Resolve may start at one hour to provide some lead time for test signals, bars, and tones before the actual editing begins. This is a standard used in broadcasting  

