
The most significant advantage to this type of view is that **you can see the full length of your clips along with the audio**. If you have a specific section you want to use from your clips, it is easy to drag it out and place it on your timeline.

"_The cut page is honestly awesome_. I use it for all of my basic editing to get things where I want them and building the bones of my videos"

An "overview" panel lets you adjust clips a different way. See above the timeline:
![](JjI1W9y.png)

Its official name is the **"Source Tape"**. This view allows you to quickly browse through all your clips in a sequential, tape-like format, making it easier to review and edit footage without switching between individual clips in the media pool. It's designed for faster editing workflows

Note the playhead (red arrow) at the timeline section requires a bit of different thinking. Unlike the Edit tab, the playhead will always be at the center. You can drag the playhead at the overview panel. Note it's harder to go to a specific frame this way, so for that you go with the Edit tab.