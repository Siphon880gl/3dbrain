
## Overview

Note Fairlight sound library as of 2/2025 is more life recordings like eagle wing, door sounds, ambient sounds. It’s lacking sound effects like Swish, Woosh, Glitch, Chime, Boom, Zap, Click, Beep.

The way to show all of the sound library is NOT intuitive. Later section will reveal how, but briefly, it's searching `***` and selecting the Fairlight library.

---

## Setup

Click "Sound Library" at the top left tabs:
![[Pasted image 20250302025926.png]]

You will be asked to sign up and agree to terms and services to download an installer file:
![[Pasted image 20250302030000.png]]

Then install:
![[Pasted image 20250302030027.png]]

## Listing all sounds first time

Restart Davinci Resolve.

When you go back to "Sound Library" at the top left tabs:
![[Pasted image 20250302025926.png]]

It may still appear empty. Listing all sounds is actually unintuitive. Go to next section.

## Listing all sounds - how

Click the expand icon (first arrow) to show a secondary dropdown for you to select "Fairlight Sound Library"

You have to search `***` to show all the sounds.

![[Pasted image 20250302030142.png]]

Now you see all the sounds:
![[Pasted image 20250302030253.png]]

You can still search for a specific sound:
![[Pasted image 20250302030442.png]]