
![](S6ugKcT.png)


http://design.tutsplus.com/tutorials/make-a-trendy-double-exposure-effect-in-adobe-photoshop--cms-23774

Thoughts:
is double exposure about clipping a picture on top of a background, giving that picture a screen blending mode, making it dark in value, desaturate and saturate, blurring out the edges, and airsofting parts of the picture? Seems complicated.
