
As of DaVinci Resolve 19.0.1 build 6, markers dont have names side-by-side. You have to hover your mouse over for the marker label to appear:
![](FL7P2uH.png)

So if you need to navigate markers, you open up Index -> Markers at the top left where the Media Pool is:
![](2IcPsoC.png)

For example, the above are markers on how to signup in a screen recording of a web app's signup page
