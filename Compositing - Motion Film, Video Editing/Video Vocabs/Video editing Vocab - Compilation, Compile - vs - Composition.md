
In movie editing terminology, "compilation" and "composition" have specific connotations:

1. **Compilation**:
   This usually refers to the process of collecting and assembling various clips or segments of footage into a sequence. In the context you're suggesting, it's akin to adding one image after another, although it more commonly refers to video clips rather than still images. A compilation is often used to create a montage or a sequence that conveys the passage of time or a variety of different scenes within a movie.

2. **Composition**:
   Composition in film editing is somewhat different from what you've described. It usually refers to the framing and arrangement of elements within a single shot or scene. This can involve the placement of actors, the set, the props, and the camera movement. However, when discussing the broader scope of post-production, composition can involve layering and arranging both visual and audio elements to create the final product. This includes not just stacking audio and video tracks but also integrating special effects, color grading, transitions, and other elements that contribute to the overall look and feel of the movie.

In digital video editing, you might also hear about "compositing," which is closer to what you described regarding stacking. Compositing is the process of visually combining multiple sources of footage or visual elements into one single image. This is often done in visual effects work, where you might combine a filmed actor with a CGI background, for example.

When you hear about audio and video tracks being stacked or layered together, this is typically referred to as "multitrack editing" or "layering," and it's a fundamental part of the post-production process that involves synchronizing, mixing, and editing audio and video tracks to create the final cut of the movie.



<<
So the verb is called compiliate? 

\>>
In the context of movie editing, the term "compilation" refers to the act or process of assembling various film clips or segments into a coherent sequence. However, the verb form of "compilation" is not "compiliate." Instead, you would use the verb "compile" to describe the action of creating a compilation. 

For example, an editor might compile various clips to create a montage sequence for a film. The term "compilation" itself is a noun, and "compile" is the verb that describes the action of creating such a collection.