Image is already zoomed in like 120% and then it pans to the left 10% over 5 seconds or 10 seconds

This is great for reveals. Like comics video where you reveal or emphasize the side or corner that has the action

Eg. Pan an already zoomed-in still at 15:43

[https://youtu.be/EkgmsD1wyO8?si=95UfLXhGNbdDoi8H](https://youtu.be/EkgmsD1wyO8?si=95UfLXhGNbdDoi8H)

---

The pan could be:
- top
- right
- bottom
- left
- topleft
- topright
- bottomright
- bottomleft