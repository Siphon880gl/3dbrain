Export the automatically generated captions:
Go to Youtube Studio -> Edit our video -> "Subtitles" left sidebar item -> At automatic captions, move mouse over Subtitles cell
![](Q05R8P2.png)

Download appears. Click Download. Download as srt:
![](ejmTOPr.png)

Then following instructions of manually editing in subtitles/captions from scratch, get to the edit subtitle screen: [[Youtube edit in subtitles or captions - From Automatic Captions]]

Except you'll import the subtitles at the edit subtitles screen WITH timing.
You do this thru either screen (depending on if your video already had manual captions saved):

![](MsYBwnu.png)

Three dots then upload file (Select with Timing later):
![](tZO7Aa1.png)

---

Then you can fix any spelling errors made by the automatic captions