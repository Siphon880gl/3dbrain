Add manual timemark ranged subtitles/captions. 

Go to Youtube Studio -> Edit a video -> "Subtitles" on left sidebar.

It may ask you to add a default language. Add it if it does ask you.

Resuming, at that page:
Move mouse over the “-”
![](blBo5vC.png)

Now you can add manually timemark ranged captions/subtitles
![](1xCHShK.png)


![](oQPDZmS.png)

Hover mouse to the most bottom caption/subtitle tile and move the mouse to the bottom left corner of the tile to reveal a Plus button:
![](CInkPj8.png)

![](A0vCSMS.png)

---

Then you have complete control over what words appear when and for how long. If you want to import automatic captions then adjust the misrecognized words, refer to: [[Youtube edit in subtitles or captions - From Automatic Captions]].