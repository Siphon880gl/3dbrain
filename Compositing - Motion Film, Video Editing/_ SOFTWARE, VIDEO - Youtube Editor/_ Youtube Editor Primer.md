Aka: Get Started

Youtube Studio → Content → For your video → Details (pencil icon) → Editor (menu item)

You can mix in audio at 0-100%. No attribution required. You cannot add in your own custom sounds

Apply template lets you add templates of outro overlay elements at end of video th upsell your other videos. You can add Info cards of video, playlist, or channel

Can I replace part of the audio with another custom audio after the video's been uploaded?
- No as of 2023 Q3

Chapters
 - Must have 1000 subscribers
 - You add the timemarks into the description. How: https://tuberanker.com/blog/youtube-chapters-not-working