Commonly used sections (Select, then CMD+F, then enter):
Final Cut Pro Onboarding

---


_ Had been unable to figure out green screen at a corner mirror app. Next lead? (But figured out in another project though) https://blogs.telestream.net/2017/08/chroma-key/

_ Crossfade audio
https://support.apple.com/guide/final-cut-pro/crossfade-audio-ver66d503b23/mac

Fade out audio track:
Modify -> Adjust Audio Fades -> ...
Now at end(s) of the audio track, you have a notch/handle to manipulate the fade out

_ have all clips of similar volume?
http://www.steves-digicams.com/knowledge-center/how-tos/photo-software/final-cut-pro-how-to-set-proper-audio-levels.html#b
Select the audio clips? -> Modify -> Auto Enhance Audio
Select the audio clips? -> Modify -> Match Audio
https://larryjordan.com/articles/normalizing-audio-in-final-cut-pro/#:~:text=The%20easiest%20way%20to%20do,work%20the%20way%20you%20expect.

_ Make video scenes all have the consistent lighting with lut

_ Tick tok font
https://www.google.com/search?q=ticktok+font&oq=ticktok+font&aqs=chrome..69i57.3861j0j4&sourceid=chrome&ie=UTF-8
Google-loading font into FCP

_v_ Record webcam using FCP
Import media to Library -> On sidebar, select away from HD to Camera -> Import
Once done recording -> Stop Import -> It'll appear under Library event on the sidebar

# Advanced Library
delete a part of a clip: i/o -> backspace -> Hide Rejected (to hide those)
To unreject: Show All (marked as red then) -> click red areas of the clip -> u

# Troubleshooting
## Stuck at 50% to youtube
https://discussions.apple.com/thread/250362559
My experience: Just wait it out. It goes from 50% to 100% then done. Problems actually showing the progress beyond 50%. Once it reaches 100%, it wont prompt you. It'll just all go to idle. Youtube Studio Creator content may say "Uploading 0%" and stuck there, but it's actually just processing.

# Final Cut Pro Onboarding

## Philosophy

FCX doesn't have the concept of "tracks." It uses "connected clips," "storylines," and "compound clips." None of them directly translate to FCE/P tracks (Final Cut Express, Final Cut Pro).

Generally speaking, a library contains media assets, events, and project(s), basically everything about your production. The library is saved as a fcpbundle file.

Project is the same thing as timeline but how would you remember that? Well the timeline is actually consists of metadata and pointers to your media files, which in a traditional sense of music/movie/image/etc editors, what projects are. You will absolutely need to bare minimum have a library and a project (because you need to have media assets and a timeline).

You may want to organize your media/timelines further with events; Say you want to organize all the assets and scenes to Chapter 1 of your movie.  In other words, an event has media assets and projects (each project/timeline can be a scene). You create a new event by going to File -> New -> Event. Your library will continue to contain your event(s).

If you want a streamlined workflow, you can create Smart Collections, which automatically gather all the clips or projects in a library or event that match criteria you specify. 

McCall's tip: Be warned that libraries are VERY prone to corruption. Once that library file becomes corrupted, all project in that library will now be “trapped” in that library. You can mitigate that risk by have MORE libraries. 

## @ New workspace

Close all opened libraries. You right click a library at the left sidebar -> Close library "..."

![](A3RNXxW.png)


You either create a new library (File->New Library) or open an existing library (File -> Open Library). If creating a new library, give it a good name that represents the production you are doing. When importing the file, "Leaving files in place" instead of copying to library will help reduce file size but you have to make sure you preserve the file locations.

	If complains "This document cannot be saved to iCloud Drive".. then choose a folder that is not iCloud syncing

	Then if applicable, you import files to the library..

Then you create a new timeline by going to File -> New Project or "New Project" button where timeline should be. Remember that timeline is called project in FCP

A good place for your library file, media, and Timeline project are in /Users/wff/Movies/<Whatever title>

Whenever you save, you are actually saving a "Final Cut Pro Bundle" which consists of the Library including any Timeline Project(s)

## Opening old workspace

1. You close all library and timeline projects.
2. Open a library.
3. Then the project is along the clip in the library sidebar. Double click that project to open the timeline project too.


## Exporting

File->Share->
Master file if you want traditional saving a video file on your computer
But there's also direct to Youtube, Vimeo, etc

## Basic Usage
https://www.youtube.com/watch?v=ygBlgaT78mM

### Panning
![](kGRT0ht.png)

Chop out CMD+B

Deleting an audio or  video track but it removes the other one too? You can do some combination of: 
- Right click -> Detach audio...
- Right click -> Lift from Storyline

# Preview only a selection (Range Selection)

Press R to change cursor on timeline

![](tp9nmP3.png)

Then drag over a portion of your timeline (maybe between two markers)

Then press /, and possibly CMD+L after for looping

![](UVYjsOI.png)

The looping mode has a visual feedback at the Preview monitor:
![](5vSyoEB.png)


![](tRDNMHX.png)


## Transitions
https://www.youtube.com/watch?v=uKufm-Q_RLM 
Most acceptable transitions:
Dissolves -> Cross Dissolve
Dissolves -> Fade to Color
Dissolves -> Flow

## Compound clips

You can group tracks and clips on timeline together then they appear as one track/clip in the timeline. You can name that compound clip and access it in the library. When you access it, it opens a separate timeline with the clips unmerged so you can do further edits.

How: Select all tracks in the timeline (CMD+A), right click -> New Compound clip

## Creating gaps (Firecut pro likes to keep collapsing clips together):
Create gap: Opt W
Then trim it to whatever length.
Useful for making sure no video clip inbetween so you can place a title there

## Usability on Timeline thinking:
Show all markers: Index (top left of timeline) -> Tags -> All

Marker dialog to name new marker: Opt+M, or press M twice

Just a quick maker (no name): M

Jumping to markers
In Final Cut Pro, do any of the following: Go to the next marker: Choose Mark > Next > Marker, or press Control-Apostrophe ('). Go to the previous marker: Choose Mark > Previous > Marker, or press Control-Semicolon (;).

## Usability: @ Library, selecting video / audio clips to drag to timeline
Selection cursor: A
Blade cursor: B
Play backwards/pause/forward: j-k-l
Faster playback: Tap twice, or three times: j/l
Stop/start: space
Select in/out (then can drag to Timeline): i/o
  You can quickly select in/out by clicking then dragging with Opt to create the "o" position
   Selected in/out portions of clips then can be dragged to timeline or favorited with f/u
Favorite/Unfavorite current in/out: f/u
Browser favorite to drag easier: Smart -> Favorites
Zoom in or out on library: CMD +/-
You can control also how tall or wide the timeline is at these two sliders:

![](VHbXYRu.png)



You can shorten a clip in the library with i/o, then delete that segment

## Usability: @Timeline, switching between Blade mode and select mode (chopping lips on timeline further or back to normal cursor) and trim (shorten/lengthen clip): B, A, T

Also @ Timeline, adding markers: m
Zoom in or out on Timeline: CMD +/-
Create gap: Opt W

# Final Cut Pro Creatives

.FCP

## Installing downloaded themes:
https://www.youtube.com/watch?v=-c95nlA8pxQ

## Theme assets
- [Not really free] https://motionarray.com/learn/final-cut-pro/top-15-final-cut-pro-intro-templates/
- https://fcpxfree.com/
- https://www.google.com/search?q=final+cut+pro+intro+download&oq=final+cut+pro+intro+download&aqs=chrome..69i57.5646j0j4&sourceid=chrome&ie=UTF-8
- More at Motion Array. Try t0rrents

## Creating text aka titles
https://www.youtube.com/watch?v=cLyPo8RC_T0

# Publishing to Youtube - fix poor quality
Go for Settings -> H.264
https://www.youtube.com/watch?v=AGo7opiImvo
~To see upload progress: Window -> Background Tasks
~Stuck at 50%? https://discussions.apple.com/thread/250362559

# Keyframes
https://youtu.be/GUibBGujg7U?t=99

# Freeze frame
Part 1: Final Cut Pro Freeze Frame Tutorial
Move your Playhead to the point where you want the freeze frame to be added.
Select your clip in the Timeline.
Go to Edit > Add Freeze Frame or click Option + F on your keyboard. Your clip will be split, and the frozen frame added in between.

# Professional: Add Ken Burns or dynamic motion to still images: Concept: https://www.youtube.com/watch?v=DrZ2miNkC5M
Possibly where: When you edit a still image in Final Cut Pro and there's Crop etc. Do we use keyframes?

# New Concepts: Motion tracking and masking
>75% https://www.facebook.com/watch/?v=245124540152625
